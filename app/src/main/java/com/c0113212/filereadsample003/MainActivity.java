package com.c0113212.filereadsample003;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {
    private static final String FILE_NAME = "FileSampleFile";
    CreateProductHelper helper = null;
    CreateProductHelper helper02 = null;
    //db01はproductテーブルを作る02はproduct02テーブルを作る
    SQLiteDatabase db01 = null;
    SQLiteDatabase db02 = null;
    //ファイル名
    String FileName[] = {"/sample.txt", "/sample (1).txt", "/sample (2).txt", "/sample (3).txt",
            "/sample (4).txt", "/sample (5).txt", "/sample (6).txt", "/sample (7).txt", "/sample (8).txt"
            , "/sample (9).txt", "/sample (10).txt"};
    //DBに格納する名前
    String prename01, cityname, helptxt, prinumber, othertxt;
    private final IntentFilter intentFilter = new IntentFilter();
    //WiFi Direct対応デバイス(P2Pデバイス)の情報、接続状態を管理するクラス
    private WifiP2pManager mWifiP2pManager;
    /** Channel */
    private WifiP2pManager.Channel mChannel;
    /** リスナアダプタ */
    private ActionListenerAdapter mActionListenerAdapter;
    String formatedIpAddress;
    /** Wi-Fi Direct 有効/無効状態 */
    private boolean mIsWiFiDirectEnabled;
    /** BroadcastReceiver 全部 */
    private BroadcastReceiver mReceiver;
    /** BroadcastReceiver P2P_STATE_CHANGED_ACTION */
    private WDBR_P2P_STATE_CHANGED_ACTION mWDBR_P2P_STATE_CHANGED_ACTION;
    /** BroadcastReceiver P2P_PEERS_CHANGED_ACTION */
    private WDBR_P2P_PEERS_CHANGED_ACTION mWDBR_P2P_PEERS_CHANGED_ACTION;
    /** BroadcastReceiver P2P_CONNECTION_CHANGED_ACTION */
    private WDBR_P2P_CONNECTION_CHANGED_ACTION mWDBR_P2P_CONNECTION_CHANGED_ACTION;
    /** BroadcastReceiver THIS_DEVICE_CHANGED_ACTION */
    private WDBR_P2P_THIS_DEVICE_CHANGED_ACTION mWDBR_THIS_DEVICE_CHANGED_ACTION;

    /**
     * Wifi-Direct用BroadCast
     */
    /** BroadcastReceiver */
    private enum ReceiverState {
        All,
        StateChange,
        PeersChange,
        ConnectionChange,
        ThisDeviceChange,
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //DB登録するための処理
        Button insertBtn = (Button) findViewById(R.id.db_insert);
        insertBtn.setTag("insert");
        insertBtn.setOnClickListener(new DbButton());
        //DBを表示するための処理
        Button DisplayBtn = (Button) findViewById(R.id.db_show);
        DisplayBtn.setTag("display");
        DisplayBtn.setOnClickListener(new DbButton());
        //DBを表示するための処理
        Button DeleteBtn = (Button) findViewById(R.id.db_delete);
        DeleteBtn.setTag("Delete");
        DeleteBtn.setOnClickListener(new DbButton());
        //NanoHTTPDとWifiDirct起動
        Button serberstart =(Button)findViewById(R.id.server_Start);
        serberstart.setOnClickListener(new ServerStart());

        // DB作成
        helper = new CreateProductHelper(MainActivity.this);
        // DB作成
        helper02 = new CreateProductHelper(MainActivity.this);
        //WiFi Directの有効/無効状態
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        //デバイス情報の変更通知（通信可能なデバイスの発見・ロストなど）
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        //IPアドレスなどコネクション情報。通信状態の変更通知
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        //自分自身のデバイス状態の変更通知(相手デバイスではないことに注意)
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

//        WifiP2pManagerを取得し、initializeメソッドで初期化します。
//        WiFi機能を使うContext(ソースコードでは第1引数のthis)とスレッド(getMainLooper()、UIスレッド)を引数に、
//        channelを取得します。返り値のchannelは、P2P機能を利用するのに必要なインスタンスです。
//        ここではinitializeメソッドの第3引数はnullですが、リスナーを登録でき、必要に応じてWiFi Direct接続が切れた場合(channelが失われた時)の通知を受け取れます。

        mWifiP2pManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel = mWifiP2pManager.initialize(this, getMainLooper(), null);
    }

    /** register the BroadcastReceiver with the intent values to be matched */
    @Override
    public void onResume() {
        super.onResume();
        // ブロードキャストレシーバで、WIFI_P2P_STATE_CHANGED_ACTIONのコールバックを持ってWi-Fi Direct ON/OFFを判定する
        mIsWiFiDirectEnabled = false;
        // たぶんこのタイミングでブロードキャストレシーバを登録するのがbetter
        registerBroadcastReceiver(ReceiverState.All);
    }

    @Override
    public void onPause() {
        super.onPause();
        // ブロードキャストレシーバで、WIFI_P2P_STATE_CHANGED_ACTIONのコールバックを持ってWi-Fi Direct ON/OFFを判定する
        mIsWiFiDirectEnabled = false;
        // たぶんこのタイミングでブロードキャストレシーバを登録するのがbetter
        registerBroadcastReceiver(ReceiverState.All);
    }

    //Serverを作る処理
    class ServerStart implements View.OnClickListener {
        // onClickメソッド(ボタンクリック時イベントハンドラ)
        public void onClick(View v) {
//            new AlertDialog.Builder(MainActivity.this)
//                    .setTitle("NanoHttpd")
//                    .setMessage("起動しました")
//                    .setPositiveButton("OK", null)
//                    .show();
            try {
                WebServer wb =new WebServer();
                wb.start();
            } catch (IOException e) {
            }

            mWifiP2pManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
            Log.w("TAG", "　Result[" + (mWifiP2pManager != null) + "]");
            onClickInitialize(v);
            mWifiP2pManager.requestConnectionInfo(mChannel, new WifiP2pManager.ConnectionInfoListener() {
                // requestConnectionInfo()実行後、非同期応答あり
                public void onConnectionInfoAvailable(WifiP2pInfo info) {
                    if (info == null) {
                        formatedIpAddress = "nullpoint";
                        return;
                    }
                    Log.w("TAG", "onClickRequestConnection");
                    Log.w("TAG", "info:" + info.groupOwnerAddress);
                    if (info.groupOwnerAddress != null) {
                        formatedIpAddress = info.groupOwnerAddress.toString();
                        TextView textIpaddr = (TextView) findViewById(R.id.ip_addr);
                        textIpaddr.setText("Please access your browsers to http:/" + formatedIpAddress + ":" + 8080);
                    } else {
                        formatedIpAddress = null;
                    }
                }
            });
        }
    }

    /**
     * リスナアダプタ
     * WifiP2pManagerクラスの各メソッドは、WifiP2pManager.ActionListenerによって、メソッドの実行結果を知ることができる
     * ただし、successと出たのに失敗したり、failureと出たのに成功したりする
     */
    class ActionListenerAdapter implements WifiP2pManager.ActionListener {
        // 成功
        public void onSuccess() {
        }
        // 失敗
        public void onFailure(int reason) {
        }

    }
    /**
     * P2Pメソッド実行前のNULLチェック
     */
    private boolean isNull(boolean both) {
        if (mActionListenerAdapter == null) {
            mActionListenerAdapter = new ActionListenerAdapter();
        }
        if (!mIsWiFiDirectEnabled) {
            return true;
        }

        if (mWifiP2pManager == null) {
            return true;
        }
        if (both && (mChannel == null) ) {
            return true;
        }

        return false;
    }

    /**
     * 初期化
     */
    public void onClickInitialize(View view) {
        if (isNull(false)) { return; }

        mChannel = mWifiP2pManager.initialize(this, getMainLooper(), new WifiP2pManager.ChannelListener() {
            public void onChannelDisconnected() {
            }
        });

        Log.w("TAG","　Result["+(mChannel != null)+"]");
        onClickRemoveGroup(view);
    }

    /**
     * グループ作成
     */
    public void onClickCreateGroup(View view) {
        if (isNull(true)) { return; }

        mWifiP2pManager.createGroup(mChannel, mActionListenerAdapter);
        Log.w("TAG", "onClickCreateGroup");

        onClickRequestConnectionInfo(view);
    }

    /**
     * グループ削除
     */
    public void onClickRemoveGroup(View view) {
        if (isNull(true)) { return; }

        mWifiP2pManager.removeGroup(mChannel, mActionListenerAdapter);

        //このタイミングでリストとかを全部nullすればwifiを消せるようになるかも
        Log.w("TAG", "onClickRemoveGroup");
        onClickCreateGroup(view);
    }

    /**
     * 接続情報要求
     */
    public void onClickRequestConnectionInfo(View view) {

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        }

        if (isNull(true)) { return; }

        onClickRequestGroupInfo(view);
    }

    /**
     * グループ情報要求
     */
    public void onClickRequestGroupInfo(View view) {
        if (isNull(true)) { return; }

        mWifiP2pManager.requestGroupInfo(mChannel, new WifiP2pManager.GroupInfoListener() {
            // requestGroupInfo()実行後、非同期応答あり
            public void onGroupInfoAvailable(WifiP2pGroup group) {
                Log.w("TAG", "onClickGroup:" + group);
                if (group == null) {
                    return;
                }

                // パスワードは、G.O.のみ取得可能
                String pass = null;
                String ssid = null;
                if (group.isGroupOwner()) {
                    pass = group.getPassphrase();
                    ssid = group.getNetworkName();
                    TextView textSsid = (TextView) findViewById(R.id.ssid_addr);
                    textSsid.setText("URL: " + ssid);
                    TextView textPass = (TextView) findViewById(R.id.pw_addr);
                    textPass.setText("Pass Word: " + pass);
                } else {
                    pass = "Client Couldn't Get Password";
                }
            }
        });

    }

    /**
     * ブロードキャストレシーバ 全部
     */
    public class WiFiDirectBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String log = "onReceive() ["+action+"]";

            //WiFi Directの有効/無効
            if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
                mIsWiFiDirectEnabled = false;
                int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
                String sttStr;
                switch (state) {
                    case WifiP2pManager.WIFI_P2P_STATE_ENABLED:
                        mIsWiFiDirectEnabled = true;
                        sttStr = "ENABLED";
                        break;
                    case WifiP2pManager.WIFI_P2P_STATE_DISABLED:
                        sttStr = "DISABLED";
                        break;
                    default:
                        sttStr = "UNKNOWN";
                        break;
                }
                //changeBackgroundColor();
            } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
                // このタイミングでrequestPeers()を呼び出すと、peerの変化(ステータス変更とか)がわかる
                // 本テストアプリは、メソッド単位での実行をテストしたいので、ここではrequestPeers()を実行しない
            } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
                NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
                // networkInfo.toString()はCSV文字列(1行)を返す。そのままでは読みにくいので、カンマを改行へ変換する。
            } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
                WifiP2pDevice device = (WifiP2pDevice) intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE);
            }
        }
    }

    private void registerBroadcastReceiver(ReceiverState rs) {
        IntentFilter filter = new IntentFilter();

        switch (rs) {
            case All:
                filter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
                filter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
                filter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
                filter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
                mReceiver = new WiFiDirectBroadcastReceiver();
                registerReceiver(mReceiver, filter);
                break;

            case StateChange:
                filter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
                mWDBR_P2P_STATE_CHANGED_ACTION = new WDBR_P2P_STATE_CHANGED_ACTION();
                registerReceiver(mWDBR_P2P_STATE_CHANGED_ACTION, filter);
                break;

            case PeersChange:
                filter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
                mWDBR_P2P_PEERS_CHANGED_ACTION = new WDBR_P2P_PEERS_CHANGED_ACTION();
                registerReceiver(mWDBR_P2P_PEERS_CHANGED_ACTION, filter);
                break;

            case ConnectionChange:
                filter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
                mWDBR_P2P_CONNECTION_CHANGED_ACTION = new WDBR_P2P_CONNECTION_CHANGED_ACTION();
                registerReceiver(mWDBR_P2P_CONNECTION_CHANGED_ACTION, filter);
                break;

            case ThisDeviceChange:
                filter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
                mWDBR_THIS_DEVICE_CHANGED_ACTION = new WDBR_P2P_THIS_DEVICE_CHANGED_ACTION();
                registerReceiver(mWDBR_THIS_DEVICE_CHANGED_ACTION, filter);
                break;

            default:
                break;
        }
    }

    /**
     * ブロードキャストレシーバ P2P ON/OFF状態変更検知
     */
    public class WDBR_P2P_STATE_CHANGED_ACTION extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String log = "onReceive() ["+action+"]";

            if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
                mIsWiFiDirectEnabled = false;
                int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
                String sttStr;
                switch (state) {
                    case WifiP2pManager.WIFI_P2P_STATE_ENABLED:
                        mIsWiFiDirectEnabled = true;
                        sttStr = "ENABLED";
                        break;
                    case WifiP2pManager.WIFI_P2P_STATE_DISABLED:
                        sttStr = "DISABLED";
                        break;
                    default:
                        sttStr = "UNKNOWN";
                        break;
                }
                //changeBackgroundColor();
            }
        }
    }

    /**
     * ブロードキャストレシーバ 周辺のP2Pデバイス検出
     */
    public class WDBR_P2P_PEERS_CHANGED_ACTION extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String log = "onReceive() ["+action+"]";

            if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
                // このタイミングでrequestPeers()を呼び出すと、peerの変化(ステータス変更とか)がわかる
                // 本テストアプリは、メソッド単位での実行をテストしたいので、ここではrequestPeers()を実行しない
            }
        }
    }

    /**
     * ブロードキャストレシーバ 接続状態変更(CONNECT/DISCONNECT)検知
     */
    public class WDBR_P2P_CONNECTION_CHANGED_ACTION extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String log = "onReceive() ["+action+"]";

            if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
                NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
                // networkInfo.toString()はCSV文字列(1行)を返す。そのままでは読みにくいので、カンマを改行へ変換する。
            }
        }
    }

    /**
     * ブロードキャストレシーバ 自端末検知
     */
    public class WDBR_P2P_THIS_DEVICE_CHANGED_ACTION extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String log = "onReceive() ["+action+"]";

            if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
                WifiP2pDevice device = (WifiP2pDevice) intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE);
            }
        }
    }


    // DBの処理
    class DbButton implements View.OnClickListener {
        // onClickメソッド(ボタンクリック時イベントハンドラ)
        public void onClick(View v) {
            // テーブルレイアウトオブジェクト取得
            TableLayout tablelayout = (TableLayout) findViewById(R.id.tb_list);
            // タグの取得
            String tag = (String) v.getTag();
            // 該当DBオブジェクト取得
            db01 = helper.getWritableDatabase();
            // 該当DBオブジェクト取得
            db02 = helper02.getWritableDatabase();

            // 登録ボタンが押された場合
            if (tag.equals("insert")) {
                try {
                    // SQL文定義
                    //text not nullがテキストの登録intger not nullは数字の登録
                    //テーブル名product 県（txt）市町村(txt) 救援物資(txt)が記述されている
                    String sql
                            = "create table product (" +
                            "_id integer primary key autoincrement," +
                            "prefecture text not null," +
                            "city text not null," +
                            "help text not null);";

                    // SQL文定義
                    //text not nullがテキストの登録intger not nullは数字の登録
                    //テーブル名product02 その他（txt）優先度(txt)が記述されている
                    String sql02
                            = "create table product02 (" +
                            "_id integer primary key autoincrement," +
                            "other text not null," +
                            "priority text not null)";


                    // SQL実行
                    db01.execSQL(sql);
                    // SQL実行
                    db02.execSQL(sql02);
                    Log.d("DB登録チェック", "テーブル作成");
                } catch (Exception e) {
                    Log.e("ERROR", e.toString());
                    Log.d("DB登録チェック", "テーブル作成されてるらしいっすよ");
                }


                // データ登録
                try {
                    // トランザクション制御開始
                    db01.beginTransaction();
                    //DownloadPath
                    File file05 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                    //ファイル個数分書き込む
                    for (int i = 0; i < FileName.length - 1; i++) {
                        //Fileのpath
                        File path = new File(file05.getPath() + FileName[i]);
                        //Fileが存在するか調べる
                        if (path.exists()) {
                            Log.d("ファイル", "あります");
                            // Write
                            {
                                try {

                                    // 登録データ設定
                                    ContentValues val = new ContentValues();
                                    String msg;
                                    FileOutputStream stream
                                            = openFileOutput(FILE_NAME, MODE_APPEND);
                                    File inputFile = new File(String.valueOf(path));
                                    BufferedWriter out
                                            = new BufferedWriter(new OutputStreamWriter(stream));
                                    FileInputStream fis = new FileInputStream(inputFile);
                                    InputStreamReader isr = new InputStreamReader(fis, "UTF8");//UTF8 SHIFT_JIS
                                    BufferedReader br = new BufferedReader(isr);

                                    //ローカルファイル読み込み
                                    while ((msg = br.readLine()) != null) {
                                        //productの処理
                                        if (msg.indexOf("県名") != -1) {
                                            prename01 = HtmlTagRemover1(msg);
                                            System.out.println("表記:" + prename01);
                                            out.write("" + prename01);
                                            out.newLine();
                                            //第一引数テーブルの列目名、第二引数登録したい値
                                            //県名格納
                                            val.put("prefecture", prename01);
                                        }
                                        if (msg.indexOf("市町村名:") != -1) {
                                            cityname = HtmlTagRemover1(msg);
                                            System.out.println("表記:" + cityname);
                                            out.write("" + cityname);
                                            out.newLine();
                                            //第一引数テーブルの列目名、第二引数登録したい値
                                            //市町村格納
                                            val.put("city", cityname);
                                        }
                                        if (msg.indexOf("救援物資:") != -1) {
                                            helptxt = HtmlTagRemover1(msg);
                                            System.out.println("表記:" + helptxt);
                                            out.write("" + helptxt);
                                            out.newLine();
                                            val.put("help", helptxt);

                                            //valに格納した値をproductにぶち込む
                                            db01.insert("product", null, val);
                                            // コミット
                                            db01.setTransactionSuccessful();
                                            // トランザクション制御終了
                                            db01.endTransaction();

                                        }
                                        //product02の処理
                                        if (msg.indexOf("優先度") != -1) {
                                            prinumber = HtmlTagRemover1(msg);
                                            System.out.println("表記" + prinumber);
                                            out.write("" + prinumber);
                                            out.newLine();

                                        }

                                        if (msg.indexOf("備考") != -1) {
                                            othertxt = HtmlTagRemover1(msg);
                                            System.out.println("表記:" + othertxt);
                                            out.write("" + othertxt);
                                            out.newLine();
                                        }

                                    }
                                    out.close();
                                } catch (Exception e) {
                                }
                                // データ登録
                                try {
                                    // トランザクション制御開始
                                    db02.beginTransaction();
                                    // 登録データ設定
                                    ContentValues val02 = new ContentValues();
                                    //第一引数テーブルの列目名、第二引数登録したい値]
                                    //その他を格納
                                    val02.put("other", othertxt);
                                    //優先度を格納
                                    val02.put("priority", prinumber);

                                    // データ登録
                                    //テーブル名、値を設定していない場合にNULLを明示的に設定しない列名、登録データを設定したCntentValuesオブジェクト
                                    db02.insert("product02", null, val02);
                                    // コミット
                                    db02.setTransactionSuccessful();
                                    // トランザクション制御終了
                                    db02.endTransaction();
                                    // メッセージ設定
                                    //message += "データを登録しました！";
                                    Log.d("データ登録(product02)", "成功");
                                } catch (Exception e) {
                                    //message  = "データ登録に失敗しました！";
                                    Log.d("データ登録(product02)", "失敗");
                                    Log.e("ERROR", e.toString());
                                }

                            }
                        } else {
                            Log.d("ファイル", " ないです");
                        }
                    }
                    // メッセージ設定
                    //message += "データを登録しました！";
                    Log.d("データ登録(product01)", "成功");
                } catch (Exception e) {
                    //message  = "データ登録に失敗しました！";
                    Log.d("データ登録(product01)", "失敗");
                    Log.e("ERROR", e.toString());
                }

            }
            //DB二格納した値をTableLayoutを表示する
            else if (tag.equals("display")) {
                // データ取得
                try {
                    // 該当DBオブジェクト取得
                    db01 = helper.getReadableDatabase();
                    // 該当DBオブジェクト取得
                    db02 = helper02.getReadableDatabase();
                    //テーブルレイアウトを一度リセット
                    tablelayout.removeAllViews();
                    // 列名定義
                    String columns[] = {"prefecture", "city", "help"};
                    // 列名定義
                    String columns02[] = {"other", "priority"};

                    // データ取得
                    //テーブル名、取得する列名の配列、選択条件、？？、集計条件。選択条件。ソート条件
                    Cursor cursor = db01.query(
                            "product", columns, null, null, null, null, null);
                    //テーブル名、取得する列名の配列、選択条件、？？、集計条件。選択条件。ソート条件
                    Cursor cursor02 = db02.query(
                            "product02", columns02, null, null, null, null, null);

                    // テーブルレイアウトの表示範囲を設定
                    tablelayout.setStretchAllColumns(true);

                    // テーブル一覧のヘッダ部設定
                    TableRow headrow = new TableRow(MainActivity.this);

                    TextView headtxt1 = new TextView(MainActivity.this);
                    headtxt1.setText("県名");
                    headtxt1.setGravity(Gravity.CENTER_HORIZONTAL);
                    headtxt1.setWidth(10);

                    TextView headtxt2 = new TextView(MainActivity.this);
                    headtxt2.setText("地域");
                    headtxt2.setGravity(Gravity.CENTER_HORIZONTAL);
                    headtxt2.setWidth(10);

                    TextView headtxt3 = new TextView(MainActivity.this);
                    headtxt3.setText("支援物資");
                    headtxt3.setGravity(Gravity.CENTER_HORIZONTAL);
                    headtxt3.setWidth(10);

                    TextView headtxt4 = new TextView(MainActivity.this);
                    headtxt4.setText("備考");
                    headtxt4.setGravity(Gravity.CENTER_HORIZONTAL);
                    headtxt4.setWidth(10);

                    TextView headtxt5 = new TextView(MainActivity.this);
                    headtxt5.setText("優先度");
                    headtxt5.setGravity(Gravity.CENTER_HORIZONTAL);
                    headtxt5.setWidth(10);

                    //addView
                    headrow.addView(headtxt1);
                    headrow.addView(headtxt2);
                    headrow.addView(headtxt3);
                    headrow.addView(headtxt4);
                    headrow.addView(headtxt5);
                    //格納
                    tablelayout.addView(headrow);


                    // 取得したデータをテーブル明細部に設定
                    while (cursor.moveToNext()) {
                        //rowにテーブルレイアウトに入れる値を格納
                        TableRow row = new TableRow(MainActivity.this);
                        //県名
                        TextView PreText
                                = new TextView(MainActivity.this);
                        PreText.setGravity(Gravity.CENTER_HORIZONTAL);
                        PreText.setText(cursor.getString(0));

                        //市町村
                        TextView CityName = new TextView(MainActivity.this);
                        CityName.setGravity(Gravity.CENTER_HORIZONTAL);
                        CityName.setText(cursor.getString(1));

                        //救援物資
                        TextView HelpText = new TextView(MainActivity.this);
                        HelpText.setGravity(Gravity.CENTER_HORIZONTAL);
                        HelpText.setText(cursor.getString(2));


                        row.addView(PreText);
                        row.addView(CityName);
                        row.addView(HelpText);

                        // 取得したデータをテーブル明細部に設定
                        while (cursor02.moveToNext()) {
                            //その他
                            TextView othertxt02
                                    = new TextView(MainActivity.this);
                            othertxt02.setGravity(Gravity.CENTER_HORIZONTAL);
                            othertxt02.setText(cursor02.getString(0));
                            //優先度
                            TextView PriNumber = new TextView(MainActivity.this);
                            PriNumber.setGravity(Gravity.CENTER_HORIZONTAL);
                            PriNumber.setText(cursor02.getString(1));


                            // row.addView(othertxt02);
                            row.addView(othertxt02);
                            row.addView(PriNumber);

                            tablelayout.addView(row);
                            break;
                        }
                    }


                } catch (Exception e) {
                    Log.e("ERROR", e.toString());
                }
            }
            else if (tag.equals("Delete")) {
                try{
                    // トランザクション制御開始
                    db01.beginTransaction();

                    // データ削除
                    db01.delete("product", null, null);

                    // コミット
                    db01.setTransactionSuccessful();

                    // トランザクション制御終了
                    db01.endTransaction();
                    Log.d("削除","完了");

                }
                catch(Exception e){
                    // メッセージ設定
                    Log.e("ERROR",e.toString());
                }
                try{
                    // トランザクション制御開始
                    db02.beginTransaction();

                    // データ削除
                    db02.delete("product02", null, null);

                    // コミット
                    db02.setTransactionSuccessful();

                    // トランザクション制御終了
                    db02.endTransaction();
                    Log.d("削除","完了");

                }catch(Exception e){
                    // メッセージ設定
                    Log.e("ERROR",e.toString());
                }

            }


            // DBオブジェクトクローズ
            db01.close();
        }
    }

    public static String HtmlTagRemover1(String str) {
        // 文字列のすべてのタグを取り除く
        str = str.replaceAll("県名:", "");
        str = str.replaceAll("市町村名:", "");
        str = str.replaceAll("救援物資:", "");
        str = str.replaceAll("優先度:", "");
        return str.replaceAll("備考:", "");
    }

    private class WebServer extends NanoHTTPD {
        public WebServer() throws IOException {
            super(8080);
        }
        @Override
        public Response serve(IHTTPSession session) {

            Log.d("NanoHttod", "起動したよ！");
            String msg ="<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                    "<html xmlns='http://www.w3.org/1999/xhtml'>\n" +
                    "\n" +
                    "<head>\n" +
                    "    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />\n" +
                    "    <script type=\"text/javascript\">\n" +
                    "    /*! jQuery v3.0.0 | (c) jQuery Foundation | jquery.org/license */ ! function(a, b) {\n" +
                    "        \"use strict\";\n" +
                    "        \"object\" == typeof module && \"object\" == typeof module.exports ? module.exports = a.document ? b(a, !0) : function(a) {\n" +
                    "            if (!a.document) throw new Error(\"jQuery requires a window with a document\");\n" +
                    "            return b(a)\n" +
                    "        } : b(a)\n" +
                    "    }(\"undefined\" != typeof window ? window : this, function(a, b) {\n" +
                    "        \"use strict\";\n" +
                    "        var c = [],\n" +
                    "            d = a.document,\n" +
                    "            e = Object.getPrototypeOf,\n" +
                    "            f = c.slice,\n" +
                    "            g = c.concat,\n" +
                    "            h = c.push,\n" +
                    "            i = c.indexOf,\n" +
                    "            j = {},\n" +
                    "            k = j.toString,\n" +
                    "            l = j.hasOwnProperty,\n" +
                    "            m = l.toString,\n" +
                    "            n = m.call(Object),\n" +
                    "            o = {};\n" +
                    "\n" +
                    "        function p(a, b) {\n" +
                    "            b = b || d;\n" +
                    "            var c = b.createElement(\"script\");\n" +
                    "            c.text = a, b.head.appendChild(c).parentNode.removeChild(c)\n" +
                    "        }\n" +
                    "        var q = \"3.0.0\",\n" +
                    "            r = function(a, b) {\n" +
                    "                return new r.fn.init(a, b)\n" +
                    "            },\n" +
                    "            s = /^[\\s\\uFEFF\\xA0]+|[\\s\\uFEFF\\xA0]+$/g,\n" +
                    "            t = /^-ms-/,\n" +
                    "            u = /-([a-z])/g,\n" +
                    "            v = function(a, b) {\n" +
                    "                return b.toUpperCase()\n" +
                    "            };\n" +
                    "        r.fn = r.prototype = {\n" +
                    "            jquery: q,\n" +
                    "            constructor: r,\n" +
                    "            length: 0,\n" +
                    "            toArray: function() {\n" +
                    "                return f.call(this)\n" +
                    "            },\n" +
                    "            get: function(a) {\n" +
                    "                return null != a ? 0 > a ? this[a + this.length] : this[a] : f.call(this)\n" +
                    "            },\n" +
                    "            pushStack: function(a) {\n" +
                    "                var b = r.merge(this.constructor(), a);\n" +
                    "                return b.prevObject = this, b\n" +
                    "            },\n" +
                    "            each: function(a) {\n" +
                    "                return r.each(this, a)\n" +
                    "            },\n" +
                    "            map: function(a) {\n" +
                    "                return this.pushStack(r.map(this, function(b, c) {\n" +
                    "                    return a.call(b, c, b)\n" +
                    "                }))\n" +
                    "            },\n" +
                    "            slice: function() {\n" +
                    "                return this.pushStack(f.apply(this, arguments))\n" +
                    "            },\n" +
                    "            first: function() {\n" +
                    "                return this.eq(0)\n" +
                    "            },\n" +
                    "            last: function() {\n" +
                    "                return this.eq(-1)\n" +
                    "            },\n" +
                    "            eq: function(a) {\n" +
                    "                var b = this.length,\n" +
                    "                    c = +a + (0 > a ? b : 0);\n" +
                    "                return this.pushStack(c >= 0 && b > c ? [this[c]] : [])\n" +
                    "            },\n" +
                    "            end: function() {\n" +
                    "                return this.prevObject || this.constructor()\n" +
                    "            },\n" +
                    "            push: h,\n" +
                    "            sort: c.sort,\n" +
                    "            splice: c.splice\n" +
                    "        }, r.extend = r.fn.extend = function() {\n" +
                    "            var a, b, c, d, e, f, g = arguments[0] || {},\n" +
                    "                h = 1,\n" +
                    "                i = arguments.length,\n" +
                    "                j = !1;\n" +
                    "            for (\"boolean\" == typeof g && (j = g, g = arguments[h] || {}, h++), \"object\" == typeof g || r.isFunction(g) || (g = {}), h === i && (g = this, h--); i > h; h++)\n" +
                    "                if (null != (a = arguments[h]))\n" +
                    "                    for (b in a) c = g[b], d = a[b], g !== d && (j && d && (r.isPlainObject(d) || (e = r.isArray(d))) ? (e ? (e = !1, f = c && r.isArray(c) ? c : []) : f = c && r.isPlainObject(c) ? c : {}, g[b] = r.extend(j, f, d)) : void 0 !== d && (g[b] = d));\n" +
                    "            return g\n" +
                    "        }, r.extend({\n" +
                    "            expando: \"jQuery\" + (q + Math.random()).replace(/\\D/g, \"\"),\n" +
                    "            isReady: !0,\n" +
                    "            error: function(a) {\n" +
                    "                throw new Error(a)\n" +
                    "            },\n" +
                    "            noop: function() {},\n" +
                    "            isFunction: function(a) {\n" +
                    "                return \"function\" === r.type(a)\n" +
                    "            },\n" +
                    "            isArray: Array.isArray,\n" +
                    "            isWindow: function(a) {\n" +
                    "                return null != a && a === a.window\n" +
                    "            },\n" +
                    "            isNumeric: function(a) {\n" +
                    "                var b = r.type(a);\n" +
                    "                return (\"number\" === b || \"string\" === b) && !isNaN(a - parseFloat(a))\n" +
                    "            },\n" +
                    "            isPlainObject: function(a) {\n" +
                    "                var b, c;\n" +
                    "                return a && \"[object Object]\" === k.call(a) ? (b = e(a)) ? (c = l.call(b, \"constructor\") && b.constructor, \"function\" == typeof c && m.call(c) === n) : !0 : !1\n" +
                    "            },\n" +
                    "            isEmptyObject: function(a) {\n" +
                    "                var b;\n" +
                    "                for (b in a) return !1;\n" +
                    "                return !0\n" +
                    "            },\n" +
                    "            type: function(a) {\n" +
                    "                return null == a ? a + \"\" : \"object\" == typeof a || \"function\" == typeof a ? j[k.call(a)] || \"object\" : typeof a\n" +
                    "            },\n" +
                    "            globalEval: function(a) {\n" +
                    "                p(a)\n" +
                    "            },\n" +
                    "            camelCase: function(a) {\n" +
                    "                return a.replace(t, \"ms-\").replace(u, v)\n" +
                    "            },\n" +
                    "            nodeName: function(a, b) {\n" +
                    "                return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase()\n" +
                    "            },\n" +
                    "            each: function(a, b) {\n" +
                    "                var c, d = 0;\n" +
                    "                if (w(a)) {\n" +
                    "                    for (c = a.length; c > d; d++)\n" +
                    "                        if (b.call(a[d], d, a[d]) === !1) break\n" +
                    "                } else\n" +
                    "                    for (d in a)\n" +
                    "                        if (b.call(a[d], d, a[d]) === !1) break; return a\n" +
                    "            },\n" +
                    "            trim: function(a) {\n" +
                    "                return null == a ? \"\" : (a + \"\").replace(s, \"\")\n" +
                    "            },\n" +
                    "            makeArray: function(a, b) {\n" +
                    "                var c = b || [];\n" +
                    "                return null != a && (w(Object(a)) ? r.merge(c, \"string\" == typeof a ? [a] : a) : h.call(c, a)), c\n" +
                    "            },\n" +
                    "            inArray: function(a, b, c) {\n" +
                    "                return null == b ? -1 : i.call(b, a, c)\n" +
                    "            },\n" +
                    "            merge: function(a, b) {\n" +
                    "                for (var c = +b.length, d = 0, e = a.length; c > d; d++) a[e++] = b[d];\n" +
                    "                return a.length = e, a\n" +
                    "            },\n" +
                    "            grep: function(a, b, c) {\n" +
                    "                for (var d, e = [], f = 0, g = a.length, h = !c; g > f; f++) d = !b(a[f], f), d !== h && e.push(a[f]);\n" +
                    "                return e\n" +
                    "            },\n" +
                    "            map: function(a, b, c) {\n" +
                    "                var d, e, f = 0,\n" +
                    "                    h = [];\n" +
                    "                if (w(a))\n" +
                    "                    for (d = a.length; d > f; f++) e = b(a[f], f, c), null != e && h.push(e);\n" +
                    "                else\n" +
                    "                    for (f in a) e = b(a[f], f, c), null != e && h.push(e);\n" +
                    "                return g.apply([], h)\n" +
                    "            },\n" +
                    "            guid: 1,\n" +
                    "            proxy: function(a, b) {\n" +
                    "                var c, d, e;\n" +
                    "                return \"string\" == typeof b && (c = a[b], b = a, a = c), r.isFunction(a) ? (d = f.call(arguments, 2), e = function() {\n" +
                    "                    return a.apply(b || this, d.concat(f.call(arguments)))\n" +
                    "                }, e.guid = a.guid = a.guid || r.guid++, e) : void 0\n" +
                    "            },\n" +
                    "            now: Date.now,\n" +
                    "            support: o\n" +
                    "        }), \"function\" == typeof Symbol && (r.fn[Symbol.iterator] = c[Symbol.iterator]), r.each(\"Boolean Number String Function Array Date RegExp Object Error Symbol\".split(\" \"), function(a, b) {\n" +
                    "            j[\"[object \" + b + \"]\"] = b.toLowerCase()\n" +
                    "        });\n" +
                    "\n" +
                    "        function w(a) {\n" +
                    "            var b = !!a && \"length\" in a && a.length,\n" +
                    "                c = r.type(a);\n" +
                    "            return \"function\" === c || r.isWindow(a) ? !1 : \"array\" === c || 0 === b || \"number\" == typeof b && b > 0 && b - 1 in a\n" +
                    "        }\n" +
                    "        var x = function(a) {\n" +
                    "            var b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u = \"sizzle\" + 1 * new Date,\n" +
                    "                v = a.document,\n" +
                    "                w = 0,\n" +
                    "                x = 0,\n" +
                    "                y = ha(),\n" +
                    "                z = ha(),\n" +
                    "                A = ha(),\n" +
                    "                B = function(a, b) {\n" +
                    "                    return a === b && (l = !0), 0\n" +
                    "                },\n" +
                    "                C = {}.hasOwnProperty,\n" +
                    "                D = [],\n" +
                    "                E = D.pop,\n" +
                    "                F = D.push,\n" +
                    "                G = D.push,\n" +
                    "                H = D.slice,\n" +
                    "                I = function(a, b) {\n" +
                    "                    for (var c = 0, d = a.length; d > c; c++)\n" +
                    "                        if (a[c] === b) return c;\n" +
                    "                    return -1\n" +
                    "                },\n" +
                    "                J = \"checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped\",\n" +
                    "                K = \"[\\\\x20\\\\t\\\\r\\\\n\\\\f]\",\n" +
                    "                L = \"(?:\\\\\\\\.|[\\\\w-]|[^\\x00-\\\\xa0])+\",\n" +
                    "                M = \"\\\\[\" + K + \"*(\" + L + \")(?:\" + K + \"*([*^$|!~]?=)\" + K + \"*(?:'((?:\\\\\\\\.|[^\\\\\\\\'])*)'|\\\"((?:\\\\\\\\.|[^\\\\\\\\\\\"])*)\\\"|(\" + L + \"))|)\" + K + \"*\\\\]\",\n" +
                    "                N = \":(\" + L + \")(?:\\\\((('((?:\\\\\\\\.|[^\\\\\\\\'])*)'|\\\"((?:\\\\\\\\.|[^\\\\\\\\\\\"])*)\\\")|((?:\\\\\\\\.|[^\\\\\\\\()[\\\\]]|\" + M + \")*)|.*)\\\\)|)\",\n" +
                    "                O = new RegExp(K + \"+\", \"g\"),\n" +
                    "                P = new RegExp(\"^\" + K + \"+|((?:^|[^\\\\\\\\])(?:\\\\\\\\.)*)\" + K + \"+$\", \"g\"),\n" +
                    "                Q = new RegExp(\"^\" + K + \"*,\" + K + \"*\"),\n" +
                    "                R = new RegExp(\"^\" + K + \"*([>+~]|\" + K + \")\" + K + \"*\"),\n" +
                    "                S = new RegExp(\"=\" + K + \"*([^\\\\]'\\\"]*?)\" + K + \"*\\\\]\", \"g\"),\n" +
                    "                T = new RegExp(N),\n" +
                    "                U = new RegExp(\"^\" + L + \"$\"),\n" +
                    "                V = {\n" +
                    "                    ID: new RegExp(\"^#(\" + L + \")\"),\n" +
                    "                    CLASS: new RegExp(\"^\\\\.(\" + L + \")\"),\n" +
                    "                    TAG: new RegExp(\"^(\" + L + \"|[*])\"),\n" +
                    "                    ATTR: new RegExp(\"^\" + M),\n" +
                    "                    PSEUDO: new RegExp(\"^\" + N),\n" +
                    "                    CHILD: new RegExp(\"^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\\\(\" + K + \"*(even|odd|(([+-]|)(\\\\d*)n|)\" + K + \"*(?:([+-]|)\" + K + \"*(\\\\d+)|))\" + K + \"*\\\\)|)\", \"i\"),\n" +
                    "                    bool: new RegExp(\"^(?:\" + J + \")$\", \"i\"),\n" +
                    "                    needsContext: new RegExp(\"^\" + K + \"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\\\(\" + K + \"*((?:-\\\\d)?\\\\d*)\" + K + \"*\\\\)|)(?=[^-]|$)\", \"i\")\n" +
                    "                },\n" +
                    "                W = /^(?:input|select|textarea|button)$/i,\n" +
                    "                X = /^h\\d$/i,\n" +
                    "                Y = /^[^{]+\\{\\s*\\[native \\w/,\n" +
                    "                Z = /^(?:#([\\w-]+)|(\\w+)|\\.([\\w-]+))$/,\n" +
                    "                $ = /[+~]/,\n" +
                    "                _ = new RegExp(\"\\\\\\\\([\\\\da-f]{1,6}\" + K + \"?|(\" + K + \")|.)\", \"ig\"),\n" +
                    "                aa = function(a, b, c) {\n" +
                    "                    var d = \"0x\" + b - 65536;\n" +
                    "                    return d !== d || c ? b : 0 > d ? String.fromCharCode(d + 65536) : String.fromCharCode(d >> 10 | 55296, 1023 & d | 56320)\n" +
                    "                },\n" +
                    "                ba = /([\\0-\\x1f\\x7f]|^-?\\d)|^-$|[^\\x80-\\uFFFF\\w-]/g,\n" +
                    "                ca = function(a, b) {\n" +
                    "                    return b ? \"\\x00\" === a ? \"\\ufffd\" : a.slice(0, -1) + \"\\\\\" + a.charCodeAt(a.length - 1).toString(16) + \" \" : \"\\\\\" + a\n" +
                    "                },\n" +
                    "                da = function() {\n" +
                    "                    m()\n" +
                    "                },\n" +
                    "                ea = ta(function(a) {\n" +
                    "                    return a.disabled === !0\n" +
                    "                }, {\n" +
                    "                    dir: \"parentNode\",\n" +
                    "                    next: \"legend\"\n" +
                    "                });\n" +
                    "            try {\n" +
                    "                G.apply(D = H.call(v.childNodes), v.childNodes), D[v.childNodes.length].nodeType\n" +
                    "            } catch (fa) {\n" +
                    "                G = {\n" +
                    "                    apply: D.length ? function(a, b) {\n" +
                    "                        F.apply(a, H.call(b))\n" +
                    "                    } : function(a, b) {\n" +
                    "                        var c = a.length,\n" +
                    "                            d = 0;\n" +
                    "                        while (a[c++] = b[d++]);\n" +
                    "                        a.length = c - 1\n" +
                    "                    }\n" +
                    "                }\n" +
                    "            }\n" +
                    "\n" +
                    "            function ga(a, b, d, e) {\n" +
                    "                var f, h, j, k, l, o, r, s = b && b.ownerDocument,\n" +
                    "                    w = b ? b.nodeType : 9;\n" +
                    "                if (d = d || [], \"string\" != typeof a || !a || 1 !== w && 9 !== w && 11 !== w) return d;\n" +
                    "                if (!e && ((b ? b.ownerDocument || b : v) !== n && m(b), b = b || n, p)) {\n" +
                    "                    if (11 !== w && (l = Z.exec(a)))\n" +
                    "                        if (f = l[1]) {\n" +
                    "                            if (9 === w) {\n" +
                    "                                if (!(j = b.getElementById(f))) return d;\n" +
                    "                                if (j.id === f) return d.push(j), d\n" +
                    "                            } else if (s && (j = s.getElementById(f)) && t(b, j) && j.id === f) return d.push(j), d\n" +
                    "                        } else {\n" +
                    "                            if (l[2]) return G.apply(d, b.getElementsByTagName(a)), d;\n" +
                    "                            if ((f = l[3]) && c.getElementsByClassName && b.getElementsByClassName) return G.apply(d, b.getElementsByClassName(f)), d\n" +
                    "                        }\n" +
                    "                    if (c.qsa && !A[a + \" \"] && (!q || !q.test(a))) {\n" +
                    "                        if (1 !== w) s = b, r = a;\n" +
                    "                        else if (\"object\" !== b.nodeName.toLowerCase()) {\n" +
                    "                            (k = b.getAttribute(\"id\")) ? k = k.replace(ba, ca): b.setAttribute(\"id\", k = u), o = g(a), h = o.length;\n" +
                    "                            while (h--) o[h] = \"#\" + k + \" \" + sa(o[h]);\n" +
                    "                            r = o.join(\",\"), s = $.test(a) && qa(b.parentNode) || b\n" +
                    "                        }\n" +
                    "                        if (r) try {\n" +
                    "                            return G.apply(d, s.querySelectorAll(r)), d\n" +
                    "                        } catch (x) {} finally {\n" +
                    "                            k === u && b.removeAttribute(\"id\")\n" +
                    "                        }\n" +
                    "                    }\n" +
                    "                }\n" +
                    "                return i(a.replace(P, \"$1\"), b, d, e)\n" +
                    "            }\n" +
                    "\n" +
                    "            function ha() {\n" +
                    "                var a = [];\n" +
                    "\n" +
                    "                function b(c, e) {\n" +
                    "                    return a.push(c + \" \") > d.cacheLength && delete b[a.shift()], b[c + \" \"] = e\n" +
                    "                }\n" +
                    "                return b\n" +
                    "            }\n" +
                    "\n" +
                    "            function ia(a) {\n" +
                    "                return a[u] = !0, a\n" +
                    "            }\n" +
                    "\n" +
                    "            function ja(a) {\n" +
                    "                var b = n.createElement(\"fieldset\");\n" +
                    "                try {\n" +
                    "                    return !!a(b)\n" +
                    "                } catch (c) {\n" +
                    "                    return !1\n" +
                    "                } finally {\n" +
                    "                    b.parentNode && b.parentNode.removeChild(b), b = null\n" +
                    "                }\n" +
                    "            }\n" +
                    "\n" +
                    "            function ka(a, b) {\n" +
                    "                var c = a.split(\"|\"),\n" +
                    "                    e = c.length;\n" +
                    "                while (e--) d.attrHandle[c[e]] = b\n" +
                    "            }\n" +
                    "\n" +
                    "            function la(a, b) {\n" +
                    "                var c = b && a,\n" +
                    "                    d = c && 1 === a.nodeType && 1 === b.nodeType && a.sourceIndex - b.sourceIndex;\n" +
                    "                if (d) return d;\n" +
                    "                if (c)\n" +
                    "                    while (c = c.nextSibling)\n" +
                    "                        if (c === b) return -1;\n" +
                    "                return a ? 1 : -1\n" +
                    "            }\n" +
                    "\n" +
                    "            function ma(a) {\n" +
                    "                return function(b) {\n" +
                    "                    var c = b.nodeName.toLowerCase();\n" +
                    "                    return \"input\" === c && b.type === a\n" +
                    "                }\n" +
                    "            }\n" +
                    "\n" +
                    "            function na(a) {\n" +
                    "                return function(b) {\n" +
                    "                    var c = b.nodeName.toLowerCase();\n" +
                    "                    return (\"input\" === c || \"button\" === c) && b.type === a\n" +
                    "                }\n" +
                    "            }\n" +
                    "\n" +
                    "            function oa(a) {\n" +
                    "                return function(b) {\n" +
                    "                    return \"label\" in b && b.disabled === a || \"form\" in b && b.disabled === a || \"form\" in b && b.disabled === !1 && (b.isDisabled === a || b.isDisabled !== !a && (\"label\" in b || !ea(b)) !== a)\n" +
                    "                }\n" +
                    "            }\n" +
                    "\n" +
                    "            function pa(a) {\n" +
                    "                return ia(function(b) {\n" +
                    "                    return b = +b, ia(function(c, d) {\n" +
                    "                        var e, f = a([], c.length, b),\n" +
                    "                            g = f.length;\n" +
                    "                        while (g--) c[e = f[g]] && (c[e] = !(d[e] = c[e]))\n" +
                    "                    })\n" +
                    "                })\n" +
                    "            }\n" +
                    "\n" +
                    "            function qa(a) {\n" +
                    "                return a && \"undefined\" != typeof a.getElementsByTagName && a\n" +
                    "            }\n" +
                    "            c = ga.support = {}, f = ga.isXML = function(a) {\n" +
                    "                var b = a && (a.ownerDocument || a).documentElement;\n" +
                    "                return b ? \"HTML\" !== b.nodeName : !1\n" +
                    "            }, m = ga.setDocument = function(a) {\n" +
                    "                var b, e, g = a ? a.ownerDocument || a : v;\n" +
                    "                return g !== n && 9 === g.nodeType && g.documentElement ? (n = g, o = n.documentElement, p = !f(n), v !== n && (e = n.defaultView) && e.top !== e && (e.addEventListener ? e.addEventListener(\"unload\", da, !1) : e.attachEvent && e.attachEvent(\"onunload\", da)), c.attributes = ja(function(a) {\n" +
                    "                    return a.className = \"i\", !a.getAttribute(\"className\")\n" +
                    "                }), c.getElementsByTagName = ja(function(a) {\n" +
                    "                    return a.appendChild(n.createComment(\"\")), !a.getElementsByTagName(\"*\").length\n" +
                    "                }), c.getElementsByClassName = Y.test(n.getElementsByClassName), c.getById = ja(function(a) {\n" +
                    "                    return o.appendChild(a).id = u, !n.getElementsByName || !n.getElementsByName(u).length\n" +
                    "                }), c.getById ? (d.find.ID = function(a, b) {\n" +
                    "                    if (\"undefined\" != typeof b.getElementById && p) {\n" +
                    "                        var c = b.getElementById(a);\n" +
                    "                        return c ? [c] : []\n" +
                    "                    }\n" +
                    "                }, d.filter.ID = function(a) {\n" +
                    "                    var b = a.replace(_, aa);\n" +
                    "                    return function(a) {\n" +
                    "                        return a.getAttribute(\"id\") === b\n" +
                    "                    }\n" +
                    "                }) : (delete d.find.ID, d.filter.ID = function(a) {\n" +
                    "                    var b = a.replace(_, aa);\n" +
                    "                    return function(a) {\n" +
                    "                        var c = \"undefined\" != typeof a.getAttributeNode && a.getAttributeNode(\"id\");\n" +
                    "                        return c && c.value === b\n" +
                    "                    }\n" +
                    "                }), d.find.TAG = c.getElementsByTagName ? function(a, b) {\n" +
                    "                    return \"undefined\" != typeof b.getElementsByTagName ? b.getElementsByTagName(a) : c.qsa ? b.querySelectorAll(a) : void 0\n" +
                    "                } : function(a, b) {\n" +
                    "                    var c, d = [],\n" +
                    "                        e = 0,\n" +
                    "                        f = b.getElementsByTagName(a);\n" +
                    "                    if (\"*\" === a) {\n" +
                    "                        while (c = f[e++]) 1 === c.nodeType && d.push(c);\n" +
                    "                        return d\n" +
                    "                    }\n" +
                    "                    return f\n" +
                    "                }, d.find.CLASS = c.getElementsByClassName && function(a, b) {\n" +
                    "                    return \"undefined\" != typeof b.getElementsByClassName && p ? b.getElementsByClassName(a) : void 0\n" +
                    "                }, r = [], q = [], (c.qsa = Y.test(n.querySelectorAll)) && (ja(function(a) {\n" +
                    "                    o.appendChild(a).innerHTML = \"<a id='\" + u + \"'></a><select id='\" + u + \"-\\r\\\\' msallowcapture=''><option selected=''></option></select>\", a.querySelectorAll(\"[msallowcapture^='']\").length && q.push(\"[*^$]=\" + K + \"*(?:''|\\\"\\\")\"), a.querySelectorAll(\"[selected]\").length || q.push(\"\\\\[\" + K + \"*(?:value|\" + J + \")\"), a.querySelectorAll(\"[id~=\" + u + \"-]\").length || q.push(\"~=\"), a.querySelectorAll(\":checked\").length || q.push(\":checked\"), a.querySelectorAll(\"a#\" + u + \"+*\").length || q.push(\".#.+[+~]\")\n" +
                    "                }), ja(function(a) {\n" +
                    "                    a.innerHTML = \"<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>\";\n" +
                    "                    var b = n.createElement(\"input\");\n" +
                    "                    b.setAttribute(\"type\", \"hidden\"), a.appendChild(b).setAttribute(\"name\", \"D\"), a.querySelectorAll(\"[name=d]\").length && q.push(\"name\" + K + \"*[*^$|!~]?=\"), 2 !== a.querySelectorAll(\":enabled\").length && q.push(\":enabled\", \":disabled\"), o.appendChild(a).disabled = !0, 2 !== a.querySelectorAll(\":disabled\").length && q.push(\":enabled\", \":disabled\"), a.querySelectorAll(\"*,:x\"), q.push(\",.*:\")\n" +
                    "                })), (c.matchesSelector = Y.test(s = o.matches || o.webkitMatchesSelector || o.mozMatchesSelector || o.oMatchesSelector || o.msMatchesSelector)) && ja(function(a) {\n" +
                    "                    c.disconnectedMatch = s.call(a, \"*\"), s.call(a, \"[s!='']:x\"), r.push(\"!=\", N)\n" +
                    "                }), q = q.length && new RegExp(q.join(\"|\")), r = r.length && new RegExp(r.join(\"|\")), b = Y.test(o.compareDocumentPosition), t = b || Y.test(o.contains) ? function(a, b) {\n" +
                    "                    var c = 9 === a.nodeType ? a.documentElement : a,\n" +
                    "                        d = b && b.parentNode;\n" +
                    "                    return a === d || !(!d || 1 !== d.nodeType || !(c.contains ? c.contains(d) : a.compareDocumentPosition && 16 & a.compareDocumentPosition(d)))\n" +
                    "                } : function(a, b) {\n" +
                    "                    if (b)\n" +
                    "                        while (b = b.parentNode)\n" +
                    "                            if (b === a) return !0;\n" +
                    "                    return !1\n" +
                    "                }, B = b ? function(a, b) {\n" +
                    "                    if (a === b) return l = !0, 0;\n" +
                    "                    var d = !a.compareDocumentPosition - !b.compareDocumentPosition;\n" +
                    "                    return d ? d : (d = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1, 1 & d || !c.sortDetached && b.compareDocumentPosition(a) === d ? a === n || a.ownerDocument === v && t(v, a) ? -1 : b === n || b.ownerDocument === v && t(v, b) ? 1 : k ? I(k, a) - I(k, b) : 0 : 4 & d ? -1 : 1)\n" +
                    "                } : function(a, b) {\n" +
                    "                    if (a === b) return l = !0, 0;\n" +
                    "                    var c, d = 0,\n" +
                    "                        e = a.parentNode,\n" +
                    "                        f = b.parentNode,\n" +
                    "                        g = [a],\n" +
                    "                        h = [b];\n" +
                    "                    if (!e || !f) return a === n ? -1 : b === n ? 1 : e ? -1 : f ? 1 : k ? I(k, a) - I(k, b) : 0;\n" +
                    "                    if (e === f) return la(a, b);\n" +
                    "                    c = a;\n" +
                    "                    while (c = c.parentNode) g.unshift(c);\n" +
                    "                    c = b;\n" +
                    "                    while (c = c.parentNode) h.unshift(c);\n" +
                    "                    while (g[d] === h[d]) d++;\n" +
                    "                    return d ? la(g[d], h[d]) : g[d] === v ? -1 : h[d] === v ? 1 : 0\n" +
                    "                }, n) : n\n" +
                    "            }, ga.matches = function(a, b) {\n" +
                    "                return ga(a, null, null, b)\n" +
                    "            }, ga.matchesSelector = function(a, b) {\n" +
                    "                if ((a.ownerDocument || a) !== n && m(a), b = b.replace(S, \"='$1']\"), c.matchesSelector && p && !A[b + \" \"] && (!r || !r.test(b)) && (!q || !q.test(b))) try {\n" +
                    "                    var d = s.call(a, b);\n" +
                    "                    if (d || c.disconnectedMatch || a.document && 11 !== a.document.nodeType) return d\n" +
                    "                } catch (e) {}\n" +
                    "                return ga(b, n, null, [a]).length > 0\n" +
                    "            }, ga.contains = function(a, b) {\n" +
                    "                return (a.ownerDocument || a) !== n && m(a), t(a, b)\n" +
                    "            }, ga.attr = function(a, b) {\n" +
                    "                (a.ownerDocument || a) !== n && m(a);\n" +
                    "                var e = d.attrHandle[b.toLowerCase()],\n" +
                    "                    f = e && C.call(d.attrHandle, b.toLowerCase()) ? e(a, b, !p) : void 0;\n" +
                    "                return void 0 !== f ? f : c.attributes || !p ? a.getAttribute(b) : (f = a.getAttributeNode(b)) && f.specified ? f.value : null\n" +
                    "            }, ga.escape = function(a) {\n" +
                    "                return (a + \"\").replace(ba, ca)\n" +
                    "            }, ga.error = function(a) {\n" +
                    "                throw new Error(\"Syntax error, unrecognized expression: \" + a)\n" +
                    "            }, ga.uniqueSort = function(a) {\n" +
                    "                var b, d = [],\n" +
                    "                    e = 0,\n" +
                    "                    f = 0;\n" +
                    "                if (l = !c.detectDuplicates, k = !c.sortStable && a.slice(0), a.sort(B), l) {\n" +
                    "                    while (b = a[f++]) b === a[f] && (e = d.push(f));\n" +
                    "                    while (e--) a.splice(d[e], 1)\n" +
                    "                }\n" +
                    "                return k = null, a\n" +
                    "            }, e = ga.getText = function(a) {\n" +
                    "                var b, c = \"\",\n" +
                    "                    d = 0,\n" +
                    "                    f = a.nodeType;\n" +
                    "                if (f) {\n" +
                    "                    if (1 === f || 9 === f || 11 === f) {\n" +
                    "                        if (\"string\" == typeof a.textContent) return a.textContent;\n" +
                    "                        for (a = a.firstChild; a; a = a.nextSibling) c += e(a)\n" +
                    "                    } else if (3 === f || 4 === f) return a.nodeValue\n" +
                    "                } else\n" +
                    "                    while (b = a[d++]) c += e(b);\n" +
                    "                return c\n" +
                    "            }, d = ga.selectors = {\n" +
                    "                cacheLength: 50,\n" +
                    "                createPseudo: ia,\n" +
                    "                match: V,\n" +
                    "                attrHandle: {},\n" +
                    "                find: {},\n" +
                    "                relative: {\n" +
                    "                    \">\": {\n" +
                    "                        dir: \"parentNode\",\n" +
                    "                        first: !0\n" +
                    "                    },\n" +
                    "                    \" \": {\n" +
                    "                        dir: \"parentNode\"\n" +
                    "                    },\n" +
                    "                    \"+\": {\n" +
                    "                        dir: \"previousSibling\",\n" +
                    "                        first: !0\n" +
                    "                    },\n" +
                    "                    \"~\": {\n" +
                    "                        dir: \"previousSibling\"\n" +
                    "                    }\n" +
                    "                },\n" +
                    "                preFilter: {\n" +
                    "                    ATTR: function(a) {\n" +
                    "                        return a[1] = a[1].replace(_, aa), a[3] = (a[3] || a[4] || a[5] || \"\").replace(_, aa), \"~=\" === a[2] && (a[3] = \" \" + a[3] + \" \"), a.slice(0, 4)\n" +
                    "                    },\n" +
                    "                    CHILD: function(a) {\n" +
                    "                        return a[1] = a[1].toLowerCase(), \"nth\" === a[1].slice(0, 3) ? (a[3] || ga.error(a[0]), a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * (\"even\" === a[3] || \"odd\" === a[3])), a[5] = +(a[7] + a[8] || \"odd\" === a[3])) : a[3] && ga.error(a[0]), a\n" +
                    "                    },\n" +
                    "                    PSEUDO: function(a) {\n" +
                    "                        var b, c = !a[6] && a[2];\n" +
                    "                        return V.CHILD.test(a[0]) ? null : (a[3] ? a[2] = a[4] || a[5] || \"\" : c && T.test(c) && (b = g(c, !0)) && (b = c.indexOf(\")\", c.length - b) - c.length) && (a[0] = a[0].slice(0, b), a[2] = c.slice(0, b)), a.slice(0, 3))\n" +
                    "                    }\n" +
                    "                },\n" +
                    "                filter: {\n" +
                    "                    TAG: function(a) {\n" +
                    "                        var b = a.replace(_, aa).toLowerCase();\n" +
                    "                        return \"*\" === a ? function() {\n" +
                    "                            return !0\n" +
                    "                        } : function(a) {\n" +
                    "                            return a.nodeName && a.nodeName.toLowerCase() === b\n" +
                    "                        }\n" +
                    "                    },\n" +
                    "                    CLASS: function(a) {\n" +
                    "                        var b = y[a + \" \"];\n" +
                    "                        return b || (b = new RegExp(\"(^|\" + K + \")\" + a + \"(\" + K + \"|$)\")) && y(a, function(a) {\n" +
                    "                            return b.test(\"string\" == typeof a.className && a.className || \"undefined\" != typeof a.getAttribute && a.getAttribute(\"class\") || \"\")\n" +
                    "                        })\n" +
                    "                    },\n" +
                    "                    ATTR: function(a, b, c) {\n" +
                    "                        return function(d) {\n" +
                    "                            var e = ga.attr(d, a);\n" +
                    "                            return null == e ? \"!=\" === b : b ? (e += \"\", \"=\" === b ? e === c : \"!=\" === b ? e !== c : \"^=\" === b ? c && 0 === e.indexOf(c) : \"*=\" === b ? c && e.indexOf(c) > -1 : \"$=\" === b ? c && e.slice(-c.length) === c : \"~=\" === b ? (\" \" + e.replace(O, \" \") + \" \").indexOf(c) > -1 : \"|=\" === b ? e === c || e.slice(0, c.length + 1) === c + \"-\" : !1) : !0\n" +
                    "                        }\n" +
                    "                    },\n" +
                    "                    CHILD: function(a, b, c, d, e) {\n" +
                    "                        var f = \"nth\" !== a.slice(0, 3),\n" +
                    "                            g = \"last\" !== a.slice(-4),\n" +
                    "                            h = \"of-type\" === b;\n" +
                    "                        return 1 === d && 0 === e ? function(a) {\n" +
                    "                            return !!a.parentNode\n" +
                    "                        } : function(b, c, i) {\n" +
                    "                            var j, k, l, m, n, o, p = f !== g ? \"nextSibling\" : \"previousSibling\",\n" +
                    "                                q = b.parentNode,\n" +
                    "                                r = h && b.nodeName.toLowerCase(),\n" +
                    "                                s = !i && !h,\n" +
                    "                                t = !1;\n" +
                    "                            if (q) {\n" +
                    "                                if (f) {\n" +
                    "                                    while (p) {\n" +
                    "                                        m = b;\n" +
                    "                                        while (m = m[p])\n" +
                    "                                            if (h ? m.nodeName.toLowerCase() === r : 1 === m.nodeType) return !1;\n" +
                    "                                        o = p = \"only\" === a && !o && \"nextSibling\"\n" +
                    "                                    }\n" +
                    "                                    return !0\n" +
                    "                                }\n" +
                    "                                if (o = [g ? q.firstChild : q.lastChild], g && s) {\n" +
                    "                                    m = q, l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), j = k[a] || [], n = j[0] === w && j[1], t = n && j[2], m = n && q.childNodes[n];\n" +
                    "                                    while (m = ++n && m && m[p] || (t = n = 0) || o.pop())\n" +
                    "                                        if (1 === m.nodeType && ++t && m === b) {\n" +
                    "                                            k[a] = [w, n, t];\n" +
                    "                                            break\n" +
                    "                                        }\n" +
                    "                                } else if (s && (m = b, l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), j = k[a] || [], n = j[0] === w && j[1], t = n), t === !1)\n" +
                    "                                    while (m = ++n && m && m[p] || (t = n = 0) || o.pop())\n" +
                    "                                        if ((h ? m.nodeName.toLowerCase() === r : 1 === m.nodeType) && ++t && (s && (l = m[u] || (m[u] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), k[a] = [w, t]), m === b)) break;\n" +
                    "                                return t -= e, t === d || t % d === 0 && t / d >= 0\n" +
                    "                            }\n" +
                    "                        }\n" +
                    "                    },\n" +
                    "                    PSEUDO: function(a, b) {\n" +
                    "                        var c, e = d.pseudos[a] || d.setFilters[a.toLowerCase()] || ga.error(\"unsupported pseudo: \" + a);\n" +
                    "                        return e[u] ? e(b) : e.length > 1 ? (c = [a, a, \"\", b], d.setFilters.hasOwnProperty(a.toLowerCase()) ? ia(function(a, c) {\n" +
                    "                            var d, f = e(a, b),\n" +
                    "                                g = f.length;\n" +
                    "                            while (g--) d = I(a, f[g]), a[d] = !(c[d] = f[g])\n" +
                    "                        }) : function(a) {\n" +
                    "                            return e(a, 0, c)\n" +
                    "                        }) : e\n" +
                    "                    }\n" +
                    "                },\n" +
                    "                pseudos: {\n" +
                    "                    not: ia(function(a) {\n" +
                    "                        var b = [],\n" +
                    "                            c = [],\n" +
                    "                            d = h(a.replace(P, \"$1\"));\n" +
                    "                        return d[u] ? ia(function(a, b, c, e) {\n" +
                    "                            var f, g = d(a, null, e, []),\n" +
                    "                                h = a.length;\n" +
                    "                            while (h--)(f = g[h]) && (a[h] = !(b[h] = f))\n" +
                    "                        }) : function(a, e, f) {\n" +
                    "                            return b[0] = a, d(b, null, f, c), b[0] = null, !c.pop()\n" +
                    "                        }\n" +
                    "                    }),\n" +
                    "                    has: ia(function(a) {\n" +
                    "                        return function(b) {\n" +
                    "                            return ga(a, b).length > 0\n" +
                    "                        }\n" +
                    "                    }),\n" +
                    "                    contains: ia(function(a) {\n" +
                    "                        return a = a.replace(_, aa),\n" +
                    "                            function(b) {\n" +
                    "                                return (b.textContent || b.innerText || e(b)).indexOf(a) > -1\n" +
                    "                            }\n" +
                    "                    }),\n" +
                    "                    lang: ia(function(a) {\n" +
                    "                        return U.test(a || \"\") || ga.error(\"unsupported lang: \" + a), a = a.replace(_, aa).toLowerCase(),\n" +
                    "                            function(b) {\n" +
                    "                                var c;\n" +
                    "                                do\n" +
                    "                                    if (c = p ? b.lang : b.getAttribute(\"xml:lang\") || b.getAttribute(\"lang\")) return c = c.toLowerCase(), c === a || 0 === c.indexOf(a + \"-\");\n" +
                    "                                while ((b = b.parentNode) && 1 === b.nodeType);\n" +
                    "                                return !1\n" +
                    "                            }\n" +
                    "                    }),\n" +
                    "                    target: function(b) {\n" +
                    "                        var c = a.location && a.location.hash;\n" +
                    "                        return c && c.slice(1) === b.id\n" +
                    "                    },\n" +
                    "                    root: function(a) {\n" +
                    "                        return a === o\n" +
                    "                    },\n" +
                    "                    focus: function(a) {\n" +
                    "                        return a === n.activeElement && (!n.hasFocus || n.hasFocus()) && !!(a.type || a.href || ~a.tabIndex)\n" +
                    "                    },\n" +
                    "                    enabled: oa(!1),\n" +
                    "                    disabled: oa(!0),\n" +
                    "                    checked: function(a) {\n" +
                    "                        var b = a.nodeName.toLowerCase();\n" +
                    "                        return \"input\" === b && !!a.checked || \"option\" === b && !!a.selected\n" +
                    "                    },\n" +
                    "                    selected: function(a) {\n" +
                    "                        return a.parentNode && a.parentNode.selectedIndex, a.selected === !0\n" +
                    "                    },\n" +
                    "                    empty: function(a) {\n" +
                    "                        for (a = a.firstChild; a; a = a.nextSibling)\n" +
                    "                            if (a.nodeType < 6) return !1;\n" +
                    "                        return !0\n" +
                    "                    },\n" +
                    "                    parent: function(a) {\n" +
                    "                        return !d.pseudos.empty(a)\n" +
                    "                    },\n" +
                    "                    header: function(a) {\n" +
                    "                        return X.test(a.nodeName)\n" +
                    "                    },\n" +
                    "                    input: function(a) {\n" +
                    "                        return W.test(a.nodeName)\n" +
                    "                    },\n" +
                    "                    button: function(a) {\n" +
                    "                        var b = a.nodeName.toLowerCase();\n" +
                    "                        return \"input\" === b && \"button\" === a.type || \"button\" === b\n" +
                    "                    },\n" +
                    "                    text: function(a) {\n" +
                    "                        var b;\n" +
                    "                        return \"input\" === a.nodeName.toLowerCase() && \"text\" === a.type && (null == (b = a.getAttribute(\"type\")) || \"text\" === b.toLowerCase())\n" +
                    "                    },\n" +
                    "                    first: pa(function() {\n" +
                    "                        return [0]\n" +
                    "                    }),\n" +
                    "                    last: pa(function(a, b) {\n" +
                    "                        return [b - 1]\n" +
                    "                    }),\n" +
                    "                    eq: pa(function(a, b, c) {\n" +
                    "                        return [0 > c ? c + b : c]\n" +
                    "                    }),\n" +
                    "                    even: pa(function(a, b) {\n" +
                    "                        for (var c = 0; b > c; c += 2) a.push(c);\n" +
                    "                        return a\n" +
                    "                    }),\n" +
                    "                    odd: pa(function(a, b) {\n" +
                    "                        for (var c = 1; b > c; c += 2) a.push(c);\n" +
                    "                        return a\n" +
                    "                    }),\n" +
                    "                    lt: pa(function(a, b, c) {\n" +
                    "                        for (var d = 0 > c ? c + b : c; --d >= 0;) a.push(d);\n" +
                    "                        return a\n" +
                    "                    }),\n" +
                    "                    gt: pa(function(a, b, c) {\n" +
                    "                        for (var d = 0 > c ? c + b : c; ++d < b;) a.push(d);\n" +
                    "                        return a\n" +
                    "                    })\n" +
                    "                }\n" +
                    "            }, d.pseudos.nth = d.pseudos.eq;\n" +
                    "            for (b in {\n" +
                    "                    radio: !0,\n" +
                    "                    checkbox: !0,\n" +
                    "                    file: !0,\n" +
                    "                    password: !0,\n" +
                    "                    image: !0\n" +
                    "                }) d.pseudos[b] = ma(b);\n" +
                    "            for (b in {\n" +
                    "                    submit: !0,\n" +
                    "                    reset: !0\n" +
                    "                }) d.pseudos[b] = na(b);\n" +
                    "\n" +
                    "            function ra() {}\n" +
                    "            ra.prototype = d.filters = d.pseudos, d.setFilters = new ra, g = ga.tokenize = function(a, b) {\n" +
                    "                var c, e, f, g, h, i, j, k = z[a + \" \"];\n" +
                    "                if (k) return b ? 0 : k.slice(0);\n" +
                    "                h = a, i = [], j = d.preFilter;\n" +
                    "                while (h) {\n" +
                    "                    c && !(e = Q.exec(h)) || (e && (h = h.slice(e[0].length) || h), i.push(f = [])), c = !1, (e = R.exec(h)) && (c = e.shift(), f.push({\n" +
                    "                        value: c,\n" +
                    "                        type: e[0].replace(P, \" \")\n" +
                    "                    }), h = h.slice(c.length));\n" +
                    "                    for (g in d.filter) !(e = V[g].exec(h)) || j[g] && !(e = j[g](e)) || (c = e.shift(), f.push({\n" +
                    "                        value: c,\n" +
                    "                        type: g,\n" +
                    "                        matches: e\n" +
                    "                    }), h = h.slice(c.length));\n" +
                    "                    if (!c) break\n" +
                    "                }\n" +
                    "                return b ? h.length : h ? ga.error(a) : z(a, i).slice(0)\n" +
                    "            };\n" +
                    "\n" +
                    "            function sa(a) {\n" +
                    "                for (var b = 0, c = a.length, d = \"\"; c > b; b++) d += a[b].value;\n" +
                    "                return d\n" +
                    "            }\n" +
                    "\n" +
                    "            function ta(a, b, c) {\n" +
                    "                var d = b.dir,\n" +
                    "                    e = b.next,\n" +
                    "                    f = e || d,\n" +
                    "                    g = c && \"parentNode\" === f,\n" +
                    "                    h = x++;\n" +
                    "                return b.first ? function(b, c, e) {\n" +
                    "                    while (b = b[d])\n" +
                    "                        if (1 === b.nodeType || g) return a(b, c, e)\n" +
                    "                } : function(b, c, i) {\n" +
                    "                    var j, k, l, m = [w, h];\n" +
                    "                    if (i) {\n" +
                    "                        while (b = b[d])\n" +
                    "                            if ((1 === b.nodeType || g) && a(b, c, i)) return !0\n" +
                    "                    } else\n" +
                    "                        while (b = b[d])\n" +
                    "                            if (1 === b.nodeType || g)\n" +
                    "                                if (l = b[u] || (b[u] = {}), k = l[b.uniqueID] || (l[b.uniqueID] = {}), e && e === b.nodeName.toLowerCase()) b = b[d] || b;\n" +
                    "                                else {\n" +
                    "                                    if ((j = k[f]) && j[0] === w && j[1] === h) return m[2] = j[2];\n" +
                    "                                    if (k[f] = m, m[2] = a(b, c, i)) return !0\n" +
                    "                                }\n" +
                    "                }\n" +
                    "            }\n" +
                    "\n" +
                    "            function ua(a) {\n" +
                    "                return a.length > 1 ? function(b, c, d) {\n" +
                    "                    var e = a.length;\n" +
                    "                    while (e--)\n" +
                    "                        if (!a[e](b, c, d)) return !1;\n" +
                    "                    return !0\n" +
                    "                } : a[0]\n" +
                    "            }\n" +
                    "\n" +
                    "            function va(a, b, c) {\n" +
                    "                for (var d = 0, e = b.length; e > d; d++) ga(a, b[d], c);\n" +
                    "                return c\n" +
                    "            }\n" +
                    "\n" +
                    "            function wa(a, b, c, d, e) {\n" +
                    "                for (var f, g = [], h = 0, i = a.length, j = null != b; i > h; h++)(f = a[h]) && (c && !c(f, d, e) || (g.push(f), j && b.push(h)));\n" +
                    "                return g\n" +
                    "            }\n" +
                    "\n" +
                    "            function xa(a, b, c, d, e, f) {\n" +
                    "                return d && !d[u] && (d = xa(d)), e && !e[u] && (e = xa(e, f)), ia(function(f, g, h, i) {\n" +
                    "                    var j, k, l, m = [],\n" +
                    "                        n = [],\n" +
                    "                        o = g.length,\n" +
                    "                        p = f || va(b || \"*\", h.nodeType ? [h] : h, []),\n" +
                    "                        q = !a || !f && b ? p : wa(p, m, a, h, i),\n" +
                    "                        r = c ? e || (f ? a : o || d) ? [] : g : q;\n" +
                    "                    if (c && c(q, r, h, i), d) {\n" +
                    "                        j = wa(r, n), d(j, [], h, i), k = j.length;\n" +
                    "                        while (k--)(l = j[k]) && (r[n[k]] = !(q[n[k]] = l))\n" +
                    "                    }\n" +
                    "                    if (f) {\n" +
                    "                        if (e || a) {\n" +
                    "                            if (e) {\n" +
                    "                                j = [], k = r.length;\n" +
                    "                                while (k--)(l = r[k]) && j.push(q[k] = l);\n" +
                    "                                e(null, r = [], j, i)\n" +
                    "                            }\n" +
                    "                            k = r.length;\n" +
                    "                            while (k--)(l = r[k]) && (j = e ? I(f, l) : m[k]) > -1 && (f[j] = !(g[j] = l))\n" +
                    "                        }\n" +
                    "                    } else r = wa(r === g ? r.splice(o, r.length) : r), e ? e(null, g, r, i) : G.apply(g, r)\n" +
                    "                })\n" +
                    "            }\n" +
                    "\n" +
                    "            function ya(a) {\n" +
                    "                for (var b, c, e, f = a.length, g = d.relative[a[0].type], h = g || d.relative[\" \"], i = g ? 1 : 0, k = ta(function(a) {\n" +
                    "                        return a === b\n" +
                    "                    }, h, !0), l = ta(function(a) {\n" +
                    "                        return I(b, a) > -1\n" +
                    "                    }, h, !0), m = [function(a, c, d) {\n" +
                    "                        var e = !g && (d || c !== j) || ((b = c).nodeType ? k(a, c, d) : l(a, c, d));\n" +
                    "                        return b = null, e\n" +
                    "                    }]; f > i; i++)\n" +
                    "                    if (c = d.relative[a[i].type]) m = [ta(ua(m), c)];\n" +
                    "                    else {\n" +
                    "                        if (c = d.filter[a[i].type].apply(null, a[i].matches), c[u]) {\n" +
                    "                            for (e = ++i; f > e; e++)\n" +
                    "                                if (d.relative[a[e].type]) break;\n" +
                    "                            return xa(i > 1 && ua(m), i > 1 && sa(a.slice(0, i - 1).concat({\n" +
                    "                                value: \" \" === a[i - 2].type ? \"*\" : \"\"\n" +
                    "                            })).replace(P, \"$1\"), c, e > i && ya(a.slice(i, e)), f > e && ya(a = a.slice(e)), f > e && sa(a))\n" +
                    "                        }\n" +
                    "                        m.push(c)\n" +
                    "                    }\n" +
                    "                return ua(m)\n" +
                    "            }\n" +
                    "\n" +
                    "            function za(a, b) {\n" +
                    "                var c = b.length > 0,\n" +
                    "                    e = a.length > 0,\n" +
                    "                    f = function(f, g, h, i, k) {\n" +
                    "                        var l, o, q, r = 0,\n" +
                    "                            s = \"0\",\n" +
                    "                            t = f && [],\n" +
                    "                            u = [],\n" +
                    "                            v = j,\n" +
                    "                            x = f || e && d.find.TAG(\"*\", k),\n" +
                    "                            y = w += null == v ? 1 : Math.random() || .1,\n" +
                    "                            z = x.length;\n" +
                    "                        for (k && (j = g === n || g || k); s !== z && null != (l = x[s]); s++) {\n" +
                    "                            if (e && l) {\n" +
                    "                                o = 0, g || l.ownerDocument === n || (m(l), h = !p);\n" +
                    "                                while (q = a[o++])\n" +
                    "                                    if (q(l, g || n, h)) {\n" +
                    "                                        i.push(l);\n" +
                    "                                        break\n" +
                    "                                    }\n" +
                    "                                k && (w = y)\n" +
                    "                            }\n" +
                    "                            c && ((l = !q && l) && r--, f && t.push(l))\n" +
                    "                        }\n" +
                    "                        if (r += s, c && s !== r) {\n" +
                    "                            o = 0;\n" +
                    "                            while (q = b[o++]) q(t, u, g, h);\n" +
                    "                            if (f) {\n" +
                    "                                if (r > 0)\n" +
                    "                                    while (s--) t[s] || u[s] || (u[s] = E.call(i));\n" +
                    "                                u = wa(u)\n" +
                    "                            }\n" +
                    "                            G.apply(i, u), k && !f && u.length > 0 && r + b.length > 1 && ga.uniqueSort(i)\n" +
                    "                        }\n" +
                    "                        return k && (w = y, j = v), t\n" +
                    "                    };\n" +
                    "                return c ? ia(f) : f\n" +
                    "            }\n" +
                    "            return h = ga.compile = function(a, b) {\n" +
                    "                var c, d = [],\n" +
                    "                    e = [],\n" +
                    "                    f = A[a + \" \"];\n" +
                    "                if (!f) {\n" +
                    "                    b || (b = g(a)), c = b.length;\n" +
                    "                    while (c--) f = ya(b[c]), f[u] ? d.push(f) : e.push(f);\n" +
                    "                    f = A(a, za(e, d)), f.selector = a\n" +
                    "                }\n" +
                    "                return f\n" +
                    "            }, i = ga.select = function(a, b, e, f) {\n" +
                    "                var i, j, k, l, m, n = \"function\" == typeof a && a,\n" +
                    "                    o = !f && g(a = n.selector || a);\n" +
                    "                if (e = e || [], 1 === o.length) {\n" +
                    "                    if (j = o[0] = o[0].slice(0), j.length > 2 && \"ID\" === (k = j[0]).type && c.getById && 9 === b.nodeType && p && d.relative[j[1].type]) {\n" +
                    "                        if (b = (d.find.ID(k.matches[0].replace(_, aa), b) || [])[0], !b) return e;\n" +
                    "                        n && (b = b.parentNode), a = a.slice(j.shift().value.length)\n" +
                    "                    }\n" +
                    "                    i = V.needsContext.test(a) ? 0 : j.length;\n" +
                    "                    while (i--) {\n" +
                    "                        if (k = j[i], d.relative[l = k.type]) break;\n" +
                    "                        if ((m = d.find[l]) && (f = m(k.matches[0].replace(_, aa), $.test(j[0].type) && qa(b.parentNode) || b))) {\n" +
                    "                            if (j.splice(i, 1), a = f.length && sa(j), !a) return G.apply(e, f), e;\n" +
                    "                            break\n" +
                    "                        }\n" +
                    "                    }\n" +
                    "                }\n" +
                    "                return (n || h(a, o))(f, b, !p, e, !b || $.test(a) && qa(b.parentNode) || b), e\n" +
                    "            }, c.sortStable = u.split(\"\").sort(B).join(\"\") === u, c.detectDuplicates = !!l, m(), c.sortDetached = ja(function(a) {\n" +
                    "                return 1 & a.compareDocumentPosition(n.createElement(\"fieldset\"))\n" +
                    "            }), ja(function(a) {\n" +
                    "                return a.innerHTML = \"<a href='#'></a>\", \"#\" === a.firstChild.getAttribute(\"href\")\n" +
                    "            }) || ka(\"type|href|height|width\", function(a, b, c) {\n" +
                    "                return c ? void 0 : a.getAttribute(b, \"type\" === b.toLowerCase() ? 1 : 2)\n" +
                    "            }), c.attributes && ja(function(a) {\n" +
                    "                return a.innerHTML = \"<input/>\", a.firstChild.setAttribute(\"value\", \"\"), \"\" === a.firstChild.getAttribute(\"value\")\n" +
                    "            }) || ka(\"value\", function(a, b, c) {\n" +
                    "                return c || \"input\" !== a.nodeName.toLowerCase() ? void 0 : a.defaultValue\n" +
                    "            }), ja(function(a) {\n" +
                    "                return null == a.getAttribute(\"disabled\")\n" +
                    "            }) || ka(J, function(a, b, c) {\n" +
                    "                var d;\n" +
                    "                return c ? void 0 : a[b] === !0 ? b.toLowerCase() : (d = a.getAttributeNode(b)) && d.specified ? d.value : null\n" +
                    "            }), ga\n" +
                    "        }(a);\n" +
                    "        r.find = x, r.expr = x.selectors, r.expr[\":\"] = r.expr.pseudos, r.uniqueSort = r.unique = x.uniqueSort, r.text = x.getText, r.isXMLDoc = x.isXML, r.contains = x.contains, r.escapeSelector = x.escape;\n" +
                    "        var y = function(a, b, c) {\n" +
                    "                var d = [],\n" +
                    "                    e = void 0 !== c;\n" +
                    "                while ((a = a[b]) && 9 !== a.nodeType)\n" +
                    "                    if (1 === a.nodeType) {\n" +
                    "                        if (e && r(a).is(c)) break;\n" +
                    "                        d.push(a)\n" +
                    "                    }\n" +
                    "                return d\n" +
                    "            },\n" +
                    "            z = function(a, b) {\n" +
                    "                for (var c = []; a; a = a.nextSibling) 1 === a.nodeType && a !== b && c.push(a);\n" +
                    "                return c\n" +
                    "            },\n" +
                    "            A = r.expr.match.needsContext,\n" +
                    "            B = /^<([a-z][^\\/\\0>:\\x20\\t\\r\\n\\f]*)[\\x20\\t\\r\\n\\f]*\\/?>(?:<\\/\\1>|)$/i,\n" +
                    "            C = /^.[^:#\\[\\.,]*$/;\n" +
                    "\n" +
                    "        function D(a, b, c) {\n" +
                    "            if (r.isFunction(b)) return r.grep(a, function(a, d) {\n" +
                    "                return !!b.call(a, d, a) !== c\n" +
                    "            });\n" +
                    "            if (b.nodeType) return r.grep(a, function(a) {\n" +
                    "                return a === b !== c\n" +
                    "            });\n" +
                    "            if (\"string\" == typeof b) {\n" +
                    "                if (C.test(b)) return r.filter(b, a, c);\n" +
                    "                b = r.filter(b, a)\n" +
                    "            }\n" +
                    "            return r.grep(a, function(a) {\n" +
                    "                return i.call(b, a) > -1 !== c && 1 === a.nodeType\n" +
                    "            })\n" +
                    "        }\n" +
                    "        r.filter = function(a, b, c) {\n" +
                    "            var d = b[0];\n" +
                    "            return c && (a = \":not(\" + a + \")\"), 1 === b.length && 1 === d.nodeType ? r.find.matchesSelector(d, a) ? [d] : [] : r.find.matches(a, r.grep(b, function(a) {\n" +
                    "                return 1 === a.nodeType\n" +
                    "            }))\n" +
                    "        }, r.fn.extend({\n" +
                    "            find: function(a) {\n" +
                    "                var b, c, d = this.length,\n" +
                    "                    e = this;\n" +
                    "                if (\"string\" != typeof a) return this.pushStack(r(a).filter(function() {\n" +
                    "                    for (b = 0; d > b; b++)\n" +
                    "                        if (r.contains(e[b], this)) return !0\n" +
                    "                }));\n" +
                    "                for (c = this.pushStack([]), b = 0; d > b; b++) r.find(a, e[b], c);\n" +
                    "                return d > 1 ? r.uniqueSort(c) : c\n" +
                    "            },\n" +
                    "            filter: function(a) {\n" +
                    "                return this.pushStack(D(this, a || [], !1))\n" +
                    "            },\n" +
                    "            not: function(a) {\n" +
                    "                return this.pushStack(D(this, a || [], !0))\n" +
                    "            },\n" +
                    "            is: function(a) {\n" +
                    "                return !!D(this, \"string\" == typeof a && A.test(a) ? r(a) : a || [], !1).length\n" +
                    "            }\n" +
                    "        });\n" +
                    "        var E, F = /^(?:\\s*(<[\\w\\W]+>)[^>]*|#([\\w-]+))$/,\n" +
                    "            G = r.fn.init = function(a, b, c) {\n" +
                    "                var e, f;\n" +
                    "                if (!a) return this;\n" +
                    "                if (c = c || E, \"string\" == typeof a) {\n" +
                    "                    if (e = \"<\" === a[0] && \">\" === a[a.length - 1] && a.length >= 3 ? [null, a, null] : F.exec(a), !e || !e[1] && b) return !b || b.jquery ? (b || c).find(a) : this.constructor(b).find(a);\n" +
                    "                    if (e[1]) {\n" +
                    "                        if (b = b instanceof r ? b[0] : b, r.merge(this, r.parseHTML(e[1], b && b.nodeType ? b.ownerDocument || b : d, !0)), B.test(e[1]) && r.isPlainObject(b))\n" +
                    "                            for (e in b) r.isFunction(this[e]) ? this[e](b[e]) : this.attr(e, b[e]);\n" +
                    "                        return this\n" +
                    "                    }\n" +
                    "                    return f = d.getElementById(e[2]), f && (this[0] = f, this.length = 1), this\n" +
                    "                }\n" +
                    "                return a.nodeType ? (this[0] = a, this.length = 1, this) : r.isFunction(a) ? void 0 !== c.ready ? c.ready(a) : a(r) : r.makeArray(a, this)\n" +
                    "            };\n" +
                    "        G.prototype = r.fn, E = r(d);\n" +
                    "        var H = /^(?:parents|prev(?:Until|All))/,\n" +
                    "            I = {\n" +
                    "                children: !0,\n" +
                    "                contents: !0,\n" +
                    "                next: !0,\n" +
                    "                prev: !0\n" +
                    "            };\n" +
                    "        r.fn.extend({\n" +
                    "            has: function(a) {\n" +
                    "                var b = r(a, this),\n" +
                    "                    c = b.length;\n" +
                    "                return this.filter(function() {\n" +
                    "                    for (var a = 0; c > a; a++)\n" +
                    "                        if (r.contains(this, b[a])) return !0\n" +
                    "                })\n" +
                    "            },";
            String msg2 ="            closest: function(a, b) {\n" +
                    "                var c, d = 0,\n" +
                    "                    e = this.length,\n" +
                    "                    f = [],\n" +
                    "                    g = \"string\" != typeof a && r(a);\n" +
                    "                if (!A.test(a))\n" +
                    "                    for (; e > d; d++)\n" +
                    "                        for (c = this[d]; c && c !== b; c = c.parentNode)\n" +
                    "                            if (c.nodeType < 11 && (g ? g.index(c) > -1 : 1 === c.nodeType && r.find.matchesSelector(c, a))) {\n" +
                    "                                f.push(c);\n" +
                    "                                break\n" +
                    "                            }\n" +
                    "                return this.pushStack(f.length > 1 ? r.uniqueSort(f) : f)\n" +
                    "            },\n" +
                    "            index: function(a) {\n" +
                    "                return a ? \"string\" == typeof a ? i.call(r(a), this[0]) : i.call(this, a.jquery ? a[0] : a) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1\n" +
                    "            },\n" +
                    "            add: function(a, b) {\n" +
                    "                return this.pushStack(r.uniqueSort(r.merge(this.get(), r(a, b))))\n" +
                    "            },\n" +
                    "            addBack: function(a) {\n" +
                    "                return this.add(null == a ? this.prevObject : this.prevObject.filter(a))\n" +
                    "            }\n" +
                    "        });\n" +
                    "\n" +
                    "        function J(a, b) {\n" +
                    "            while ((a = a[b]) && 1 !== a.nodeType);\n" +
                    "            return a\n" +
                    "        }\n" +
                    "        r.each({\n" +
                    "            parent: function(a) {\n" +
                    "                var b = a.parentNode;\n" +
                    "                return b && 11 !== b.nodeType ? b : null\n" +
                    "            },\n" +
                    "            parents: function(a) {\n" +
                    "                return y(a, \"parentNode\")\n" +
                    "            },\n" +
                    "            parentsUntil: function(a, b, c) {\n" +
                    "                return y(a, \"parentNode\", c)\n" +
                    "            },\n" +
                    "            next: function(a) {\n" +
                    "                return J(a, \"nextSibling\")\n" +
                    "            },\n" +
                    "            prev: function(a) {\n" +
                    "                return J(a, \"previousSibling\")\n" +
                    "            },\n" +
                    "            nextAll: function(a) {\n" +
                    "                return y(a, \"nextSibling\")\n" +
                    "            },\n" +
                    "            prevAll: function(a) {\n" +
                    "                return y(a, \"previousSibling\")\n" +
                    "            },\n" +
                    "            nextUntil: function(a, b, c) {\n" +
                    "                return y(a, \"nextSibling\", c)\n" +
                    "            },\n" +
                    "            prevUntil: function(a, b, c) {\n" +
                    "                return y(a, \"previousSibling\", c)\n" +
                    "            },\n" +
                    "            siblings: function(a) {\n" +
                    "                return z((a.parentNode || {}).firstChild, a)\n" +
                    "            },\n" +
                    "            children: function(a) {\n" +
                    "                return z(a.firstChild)\n" +
                    "            },\n" +
                    "            contents: function(a) {\n" +
                    "                return a.contentDocument || r.merge([], a.childNodes)\n" +
                    "            }\n" +
                    "        }, function(a, b) {\n" +
                    "            r.fn[a] = function(c, d) {\n" +
                    "                var e = r.map(this, b, c);\n" +
                    "                return \"Until\" !== a.slice(-5) && (d = c), d && \"string\" == typeof d && (e = r.filter(d, e)), this.length > 1 && (I[a] || r.uniqueSort(e), H.test(a) && e.reverse()), this.pushStack(e)\n" +
                    "            }\n" +
                    "        });\n" +
                    "        var K = /\\S+/g;\n" +
                    "\n" +
                    "        function L(a) {\n" +
                    "            var b = {};\n" +
                    "            return r.each(a.match(K) || [], function(a, c) {\n" +
                    "                b[c] = !0\n" +
                    "            }), b\n" +
                    "        }\n" +
                    "        r.Callbacks = function(a) {\n" +
                    "            a = \"string\" == typeof a ? L(a) : r.extend({}, a);\n" +
                    "            var b, c, d, e, f = [],\n" +
                    "                g = [],\n" +
                    "                h = -1,\n" +
                    "                i = function() {\n" +
                    "                    for (e = a.once, d = b = !0; g.length; h = -1) {\n" +
                    "                        c = g.shift();\n" +
                    "                        while (++h < f.length) f[h].apply(c[0], c[1]) === !1 && a.stopOnFalse && (h = f.length, c = !1)\n" +
                    "                    }\n" +
                    "                    a.memory || (c = !1), b = !1, e && (f = c ? [] : \"\")\n" +
                    "                },\n" +
                    "                j = {\n" +
                    "                    add: function() {\n" +
                    "                        return f && (c && !b && (h = f.length - 1, g.push(c)), function d(b) {\n" +
                    "                            r.each(b, function(b, c) {\n" +
                    "                                r.isFunction(c) ? a.unique && j.has(c) || f.push(c) : c && c.length && \"string\" !== r.type(c) && d(c)\n" +
                    "                            })\n" +
                    "                        }(arguments), c && !b && i()), this\n" +
                    "                    },\n" +
                    "                    remove: function() {\n" +
                    "                        return r.each(arguments, function(a, b) {\n" +
                    "                            var c;\n" +
                    "                            while ((c = r.inArray(b, f, c)) > -1) f.splice(c, 1), h >= c && h--\n" +
                    "                        }), this\n" +
                    "                    },\n" +
                    "                    has: function(a) {\n" +
                    "                        return a ? r.inArray(a, f) > -1 : f.length > 0\n" +
                    "                    },\n" +
                    "                    empty: function() {\n" +
                    "                        return f && (f = []), this\n" +
                    "                    },\n" +
                    "                    disable: function() {\n" +
                    "                        return e = g = [], f = c = \"\", this\n" +
                    "                    },\n" +
                    "                    disabled: function() {\n" +
                    "                        return !f\n" +
                    "                    },\n" +
                    "                    lock: function() {\n" +
                    "                        return e = g = [], c || b || (f = c = \"\"), this\n" +
                    "                    },\n" +
                    "                    locked: function() {\n" +
                    "                        return !!e\n" +
                    "                    },\n" +
                    "                    fireWith: function(a, c) {\n" +
                    "                        return e || (c = c || [], c = [a, c.slice ? c.slice() : c], g.push(c), b || i()), this\n" +
                    "                    },\n" +
                    "                    fire: function() {\n" +
                    "                        return j.fireWith(this, arguments), this\n" +
                    "                    },\n" +
                    "                    fired: function() {\n" +
                    "                        return !!d\n" +
                    "                    }\n" +
                    "                };\n" +
                    "            return j\n" +
                    "        };\n" +
                    "\n" +
                    "        function M(a) {\n" +
                    "            return a\n" +
                    "        }\n" +
                    "\n" +
                    "        function N(a) {\n" +
                    "            throw a\n" +
                    "        }\n" +
                    "\n" +
                    "        function O(a, b, c) {\n" +
                    "            var d;\n" +
                    "            try {\n" +
                    "                a && r.isFunction(d = a.promise) ? d.call(a).done(b).fail(c) : a && r.isFunction(d = a.then) ? d.call(a, b, c) : b.call(void 0, a)\n" +
                    "            } catch (a) {\n" +
                    "                c.call(void 0, a)\n" +
                    "            }\n" +
                    "        }\n" +
                    "        r.extend({\n" +
                    "            Deferred: function(b) {\n" +
                    "                var c = [\n" +
                    "                        [\"notify\", \"progress\", r.Callbacks(\"memory\"), r.Callbacks(\"memory\"), 2],\n" +
                    "                        [\"resolve\", \"done\", r.Callbacks(\"once memory\"), r.Callbacks(\"once memory\"), 0, \"resolved\"],\n" +
                    "                        [\"reject\", \"fail\", r.Callbacks(\"once memory\"), r.Callbacks(\"once memory\"), 1, \"rejected\"]\n" +
                    "                    ],\n" +
                    "                    d = \"pending\",\n" +
                    "                    e = {\n" +
                    "                        state: function() {\n" +
                    "                            return d\n" +
                    "                        },\n" +
                    "                        always: function() {\n" +
                    "                            return f.done(arguments).fail(arguments), this\n" +
                    "                        },\n" +
                    "                        \"catch\": function(a) {\n" +
                    "                            return e.then(null, a)\n" +
                    "                        },\n" +
                    "                        pipe: function() {\n" +
                    "                            var a = arguments;\n" +
                    "                            return r.Deferred(function(b) {\n" +
                    "                                r.each(c, function(c, d) {\n" +
                    "                                    var e = r.isFunction(a[d[4]]) && a[d[4]];\n" +
                    "                                    f[d[1]](function() {\n" +
                    "                                        var a = e && e.apply(this, arguments);\n" +
                    "                                        a && r.isFunction(a.promise) ? a.promise().progress(b.notify).done(b.resolve).fail(b.reject) : b[d[0] + \"With\"](this, e ? [a] : arguments)\n" +
                    "                                    })\n" +
                    "                                }), a = null\n" +
                    "                            }).promise()\n" +
                    "                        },\n" +
                    "                        then: function(b, d, e) {\n" +
                    "                            var f = 0;\n" +
                    "\n" +
                    "                            function g(b, c, d, e) {\n" +
                    "                                return function() {\n" +
                    "                                    var h = this,\n" +
                    "                                        i = arguments,\n" +
                    "                                        j = function() {\n" +
                    "                                            var a, j;\n" +
                    "                                            if (!(f > b)) {\n" +
                    "                                                if (a = d.apply(h, i), a === c.promise()) throw new TypeError(\"Thenable self-resolution\");\n" +
                    "                                                j = a && (\"object\" == typeof a || \"function\" == typeof a) && a.then, r.isFunction(j) ? e ? j.call(a, g(f, c, M, e), g(f, c, N, e)) : (f++, j.call(a, g(f, c, M, e), g(f, c, N, e), g(f, c, M, c.notifyWith))) : (d !== M && (h = void 0, i = [a]), (e || c.resolveWith)(h, i))\n" +
                    "                                            }\n" +
                    "                                        },\n" +
                    "                                        k = e ? j : function() {\n" +
                    "                                            try {\n" +
                    "                                                j()\n" +
                    "                                            } catch (a) {\n" +
                    "                                                r.Deferred.exceptionHook && r.Deferred.exceptionHook(a, k.stackTrace), b + 1 >= f && (d !== N && (h = void 0, i = [a]), c.rejectWith(h, i))\n" +
                    "                                            }\n" +
                    "                                        };\n" +
                    "                                    b ? k() : (r.Deferred.getStackHook && (k.stackTrace = r.Deferred.getStackHook()), a.setTimeout(k))\n" +
                    "                                }\n" +
                    "                            }\n" +
                    "                            return r.Deferred(function(a) {\n" +
                    "                                c[0][3].add(g(0, a, r.isFunction(e) ? e : M, a.notifyWith)), c[1][3].add(g(0, a, r.isFunction(b) ? b : M)), c[2][3].add(g(0, a, r.isFunction(d) ? d : N))\n" +
                    "                            }).promise()\n" +
                    "                        },\n" +
                    "                        promise: function(a) {\n" +
                    "                            return null != a ? r.extend(a, e) : e\n" +
                    "                        }\n" +
                    "                    },\n" +
                    "                    f = {};\n" +
                    "                return r.each(c, function(a, b) {\n" +
                    "                    var g = b[2],\n" +
                    "                        h = b[5];\n" +
                    "                    e[b[1]] = g.add, h && g.add(function() {\n" +
                    "                        d = h\n" +
                    "                    }, c[3 - a][2].disable, c[0][2].lock), g.add(b[3].fire), f[b[0]] = function() {\n" +
                    "                        return f[b[0] + \"With\"](this === f ? void 0 : this, arguments), this\n" +
                    "                    }, f[b[0] + \"With\"] = g.fireWith\n" +
                    "                }), e.promise(f), b && b.call(f, f), f\n" +
                    "            },\n" +
                    "            when: function(a) {\n" +
                    "                var b = arguments.length,\n" +
                    "                    c = b,\n" +
                    "                    d = Array(c),\n" +
                    "                    e = f.call(arguments),\n" +
                    "                    g = r.Deferred(),\n" +
                    "                    h = function(a) {\n" +
                    "                        return function(c) {\n" +
                    "                            d[a] = this, e[a] = arguments.length > 1 ? f.call(arguments) : c, --b || g.resolveWith(d, e)\n" +
                    "                        }\n" +
                    "                    };\n" +
                    "                if (1 >= b && (O(a, g.done(h(c)).resolve, g.reject), \"pending\" === g.state() || r.isFunction(e[c] && e[c].then))) return g.then();\n" +
                    "                while (c--) O(e[c], h(c), g.reject);\n" +
                    "                return g.promise()\n" +
                    "            }\n" +
                    "        });\n" +
                    "        var P = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;\n" +
                    "        r.Deferred.exceptionHook = function(b, c) {\n" +
                    "            a.console && a.console.warn && b && P.test(b.name) && a.console.warn(\"jQuery.Deferred exception: \" + b.message, b.stack, c)\n" +
                    "        };\n" +
                    "        var Q = r.Deferred();\n" +
                    "        r.fn.ready = function(a) {\n" +
                    "            return Q.then(a), this\n" +
                    "        }, r.extend({\n" +
                    "            isReady: !1,\n" +
                    "            readyWait: 1,\n" +
                    "            holdReady: function(a) {\n" +
                    "                a ? r.readyWait++ : r.ready(!0)\n" +
                    "            },\n" +
                    "            ready: function(a) {\n" +
                    "                (a === !0 ? --r.readyWait : r.isReady) || (r.isReady = !0, a !== !0 && --r.readyWait > 0 || Q.resolveWith(d, [r]))\n" +
                    "            }\n" +
                    "        }), r.ready.then = Q.then;\n" +
                    "\n" +
                    "        function R() {\n" +
                    "            d.removeEventListener(\"DOMContentLoaded\", R), a.removeEventListener(\"load\", R), r.ready()\n" +
                    "        }\n" +
                    "        \"complete\" === d.readyState || \"loading\" !== d.readyState && !d.documentElement.doScroll ? a.setTimeout(r.ready) : (d.addEventListener(\"DOMContentLoaded\", R), a.addEventListener(\"load\", R));\n" +
                    "        var S = function(a, b, c, d, e, f, g) {\n" +
                    "                var h = 0,\n" +
                    "                    i = a.length,\n" +
                    "                    j = null == c;\n" +
                    "                if (\"object\" === r.type(c)) {\n" +
                    "                    e = !0;\n" +
                    "                    for (h in c) S(a, b, h, c[h], !0, f, g)\n" +
                    "                } else if (void 0 !== d && (e = !0, r.isFunction(d) || (g = !0), j && (g ? (b.call(a, d), b = null) : (j = b, b = function(a, b, c) {\n" +
                    "                        return j.call(r(a), c)\n" +
                    "                    })), b))\n" +
                    "                    for (; i > h; h++) b(a[h], c, g ? d : d.call(a[h], h, b(a[h], c)));\n" +
                    "                return e ? a : j ? b.call(a) : i ? b(a[0], c) : f\n" +
                    "            },\n" +
                    "            T = function(a) {\n" +
                    "                return 1 === a.nodeType || 9 === a.nodeType || !+a.nodeType\n" +
                    "            };\n" +
                    "\n" +
                    "        function U() {\n" +
                    "            this.expando = r.expando + U.uid++\n" +
                    "        }\n" +
                    "        U.uid = 1, U.prototype = {\n" +
                    "            cache: function(a) {\n" +
                    "                var b = a[this.expando];\n" +
                    "                return b || (b = {}, T(a) && (a.nodeType ? a[this.expando] = b : Object.defineProperty(a, this.expando, {\n" +
                    "                    value: b,\n" +
                    "                    configurable: !0\n" +
                    "                }))), b\n" +
                    "            },\n" +
                    "            set: function(a, b, c) {\n" +
                    "                var d, e = this.cache(a);\n" +
                    "                if (\"string\" == typeof b) e[r.camelCase(b)] = c;\n" +
                    "                else\n" +
                    "                    for (d in b) e[r.camelCase(d)] = b[d];\n" +
                    "                return e\n" +
                    "            },\n" +
                    "            get: function(a, b) {\n" +
                    "                return void 0 === b ? this.cache(a) : a[this.expando] && a[this.expando][r.camelCase(b)]\n" +
                    "            },\n" +
                    "            access: function(a, b, c) {\n" +
                    "                return void 0 === b || b && \"string\" == typeof b && void 0 === c ? this.get(a, b) : (this.set(a, b, c), void 0 !== c ? c : b)\n" +
                    "            },\n" +
                    "            remove: function(a, b) {\n" +
                    "                var c, d = a[this.expando];\n" +
                    "                if (void 0 !== d) {\n" +
                    "                    if (void 0 !== b) {\n" +
                    "                        r.isArray(b) ? b = b.map(r.camelCase) : (b = r.camelCase(b), b = b in d ? [b] : b.match(K) || []), c = b.length;\n" +
                    "                        while (c--) delete d[b[c]]\n" +
                    "                    }(void 0 === b || r.isEmptyObject(d)) && (a.nodeType ? a[this.expando] = void 0 : delete a[this.expando])\n" +
                    "                }\n" +
                    "            },\n" +
                    "            hasData: function(a) {\n" +
                    "                var b = a[this.expando];\n" +
                    "                return void 0 !== b && !r.isEmptyObject(b)\n" +
                    "            }\n" +
                    "        };\n" +
                    "        var V = new U,\n" +
                    "            W = new U,\n" +
                    "            X = /^(?:\\{[\\w\\W]*\\}|\\[[\\w\\W]*\\])$/,\n" +
                    "            Y = /[A-Z]/g;\n" +
                    "\n" +
                    "        function Z(a, b, c) {\n" +
                    "            var d;\n" +
                    "            if (void 0 === c && 1 === a.nodeType)\n" +
                    "                if (d = \"data-\" + b.replace(Y, \"-$&\").toLowerCase(), c = a.getAttribute(d), \"string\" == typeof c) {\n" +
                    "                    try {\n" +
                    "                        c = \"true\" === c ? !0 : \"false\" === c ? !1 : \"null\" === c ? null : +c + \"\" === c ? +c : X.test(c) ? JSON.parse(c) : c\n" +
                    "                    } catch (e) {}\n" +
                    "                    W.set(a, b, c)\n" +
                    "                } else c = void 0;\n" +
                    "            return c\n" +
                    "        }\n" +
                    "        r.extend({\n" +
                    "            hasData: function(a) {\n" +
                    "                return W.hasData(a) || V.hasData(a)\n" +
                    "            },\n" +
                    "            data: function(a, b, c) {\n" +
                    "                return W.access(a, b, c)\n" +
                    "            },\n" +
                    "            removeData: function(a, b) {\n" +
                    "                W.remove(a, b)\n" +
                    "            },\n" +
                    "            _data: function(a, b, c) {\n" +
                    "                return V.access(a, b, c)\n" +
                    "            },\n" +
                    "            _removeData: function(a, b) {\n" +
                    "                V.remove(a, b)\n" +
                    "            }\n" +
                    "        }), r.fn.extend({\n" +
                    "            data: function(a, b) {\n" +
                    "                var c, d, e, f = this[0],\n" +
                    "                    g = f && f.attributes;\n" +
                    "                if (void 0 === a) {\n" +
                    "                    if (this.length && (e = W.get(f), 1 === f.nodeType && !V.get(f, \"hasDataAttrs\"))) {\n" +
                    "                        c = g.length;\n" +
                    "                        while (c--) g[c] && (d = g[c].name, 0 === d.indexOf(\"data-\") && (d = r.camelCase(d.slice(5)), Z(f, d, e[d])));\n" +
                    "                        V.set(f, \"hasDataAttrs\", !0)\n" +
                    "                    }\n" +
                    "                    return e\n" +
                    "                }\n" +
                    "                return \"object\" == typeof a ? this.each(function() {\n" +
                    "                    W.set(this, a)\n" +
                    "                }) : S(this, function(b) {\n" +
                    "                    var c;\n" +
                    "                    if (f && void 0 === b) {\n" +
                    "                        if (c = W.get(f, a), void 0 !== c) return c;\n" +
                    "                        if (c = Z(f, a), void 0 !== c) return c\n" +
                    "                    } else this.each(function() {\n" +
                    "                        W.set(this, a, b)\n" +
                    "                    })\n" +
                    "                }, null, b, arguments.length > 1, null, !0)\n" +
                    "            },\n" +
                    "            removeData: function(a) {\n" +
                    "                return this.each(function() {\n" +
                    "                    W.remove(this, a)\n" +
                    "                })\n" +
                    "            }\n" +
                    "        }), r.extend({\n" +
                    "            queue: function(a, b, c) {\n" +
                    "                var d;\n" +
                    "                return a ? (b = (b || \"fx\") + \"queue\", d = V.get(a, b), c && (!d || r.isArray(c) ? d = V.access(a, b, r.makeArray(c)) : d.push(c)), d || []) : void 0\n" +
                    "            },\n" +
                    "            dequeue: function(a, b) {\n" +
                    "                b = b || \"fx\";\n" +
                    "                var c = r.queue(a, b),\n" +
                    "                    d = c.length,\n" +
                    "                    e = c.shift(),\n" +
                    "                    f = r._queueHooks(a, b),\n" +
                    "                    g = function() {\n" +
                    "                        r.dequeue(a, b)\n" +
                    "                    };\n" +
                    "                \"inprogress\" === e && (e = c.shift(), d--), e && (\"fx\" === b && c.unshift(\"inprogress\"), delete f.stop, e.call(a, g, f)), !d && f && f.empty.fire()\n" +
                    "            },\n" +
                    "            _queueHooks: function(a, b) {\n" +
                    "                var c = b + \"queueHooks\";\n" +
                    "                return V.get(a, c) || V.access(a, c, {\n" +
                    "                    empty: r.Callbacks(\"once memory\").add(function() {\n" +
                    "                        V.remove(a, [b + \"queue\", c])\n" +
                    "                    })\n" +
                    "                })\n" +
                    "            }\n" +
                    "        }), r.fn.extend({\n" +
                    "            queue: function(a, b) {\n" +
                    "                var c = 2;\n" +
                    "                return \"string\" != typeof a && (b = a, a = \"fx\", c--), arguments.length < c ? r.queue(this[0], a) : void 0 === b ? this : this.each(function() {\n" +
                    "                    var c = r.queue(this, a, b);\n" +
                    "                    r._queueHooks(this, a), \"fx\" === a && \"inprogress\" !== c[0] && r.dequeue(this, a)\n" +
                    "                })\n" +
                    "            },\n" +
                    "            dequeue: function(a) {\n" +
                    "                return this.each(function() {\n" +
                    "                    r.dequeue(this, a)\n" +
                    "                })\n" +
                    "            },\n" +
                    "            clearQueue: function(a) {\n" +
                    "                return this.queue(a || \"fx\", [])\n" +
                    "            },\n" +
                    "            promise: function(a, b) {\n" +
                    "                var c, d = 1,\n" +
                    "                    e = r.Deferred(),\n" +
                    "                    f = this,\n" +
                    "                    g = this.length,\n" +
                    "                    h = function() {\n" +
                    "                        --d || e.resolveWith(f, [f])\n" +
                    "                    };\n" +
                    "                \"string\" != typeof a && (b = a, a = void 0), a = a || \"fx\";\n" +
                    "                while (g--) c = V.get(f[g], a + \"queueHooks\"), c && c.empty && (d++, c.empty.add(h));\n" +
                    "                return h(), e.promise(b)\n" +
                    "            }\n" +
                    "        });\n" +
                    "        var $ = /[+-]?(?:\\d*\\.|)\\d+(?:[eE][+-]?\\d+|)/.source,\n" +
                    "            _ = new RegExp(\"^(?:([+-])=|)(\" + $ + \")([a-z%]*)$\", \"i\"),\n" +
                    "            aa = [\"Top\", \"Right\", \"Bottom\", \"Left\"],\n" +
                    "            ba = function(a, b) {\n" +
                    "                return a = b || a, \"none\" === a.style.display || \"\" === a.style.display && r.contains(a.ownerDocument, a) && \"none\" === r.css(a, \"display\")\n" +
                    "            },\n" +
                    "            ca = function(a, b, c, d) {\n" +
                    "                var e, f, g = {};\n" +
                    "                for (f in b) g[f] = a.style[f], a.style[f] = b[f];\n" +
                    "                e = c.apply(a, d || []);\n" +
                    "                for (f in b) a.style[f] = g[f];\n" +
                    "                return e\n" +
                    "            };\n" +
                    "\n" +
                    "        function da(a, b, c, d) {\n" +
                    "            var e, f = 1,\n" +
                    "                g = 20,\n" +
                    "                h = d ? function() {\n" +
                    "                    return d.cur()\n" +
                    "                } : function() {\n" +
                    "                    return r.css(a, b, \"\")\n" +
                    "                },\n" +
                    "                i = h(),\n" +
                    "                j = c && c[3] || (r.cssNumber[b] ? \"\" : \"px\"),\n" +
                    "                k = (r.cssNumber[b] || \"px\" !== j && +i) && _.exec(r.css(a, b));\n" +
                    "            if (k && k[3] !== j) {\n" +
                    "                j = j || k[3], c = c || [], k = +i || 1;\n" +
                    "                do f = f || \".5\", k /= f, r.style(a, b, k + j); while (f !== (f = h() / i) && 1 !== f && --g)\n" +
                    "            }\n" +
                    "            return c && (k = +k || +i || 0, e = c[1] ? k + (c[1] + 1) * c[2] : +c[2], d && (d.unit = j, d.start = k, d.end = e)), e\n" +
                    "        }\n" +
                    "        var ea = {};\n" +
                    "\n" +
                    "        function fa(a) {\n" +
                    "            var b, c = a.ownerDocument,\n" +
                    "                d = a.nodeName,\n" +
                    "                e = ea[d];\n" +
                    "            return e ? e : (b = c.body.appendChild(c.createElement(d)), e = r.css(b, \"display\"), b.parentNode.removeChild(b), \"none\" === e && (e = \"block\"), ea[d] = e, e)\n" +
                    "        }\n" +
                    "\n" +
                    "        function ga(a, b) {\n" +
                    "            for (var c, d, e = [], f = 0, g = a.length; g > f; f++) d = a[f], d.style && (c = d.style.display, b ? (\"none\" === c && (e[f] = V.get(d, \"display\") || null, e[f] || (d.style.display = \"\")), \"\" === d.style.display && ba(d) && (e[f] = fa(d))) : \"none\" !== c && (e[f] = \"none\", V.set(d, \"display\", c)));\n" +
                    "            for (f = 0; g > f; f++) null != e[f] && (a[f].style.display = e[f]);\n" +
                    "            return a\n" +
                    "        }\n" +
                    "        r.fn.extend({\n" +
                    "            show: function() {\n" +
                    "                return ga(this, !0)\n" +
                    "            },\n" +
                    "            hide: function() {\n" +
                    "                return ga(this)\n" +
                    "            },\n" +
                    "            toggle: function(a) {\n" +
                    "                return \"boolean\" == typeof a ? a ? this.show() : this.hide() : this.each(function() {\n" +
                    "                    ba(this) ? r(this).show() : r(this).hide()\n" +
                    "                })\n" +
                    "            }\n" +
                    "        });\n" +
                    "        var ha = /^(?:checkbox|radio)$/i,\n" +
                    "            ia = /<([a-z][^\\/\\0>\\x20\\t\\r\\n\\f]+)/i,\n" +
                    "            ja = /^$|\\/(?:java|ecma)script/i,\n" +
                    "            ka = {\n" +
                    "                option: [1, \"<select multiple='multiple'>\", \"</select>\"],\n" +
                    "                thead: [1, \"<table>\", \"</table>\"],\n" +
                    "                col: [2, \"<table><colgroup>\", \"</colgroup></table>\"],\n" +
                    "                tr: [2, \"<table><tbody>\", \"</tbody></table>\"],\n" +
                    "                td: [3, \"<table><tbody><tr>\", \"</tr></tbody></table>\"],\n" +
                    "                _default: [0, \"\", \"\"]\n" +
                    "            };\n" +
                    "        ka.optgroup = ka.option, ka.tbody = ka.tfoot = ka.colgroup = ka.caption = ka.thead, ka.th = ka.td;\n" +
                    "\n" +
                    "        function la(a, b) {\n" +
                    "            var c = \"undefined\" != typeof a.getElementsByTagName ? a.getElementsByTagName(b || \"*\") : \"undefined\" != typeof a.querySelectorAll ? a.querySelectorAll(b || \"*\") : [];\n" +
                    "            return void 0 === b || b && r.nodeName(a, b) ? r.merge([a], c) : c\n" +
                    "        }\n" +
                    "\n" +
                    "        function ma(a, b) {\n" +
                    "            for (var c = 0, d = a.length; d > c; c++) V.set(a[c], \"globalEval\", !b || V.get(b[c], \"globalEval\"))\n" +
                    "        }\n" +
                    "        var na = /<|&#?\\w+;/;\n" +
                    "\n" +
                    "        function oa(a, b, c, d, e) {\n" +
                    "            for (var f, g, h, i, j, k, l = b.createDocumentFragment(), m = [], n = 0, o = a.length; o > n; n++)\n" +
                    "                if (f = a[n], f || 0 === f)\n" +
                    "                    if (\"object\" === r.type(f)) r.merge(m, f.nodeType ? [f] : f);\n" +
                    "                    else if (na.test(f)) {\n" +
                    "                g = g || l.appendChild(b.createElement(\"div\")), h = (ia.exec(f) || [\"\", \"\"])[1].toLowerCase(), i = ka[h] || ka._default, g.innerHTML = i[1] + r.htmlPrefilter(f) + i[2], k = i[0];\n" +
                    "                while (k--) g = g.lastChild;\n" +
                    "                r.merge(m, g.childNodes), g = l.firstChild, g.textContent = \"\"\n" +
                    "            } else m.push(b.createTextNode(f));\n" +
                    "            l.textContent = \"\", n = 0;\n" +
                    "            while (f = m[n++])\n" +
                    "                if (d && r.inArray(f, d) > -1) e && e.push(f);\n" +
                    "                else if (j = r.contains(f.ownerDocument, f), g = la(l.appendChild(f), \"script\"), j && ma(g), c) {\n" +
                    "                k = 0;\n" +
                    "                while (f = g[k++]) ja.test(f.type || \"\") && c.push(f)\n" +
                    "            }\n" +
                    "            return l\n" +
                    "        }! function() {\n" +
                    "            var a = d.createDocumentFragment(),\n" +
                    "                b = a.appendChild(d.createElement(\"div\")),\n" +
                    "                c = d.createElement(\"input\");\n" +
                    "            c.setAttribute(\"type\", \"radio\"), c.setAttribute(\"checked\", \"checked\"), c.setAttribute(\"name\", \"t\"), b.appendChild(c), o.checkClone = b.cloneNode(!0).cloneNode(!0).lastChild.checked, b.innerHTML = \"<textarea>x</textarea>\", o.noCloneChecked = !!b.cloneNode(!0).lastChild.defaultValue\n" +
                    "        }();\n" +
                    "        var pa = d.documentElement,\n" +
                    "            qa = /^key/,\n" +
                    "            ra = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,\n" +
                    "            sa = /^([^.]*)(?:\\.(.+)|)/;\n" +
                    "\n" +
                    "        function ta() {\n" +
                    "            return !0\n" +
                    "        }\n" +
                    "\n" +
                    "        function ua() {\n" +
                    "            return !1\n" +
                    "        }\n" +
                    "\n" +
                    "        function va() {\n" +
                    "            try {\n" +
                    "                return d.activeElement\n" +
                    "            } catch (a) {}\n" +
                    "        }\n" +
                    "\n" +
                    "        function wa(a, b, c, d, e, f) {\n" +
                    "            var g, h;\n" +
                    "            if (\"object\" == typeof b) {\n" +
                    "                \"string\" != typeof c && (d = d || c, c = void 0);\n" +
                    "                for (h in b) wa(a, h, c, d, b[h], f);\n" +
                    "                return a\n" +
                    "            }\n" +
                    "            if (null == d && null == e ? (e = c, d = c = void 0) : null == e && (\"string\" == typeof c ? (e = d, d = void 0) : (e = d, d = c, c = void 0)), e === !1) e = ua;\n" +
                    "            else if (!e) return a;\n" +
                    "            return 1 === f && (g = e, e = function(a) {\n" +
                    "                return r().off(a), g.apply(this, arguments)\n" +
                    "            }, e.guid = g.guid || (g.guid = r.guid++)), a.each(function() {\n" +
                    "                r.event.add(this, b, e, d, c)\n" +
                    "            })\n" +
                    "        }\n" +
                    "        r.event = {\n" +
                    "            global: {},\n" +
                    "            add: function(a, b, c, d, e) {\n" +
                    "                var f, g, h, i, j, k, l, m, n, o, p, q = V.get(a);\n" +
                    "                if (q) {\n" +
                    "                    c.handler && (f = c, c = f.handler, e = f.selector), e && r.find.matchesSelector(pa, e), c.guid || (c.guid = r.guid++), (i = q.events) || (i = q.events = {}), (g = q.handle) || (g = q.handle = function(b) {\n" +
                    "                        return \"undefined\" != typeof r && r.event.triggered !== b.type ? r.event.dispatch.apply(a, arguments) : void 0\n" +
                    "                    }), b = (b || \"\").match(K) || [\"\"], j = b.length;\n" +
                    "                    while (j--) h = sa.exec(b[j]) || [], n = p = h[1], o = (h[2] || \"\").split(\".\").sort(), n && (l = r.event.special[n] || {}, n = (e ? l.delegateType : l.bindType) || n, l = r.event.special[n] || {}, k = r.extend({\n" +
                    "                        type: n,\n" +
                    "                        origType: p,\n" +
                    "                        data: d,\n" +
                    "                        handler: c,\n" +
                    "                        guid: c.guid,\n" +
                    "                        selector: e,\n" +
                    "                        needsContext: e && r.expr.match.needsContext.test(e),\n" +
                    "                        namespace: o.join(\".\")\n" +
                    "                    }, f), (m = i[n]) || (m = i[n] = [], m.delegateCount = 0, l.setup && l.setup.call(a, d, o, g) !== !1 || a.addEventListener && a.addEventListener(n, g)), l.add && (l.add.call(a, k), k.handler.guid || (k.handler.guid = c.guid)), e ? m.splice(m.delegateCount++, 0, k) : m.push(k), r.event.global[n] = !0)\n" +
                    "                }\n" +
                    "            },\n" +
                    "            remove: function(a, b, c, d, e) {\n" +
                    "                var f, g, h, i, j, k, l, m, n, o, p, q = V.hasData(a) && V.get(a);\n" +
                    "                if (q && (i = q.events)) {\n" +
                    "                    b = (b || \"\").match(K) || [\"\"], j = b.length;\n" +
                    "                    while (j--)\n" +
                    "                        if (h = sa.exec(b[j]) || [], n = p = h[1], o = (h[2] || \"\").split(\".\").sort(), n) {\n" +
                    "                            l = r.event.special[n] || {}, n = (d ? l.delegateType : l.bindType) || n, m = i[n] || [], h = h[2] && new RegExp(\"(^|\\\\.)\" + o.join(\"\\\\.(?:.*\\\\.|)\") + \"(\\\\.|$)\"), g = f = m.length;\n" +
                    "                            while (f--) k = m[f], !e && p !== k.origType || c && c.guid !== k.guid || h && !h.test(k.namespace) || d && d !== k.selector && (\"**\" !== d || !k.selector) || (m.splice(f, 1), k.selector && m.delegateCount--, l.remove && l.remove.call(a, k));\n" +
                    "                            g && !m.length && (l.teardown && l.teardown.call(a, o, q.handle) !== !1 || r.removeEvent(a, n, q.handle), delete i[n])\n" +
                    "                        } else\n" +
                    "                            for (n in i) r.event.remove(a, n + b[j], c, d, !0);\n" +
                    "                    r.isEmptyObject(i) && V.remove(a, \"handle events\")\n" +
                    "                }\n" +
                    "            },\n" +
                    "            dispatch: function(a) {\n" +
                    "                var b = r.event.fix(a),\n" +
                    "                    c, d, e, f, g, h, i = new Array(arguments.length),\n" +
                    "                    j = (V.get(this, \"events\") || {})[b.type] || [],\n" +
                    "                    k = r.event.special[b.type] || {};\n" +
                    "                for (i[0] = b, c = 1; c < arguments.length; c++) i[c] = arguments[c];\n" +
                    "                if (b.delegateTarget = this, !k.preDispatch || k.preDispatch.call(this, b) !== !1) {\n" +
                    "                    h = r.event.handlers.call(this, b, j), c = 0;\n" +
                    "                    while ((f = h[c++]) && !b.isPropagationStopped()) {\n" +
                    "                        b.currentTarget = f.elem, d = 0;\n" +
                    "                        while ((g = f.handlers[d++]) && !b.isImmediatePropagationStopped()) b.rnamespace && !b.rnamespace.test(g.namespace) || (b.handleObj = g, b.data = g.data, e = ((r.event.special[g.origType] || {}).handle || g.handler).apply(f.elem, i), void 0 !== e && (b.result = e) === !1 && (b.preventDefault(), b.stopPropagation()))\n" +
                    "                    }\n" +
                    "                    return k.postDispatch && k.postDispatch.call(this, b), b.result\n" +
                    "                }\n" +
                    "            },\n" +
                    "            handlers: function(a, b) {\n" +
                    "                var c, d, e, f, g = [],\n" +
                    "                    h = b.delegateCount,\n" +
                    "                    i = a.target;\n" +
                    "                if (h && i.nodeType && (\"click\" !== a.type || isNaN(a.button) || a.button < 1))\n" +
                    "                    for (; i !== this; i = i.parentNode || this)\n" +
                    "                        if (1 === i.nodeType && (i.disabled !== !0 || \"click\" !== a.type)) {\n" +
                    "                            for (d = [], c = 0; h > c; c++) f = b[c], e = f.selector + \" \", void 0 === d[e] && (d[e] = f.needsContext ? r(e, this).index(i) > -1 : r.find(e, this, null, [i]).length), d[e] && d.push(f);\n" +
                    "                            d.length && g.push({\n" +
                    "                                elem: i,\n" +
                    "                                handlers: d\n" +
                    "                            })\n" +
                    "                        }\n" +
                    "                return h < b.length && g.push({\n" +
                    "                    elem: this,\n" +
                    "                    handlers: b.slice(h)\n" +
                    "                }), g\n" +
                    "            },\n" +
                    "            addProp: function(a, b) {\n" +
                    "                Object.defineProperty(r.Event.prototype, a, {\n" +
                    "                    enumerable: !0,\n" +
                    "                    configurable: !0,\n" +
                    "                    get: r.isFunction(b) ? function() {\n" +
                    "                        return this.originalEvent ? b(this.originalEvent) : void 0\n" +
                    "                    } : function() {\n" +
                    "                        return this.originalEvent ? this.originalEvent[a] : void 0\n" +
                    "                    },\n" +
                    "                    set: function(b) {\n" +
                    "                        Object.defineProperty(this, a, {\n" +
                    "                            enumerable: !0,\n" +
                    "                            configurable: !0,\n" +
                    "                            writable: !0,\n" +
                    "                            value: b\n" +
                    "                        })\n" +
                    "                    }\n" +
                    "                })\n" +
                    "            },\n" +
                    "            fix: function(a) {\n" +
                    "                return a[r.expando] ? a : new r.Event(a)\n" +
                    "            },\n" +
                    "            special: {\n" +
                    "                load: {\n" +
                    "                    noBubble: !0\n" +
                    "                },\n" +
                    "                focus: {\n" +
                    "                    trigger: function() {\n" +
                    "                        return this !== va() && this.focus ? (this.focus(), !1) : void 0\n" +
                    "                    },\n" +
                    "                    delegateType: \"focusin\"\n" +
                    "                },\n" +
                    "                blur: {\n" +
                    "                    trigger: function() {\n" +
                    "                        return this === va() && this.blur ? (this.blur(), !1) : void 0\n" +
                    "                    },\n" +
                    "                    delegateType: \"focusout\"\n" +
                    "                },\n" +
                    "                click: {\n" +
                    "                    trigger: function() {\n" +
                    "                        return \"checkbox\" === this.type && this.click && r.nodeName(this, \"input\") ? (this.click(), !1) : void 0\n" +
                    "                    },\n" +
                    "                    _default: function(a) {\n" +
                    "                        return r.nodeName(a.target, \"a\")\n" +
                    "                    }\n" +
                    "                },\n" +
                    "                beforeunload: {\n" +
                    "                    postDispatch: function(a) {\n" +
                    "                        void 0 !== a.result && a.originalEvent && (a.originalEvent.returnValue = a.result)\n" +
                    "                    }\n" +
                    "                }\n" +
                    "            }\n" +
                    "        }, r.removeEvent = function(a, b, c) {\n" +
                    "            a.removeEventListener && a.removeEventListener(b, c)\n" +
                    "        }, r.Event = function(a, b) {\n" +
                    "            return this instanceof r.Event ? (a && a.type ? (this.originalEvent = a, this.type = a.type, this.isDefaultPrevented = a.defaultPrevented || void 0 === a.defaultPrevented && a.returnValue === !1 ? ta : ua, this.target = a.target && 3 === a.target.nodeType ? a.target.parentNode : a.target, this.currentTarget = a.currentTarget, this.relatedTarget = a.relatedTarget) : this.type = a, b && r.extend(this, b), this.timeStamp = a && a.timeStamp || r.now(), void(this[r.expando] = !0)) : new r.Event(a, b)\n" +
                    "        }, r.Event.prototype = {\n" +
                    "            constructor: r.Event,\n" +
                    "            isDefaultPrevented: ua,\n" +
                    "            isPropagationStopped: ua,\n" +
                    "            isImmediatePropagationStopped: ua,\n" +
                    "            isSimulated: !1,\n" +
                    "            preventDefault: function() {\n" +
                    "                var a = this.originalEvent;\n" +
                    "                this.isDefaultPrevented = ta, a && !this.isSimulated && a.preventDefault()\n" +
                    "            },\n" +
                    "            stopPropagation: function() {\n" +
                    "                var a = this.originalEvent;\n" +
                    "                this.isPropagationStopped = ta, a && !this.isSimulated && a.stopPropagation()\n" +
                    "            },\n" +
                    "            stopImmediatePropagation: function() {\n" +
                    "                var a = this.originalEvent;\n" +
                    "                this.isImmediatePropagationStopped = ta, a && !this.isSimulated && a.stopImmediatePropagation(), this.stopPropagation()\n" +
                    "            }\n" +
                    "        }, r.each({\n" +
                    "            altKey: !0,\n" +
                    "            bubbles: !0,\n" +
                    "            cancelable: !0,\n" +
                    "            changedTouches: !0,\n" +
                    "            ctrlKey: !0,\n" +
                    "            detail: !0,\n" +
                    "            eventPhase: !0,\n" +
                    "            metaKey: !0,\n" +
                    "            pageX: !0,\n" +
                    "            pageY: !0,\n" +
                    "            shiftKey: !0,\n" +
                    "            view: !0,\n" +
                    "            \"char\": !0,\n" +
                    "            charCode: !0,\n" +
                    "            key: !0,\n" +
                    "            keyCode: !0,\n" +
                    "            button: !0,\n" +
                    "            buttons: !0,\n" +
                    "            clientX: !0,\n" +
                    "            clientY: !0,\n" +
                    "            offsetX: !0,\n" +
                    "            offsetY: !0,\n" +
                    "            pointerId: !0,\n" +
                    "            pointerType: !0,\n" +
                    "            screenX: !0,\n" +
                    "            screenY: !0,\n" +
                    "            targetTouches: !0,\n" +
                    "            toElement: !0,\n" +
                    "            touches: !0,\n" +
                    "            which: function(a) {\n" +
                    "                var b = a.button;\n" +
                    "                return null == a.which && qa.test(a.type) ? null != a.charCode ? a.charCode : a.keyCode : !a.which && void 0 !== b && ra.test(a.type) ? 1 & b ? 1 : 2 & b ? 3 : 4 & b ? 2 : 0 : a.which\n" +
                    "            }\n" +
                    "        }, r.event.addProp), r.each({\n" +
                    "            mouseenter: \"mouseover\",\n" +
                    "            mouseleave: \"mouseout\",\n" +
                    "            pointerenter: \"pointerover\",\n" +
                    "            pointerleave: \"pointerout\"\n" +
                    "        }, function(a, b) {\n" +
                    "            r.event.special[a] = {\n" +
                    "                delegateType: b,\n" +
                    "                bindType: b,\n" +
                    "                handle: function(a) {\n" +
                    "                    var c, d = this,\n" +
                    "                        e = a.relatedTarget,\n" +
                    "                        f = a.handleObj;\n" +
                    "                    return e && (e === d || r.contains(d, e)) || (a.type = f.origType, c = f.handler.apply(this, arguments), a.type = b), c\n" +
                    "                }\n" +
                    "            }\n" +
                    "        }), r.fn.extend({\n" +
                    "            on: function(a, b, c, d) {\n" +
                    "                return wa(this, a, b, c, d)\n" +
                    "            },\n" +
                    "            one: function(a, b, c, d) {\n" +
                    "                return wa(this, a, b, c, d, 1)\n" +
                    "            },\n" +
                    "            off: function(a, b, c) {\n" +
                    "                var d, e;\n" +
                    "                if (a && a.preventDefault && a.handleObj) return d = a.handleObj, r(a.delegateTarget).off(d.namespace ? d.origType + \".\" + d.namespace : d.origType, d.selector, d.handler), this;\n" +
                    "                if (\"object\" == typeof a) {\n" +
                    "                    for (e in a) this.off(e, b, a[e]);\n" +
                    "                    return this\n" +
                    "                }\n" +
                    "                return b !== !1 && \"function\" != typeof b || (c = b, b = void 0), c === !1 && (c = ua), this.each(function() {\n" +
                    "                    r.event.remove(this, a, c, b)\n" +
                    "                })\n" +
                    "            }\n" +
                    "        });\n" +
                    "        var xa = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\\/\\0>\\x20\\t\\r\\n\\f]*)[^>]*)\\/>/gi,\n" +
                    "            ya = /<script|<style|<link/i,\n" +
                    "            za = /checked\\s*(?:[^=]|=\\s*.checked.)/i,\n" +
                    "            Aa = /^true\\/(.*)/,\n" +
                    "            Ba = /^\\s*<!(?:\\[CDATA\\[|--)|(?:\\]\\]|--)>\\s*$/g;\n" +
                    "\n" +
                    "        function Ca(a, b) {\n" +
                    "            return r.nodeName(a, \"table\") && r.nodeName(11 !== b.nodeType ? b : b.firstChild, \"tr\") ? a.getElementsByTagName(\"tbody\")[0] || a : a\n" +
                    "        }\n" +
                    "\n" +
                    "        function Da(a) {\n" +
                    "            return a.type = (null !== a.getAttribute(\"type\")) + \"/\" + a.type, a\n" +
                    "        }\n" +
                    "\n" +
                    "        function Ea(a) {\n" +
                    "            var b = Aa.exec(a.type);\n" +
                    "            return b ? a.type = b[1] : a.removeAttribute(\"type\"), a\n" +
                    "        }\n" +
                    "\n" +
                    "        function Fa(a, b) {\n" +
                    "            var c, d, e, f, g, h, i, j;\n" +
                    "            if (1 === b.nodeType) {\n" +
                    "                if (V.hasData(a) && (f = V.access(a), g = V.set(b, f), j = f.events)) {\n" +
                    "                    delete g.handle, g.events = {};\n" +
                    "                    for (e in j)\n" +
                    "                        for (c = 0, d = j[e].length; d > c; c++) r.event.add(b, e, j[e][c])\n" +
                    "                }\n" +
                    "                W.hasData(a) && (h = W.access(a), i = r.extend({}, h), W.set(b, i))\n" +
                    "            }\n" +
                    "        }\n" +
                    "\n" +
                    "        function Ga(a, b) {\n" +
                    "            var c = b.nodeName.toLowerCase();\n" +
                    "            \"input\" === c && ha.test(a.type) ? b.checked = a.checked : \"input\" !== c && \"textarea\" !== c || (b.defaultValue = a.defaultValue)\n" +
                    "        }\n" +
                    "\n" +
                    "        function Ha(a, b, c, d) {\n" +
                    "            b = g.apply([], b);\n" +
                    "            var e, f, h, i, j, k, l = 0,\n" +
                    "                m = a.length,\n" +
                    "                n = m - 1,\n" +
                    "                q = b[0],\n" +
                    "                s = r.isFunction(q);\n" +
                    "            if (s || m > 1 && \"string\" == typeof q && !o.checkClone && za.test(q)) return a.each(function(e) {\n" +
                    "                var f = a.eq(e);\n" +
                    "                s && (b[0] = q.call(this, e, f.html())), Ha(f, b, c, d)\n" +
                    "            });\n" +
                    "            if (m && (e = oa(b, a[0].ownerDocument, !1, a, d), f = e.firstChild, 1 === e.childNodes.length && (e = f), f || d)) {\n" +
                    "                for (h = r.map(la(e, \"script\"), Da), i = h.length; m > l; l++) j = e, l !== n && (j = r.clone(j, !0, !0), i && r.merge(h, la(j, \"script\"))), c.call(a[l], j, l);\n" +
                    "                if (i)\n" +
                    "                    for (k = h[h.length - 1].ownerDocument, r.map(h, Ea), l = 0; i > l; l++) j = h[l], ja.test(j.type || \"\") && !V.access(j, \"globalEval\") && r.contains(k, j) && (j.src ? r._evalUrl && r._evalUrl(j.src) : p(j.textContent.replace(Ba, \"\"), k))\n" +
                    "            }\n" +
                    "            return a\n" +
                    "        }\n" +
                    "\n" +
                    "        function Ia(a, b, c) {\n" +
                    "            for (var d, e = b ? r.filter(b, a) : a, f = 0; null != (d = e[f]); f++) c || 1 !== d.nodeType || r.cleanData(la(d)), d.parentNode && (c && r.contains(d.ownerDocument, d) && ma(la(d, \"script\")), d.parentNode.removeChild(d));\n" +
                    "            return a\n" +
                    "        }\n" +
                    "        r.extend({\n" +
                    "            htmlPrefilter: function(a) {\n" +
                    "                return a.replace(xa, \"<$1></$2>\")\n" +
                    "            },\n" +
                    "            clone: function(a, b, c) {\n" +
                    "                var d, e, f, g, h = a.cloneNode(!0),\n" +
                    "                    i = r.contains(a.ownerDocument, a);\n" +
                    "                if (!(o.noCloneChecked || 1 !== a.nodeType && 11 !== a.nodeType || r.isXMLDoc(a)))\n" +
                    "                    for (g = la(h), f = la(a), d = 0, e = f.length; e > d; d++) Ga(f[d], g[d]);\n" +
                    "                if (b)\n" +
                    "                    if (c)\n" +
                    "                        for (f = f || la(a), g = g || la(h), d = 0, e = f.length; e > d; d++) Fa(f[d], g[d]);\n" +
                    "                    else Fa(a, h);\n" +
                    "                return g = la(h, \"script\"), g.length > 0 && ma(g, !i && la(a, \"script\")), h\n" +
                    "            },\n" +
                    "            cleanData: function(a) {\n" +
                    "                for (var b, c, d, e = r.event.special, f = 0; void 0 !== (c = a[f]); f++)\n" +
                    "                    if (T(c)) {\n" +
                    "                        if (b = c[V.expando]) {\n" +
                    "                            if (b.events)\n" +
                    "                                for (d in b.events) e[d] ? r.event.remove(c, d) : r.removeEvent(c, d, b.handle);\n" +
                    "                            c[V.expando] = void 0\n" +
                    "                        }\n" +
                    "                        c[W.expando] && (c[W.expando] = void 0)\n" +
                    "                    }\n" +
                    "            }\n" +
                    "        }), r.fn.extend({\n" +
                    "            detach: function(a) {\n" +
                    "                return Ia(this, a, !0)\n" +
                    "            },\n" +
                    "            remove: function(a) {\n" +
                    "                return Ia(this, a)\n" +
                    "            },\n" +
                    "            text: function(a) {\n" +
                    "                return S(this, function(a) {\n" +
                    "                    return void 0 === a ? r.text(this) : this.empty().each(function() {\n" +
                    "                        1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = a)\n" +
                    "                    })\n" +
                    "                }, null, a, arguments.length)\n" +
                    "            },\n" +
                    "            append: function() {\n" +
                    "                return Ha(this, arguments, function(a) {\n" +
                    "                    if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {\n" +
                    "                        var b = Ca(this, a);\n" +
                    "                        b.appendChild(a)\n" +
                    "                    }\n" +
                    "                })\n" +
                    "            },\n" +
                    "            prepend: function() {\n" +
                    "                return Ha(this, arguments, function(a) {\n" +
                    "                    if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {\n" +
                    "                        var b = Ca(this, a);\n" +
                    "                        b.insertBefore(a, b.firstChild)\n" +
                    "                    }\n" +
                    "                })\n" +
                    "            },\n" +
                    "            before: function() {\n" +
                    "                return Ha(this, arguments, function(a) {\n" +
                    "                    this.parentNode && this.parentNode.insertBefore(a, this)\n" +
                    "                })\n" +
                    "            },\n" +
                    "            after: function() {\n" +
                    "                return Ha(this, arguments, function(a) {\n" +
                    "                    this.parentNode && this.parentNode.insertBefore(a, this.nextSibling)\n" +
                    "                })\n" +
                    "            },\n" +
                    "            empty: function() {\n" +
                    "                for (var a, b = 0; null != (a = this[b]); b++) 1 === a.nodeType && (r.cleanData(la(a, !1)), a.textContent = \"\");\n" +
                    "                return this\n" +
                    "            },\n" +
                    "            clone: function(a, b) {\n" +
                    "                return a = null == a ? !1 : a, b = null == b ? a : b, this.map(function() {\n" +
                    "                    return r.clone(this, a, b)\n" +
                    "                })\n" +
                    "            },\n" +
                    "            html: function(a) {\n" +
                    "                return S(this, function(a) {\n" +
                    "                    var b = this[0] || {},\n" +
                    "                        c = 0,\n" +
                    "                        d = this.length;\n" +
                    "                    if (void 0 === a && 1 === b.nodeType) return b.innerHTML;\n" +
                    "                    if (\"string\" == typeof a && !ya.test(a) && !ka[(ia.exec(a) || [\"\", \"\"])[1].toLowerCase()]) {\n" +
                    "                        a = r.htmlPrefilter(a);\n" +
                    "                        try {\n" +
                    "                            for (; d > c; c++) b = this[c] || {}, 1 === b.nodeType && (r.cleanData(la(b, !1)), b.innerHTML = a);\n" +
                    "                            b = 0\n" +
                    "                        } catch (e) {}\n" +
                    "                    }\n" +
                    "                    b && this.empty().append(a)\n" +
                    "                }, null, a, arguments.length)\n" +
                    "            },\n" +
                    "            replaceWith: function() {\n" +
                    "                var a = [];\n" +
                    "                return Ha(this, arguments, function(b) {\n" +
                    "                    var c = this.parentNode;\n" +
                    "                    r.inArray(this, a) < 0 && (r.cleanData(la(this)), c && c.replaceChild(b, this))\n" +
                    "                }, a)\n" +
                    "            }\n" +
                    "        }), r.each({\n" +
                    "            appendTo: \"append\",\n" +
                    "            prependTo: \"prepend\",\n" +
                    "            insertBefore: \"before\",\n" +
                    "            insertAfter: \"after\",\n" +
                    "            replaceAll: \"replaceWith\"\n" +
                    "        }, function(a, b) {\n" +
                    "            r.fn[a] = function(a) {\n" +
                    "                for (var c, d = [], e = r(a), f = e.length - 1, g = 0; f >= g; g++) c = g === f ? this : this.clone(!0), r(e[g])[b](c), h.apply(d, c.get());\n" +
                    "                return this.pushStack(d)\n" +
                    "            }\n" +
                    "        });\n" +
                    "        var Ja = /^margin/,\n" +
                    "            Ka = new RegExp(\"^(\" + $ + \")(?!px)[a-z%]+$\", \"i\"),\n" +
                    "            La = function(b) {\n" +
                    "                var c = b.ownerDocument.defaultView;\n" +
                    "                return c && c.opener || (c = a), c.getComputedStyle(b)\n" +
                    "            };\n" +
                    "        ! function() {\n" +
                    "            function b() {\n" +
                    "                if (i) {\n" +
                    "                    i.style.cssText = \"box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%\", i.innerHTML = \"\", pa.appendChild(h);\n" +
                    "                    var b = a.getComputedStyle(i);\n" +
                    "                    c = \"1%\" !== b.top, g = \"2px\" === b.marginLeft, e = \"4px\" === b.width, i.style.marginRight = \"50%\", f = \"4px\" === b.marginRight, pa.removeChild(h), i = null\n" +
                    "                }\n" +
                    "            }\n" +
                    "            var c, e, f, g, h = d.createElement(\"div\"),\n" +
                    "                i = d.createElement(\"div\");\n" +
                    "            i.style && (i.style.backgroundClip = \"content-box\", i.cloneNode(!0).style.backgroundClip = \"\", o.clearCloneStyle = \"content-box\" === i.style.backgroundClip, h.style.cssText = \"border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute\", h.appendChild(i), r.extend(o, {\n" +
                    "                pixelPosition: function() {\n" +
                    "                    return b(), c\n" +
                    "                },\n" +
                    "                boxSizingReliable: function() {\n" +
                    "                    return b(), e\n" +
                    "                },\n" +
                    "                pixelMarginRight: function() {\n" +
                    "                    return b(), f\n" +
                    "                },\n" +
                    "                reliableMarginLeft: function() {\n" +
                    "                    return b(), g\n" +
                    "                }\n" +
                    "            }))\n" +
                    "        }();\n" +
                    "\n" +
                    "        function Ma(a, b, c) {\n" +
                    "            var d, e, f, g, h = a.style;\n" +
                    "            return c = c || La(a), c && (g = c.getPropertyValue(b) || c[b], \"\" !== g || r.contains(a.ownerDocument, a) || (g = r.style(a, b)), !o.pixelMarginRight() && Ka.test(g) && Ja.test(b) && (d = h.width, e = h.minWidth, f = h.maxWidth, h.minWidth = h.maxWidth = h.width = g, g = c.width, h.width = d, h.minWidth = e, h.maxWidth = f)), void 0 !== g ? g + \"\" : g\n" +
                    "        }\n" +
                    "\n" +
                    "        function Na(a, b) {\n" +
                    "            return {\n" +
                    "                get: function() {\n" +
                    "                    return a() ? void delete this.get : (this.get = b).apply(this, arguments)\n" +
                    "                }\n" +
                    "            }\n" +
                    "        }\n" +
                    "        var Oa = /^(none|table(?!-c[ea]).+)/,\n" +
                    "            Pa = {\n" +
                    "                position: \"absolute\",\n" +
                    "                visibility: \"hidden\",\n" +
                    "                display: \"block\"";
            String msg3="           },\n" +
                    "            Qa = {\n" +
                    "                letterSpacing: \"0\",\n" +
                    "                fontWeight: \"400\"\n" +
                    "            },\n" +
                    "            Ra = [\"Webkit\", \"Moz\", \"ms\"],\n" +
                    "            Sa = d.createElement(\"div\").style;\n" +
                    "\n" +
                    "        function Ta(a) {\n" +
                    "            if (a in Sa) return a;\n" +
                    "            var b = a[0].toUpperCase() + a.slice(1),\n" +
                    "                c = Ra.length;\n" +
                    "            while (c--)\n" +
                    "                if (a = Ra[c] + b, a in Sa) return a\n" +
                    "        }\n" +
                    "\n" +
                    "        function Ua(a, b, c) {\n" +
                    "            var d = _.exec(b);\n" +
                    "            return d ? Math.max(0, d[2] - (c || 0)) + (d[3] || \"px\") : b\n" +
                    "        }\n" +
                    "\n" +
                    "        function Va(a, b, c, d, e) {\n" +
                    "            for (var f = c === (d ? \"border\" : \"content\") ? 4 : \"width\" === b ? 1 : 0, g = 0; 4 > f; f += 2) \"margin\" === c && (g += r.css(a, c + aa[f], !0, e)), d ? (\"content\" === c && (g -= r.css(a, \"padding\" + aa[f], !0, e)), \"margin\" !== c && (g -= r.css(a, \"border\" + aa[f] + \"Width\", !0, e))) : (g += r.css(a, \"padding\" + aa[f], !0, e), \"padding\" !== c && (g += r.css(a, \"border\" + aa[f] + \"Width\", !0, e)));\n" +
                    "            return g\n" +
                    "        }\n" +
                    "\n" +
                    "        function Wa(a, b, c) {\n" +
                    "            var d, e = !0,\n" +
                    "                f = La(a),\n" +
                    "                g = \"border-box\" === r.css(a, \"boxSizing\", !1, f);\n" +
                    "            if (a.getClientRects().length && (d = a.getBoundingClientRect()[b]), 0 >= d || null == d) {\n" +
                    "                if (d = Ma(a, b, f), (0 > d || null == d) && (d = a.style[b]), Ka.test(d)) return d;\n" +
                    "                e = g && (o.boxSizingReliable() || d === a.style[b]), d = parseFloat(d) || 0\n" +
                    "            }\n" +
                    "            return d + Va(a, b, c || (g ? \"border\" : \"content\"), e, f) + \"px\"\n" +
                    "        }\n" +
                    "        r.extend({\n" +
                    "            cssHooks: {\n" +
                    "                opacity: {\n" +
                    "                    get: function(a, b) {\n" +
                    "                        if (b) {\n" +
                    "                            var c = Ma(a, \"opacity\");\n" +
                    "                            return \"\" === c ? \"1\" : c\n" +
                    "                        }\n" +
                    "                    }\n" +
                    "                }\n" +
                    "            },\n" +
                    "            cssNumber: {\n" +
                    "                animationIterationCount: !0,\n" +
                    "                columnCount: !0,\n" +
                    "                fillOpacity: !0,\n" +
                    "                flexGrow: !0,\n" +
                    "                flexShrink: !0,\n" +
                    "                fontWeight: !0,\n" +
                    "                lineHeight: !0,\n" +
                    "                opacity: !0,\n" +
                    "                order: !0,\n" +
                    "                orphans: !0,\n" +
                    "                widows: !0,\n" +
                    "                zIndex: !0,\n" +
                    "                zoom: !0\n" +
                    "            },\n" +
                    "            cssProps: {\n" +
                    "                \"float\": \"cssFloat\"\n" +
                    "            },\n" +
                    "            style: function(a, b, c, d) {\n" +
                    "                if (a && 3 !== a.nodeType && 8 !== a.nodeType && a.style) {\n" +
                    "                    var e, f, g, h = r.camelCase(b),\n" +
                    "                        i = a.style;\n" +
                    "                    return b = r.cssProps[h] || (r.cssProps[h] = Ta(h) || h), g = r.cssHooks[b] || r.cssHooks[h], void 0 === c ? g && \"get\" in g && void 0 !== (e = g.get(a, !1, d)) ? e : i[b] : (f = typeof c, \"string\" === f && (e = _.exec(c)) && e[1] && (c = da(a, b, e), f = \"number\"), null != c && c === c && (\"number\" === f && (c += e && e[3] || (r.cssNumber[h] ? \"\" : \"px\")), o.clearCloneStyle || \"\" !== c || 0 !== b.indexOf(\"background\") || (i[b] = \"inherit\"), g && \"set\" in g && void 0 === (c = g.set(a, c, d)) || (i[b] = c)), void 0)\n" +
                    "                }\n" +
                    "            },\n" +
                    "            css: function(a, b, c, d) {\n" +
                    "                var e, f, g, h = r.camelCase(b);\n" +
                    "                return b = r.cssProps[h] || (r.cssProps[h] = Ta(h) || h), g = r.cssHooks[b] || r.cssHooks[h], g && \"get\" in g && (e = g.get(a, !0, c)), void 0 === e && (e = Ma(a, b, d)), \"normal\" === e && b in Qa && (e = Qa[b]), \"\" === c || c ? (f = parseFloat(e), c === !0 || isFinite(f) ? f || 0 : e) : e\n" +
                    "            }\n" +
                    "        }), r.each([\"height\", \"width\"], function(a, b) {\n" +
                    "            r.cssHooks[b] = {\n" +
                    "                get: function(a, c, d) {\n" +
                    "                    return c ? !Oa.test(r.css(a, \"display\")) || a.getClientRects().length && a.getBoundingClientRect().width ? Wa(a, b, d) : ca(a, Pa, function() {\n" +
                    "                        return Wa(a, b, d)\n" +
                    "                    }) : void 0\n" +
                    "                },\n" +
                    "                set: function(a, c, d) {\n" +
                    "                    var e, f = d && La(a),\n" +
                    "                        g = d && Va(a, b, d, \"border-box\" === r.css(a, \"boxSizing\", !1, f), f);\n" +
                    "                    return g && (e = _.exec(c)) && \"px\" !== (e[3] || \"px\") && (a.style[b] = c, c = r.css(a, b)), Ua(a, c, g)\n" +
                    "                }\n" +
                    "            }\n" +
                    "        }), r.cssHooks.marginLeft = Na(o.reliableMarginLeft, function(a, b) {\n" +
                    "            return b ? (parseFloat(Ma(a, \"marginLeft\")) || a.getBoundingClientRect().left - ca(a, {\n" +
                    "                marginLeft: 0\n" +
                    "            }, function() {\n" +
                    "                return a.getBoundingClientRect().left\n" +
                    "            })) + \"px\" : void 0\n" +
                    "        }), r.each({\n" +
                    "            margin: \"\",\n" +
                    "            padding: \"\",\n" +
                    "            border: \"Width\"\n" +
                    "        }, function(a, b) {\n" +
                    "            r.cssHooks[a + b] = {\n" +
                    "                expand: function(c) {\n" +
                    "                    for (var d = 0, e = {}, f = \"string\" == typeof c ? c.split(\" \") : [c]; 4 > d; d++) e[a + aa[d] + b] = f[d] || f[d - 2] || f[0];\n" +
                    "                    return e\n" +
                    "                }\n" +
                    "            }, Ja.test(a) || (r.cssHooks[a + b].set = Ua)\n" +
                    "        }), r.fn.extend({\n" +
                    "            css: function(a, b) {\n" +
                    "                return S(this, function(a, b, c) {\n" +
                    "                    var d, e, f = {},\n" +
                    "                        g = 0;\n" +
                    "                    if (r.isArray(b)) {\n" +
                    "                        for (d = La(a), e = b.length; e > g; g++) f[b[g]] = r.css(a, b[g], !1, d);\n" +
                    "                        return f\n" +
                    "                    }\n" +
                    "                    return void 0 !== c ? r.style(a, b, c) : r.css(a, b)\n" +
                    "                }, a, b, arguments.length > 1)\n" +
                    "            }\n" +
                    "        });\n" +
                    "\n" +
                    "        function Xa(a, b, c, d, e) {\n" +
                    "            return new Xa.prototype.init(a, b, c, d, e)\n" +
                    "        }\n" +
                    "        r.Tween = Xa, Xa.prototype = {\n" +
                    "            constructor: Xa,\n" +
                    "            init: function(a, b, c, d, e, f) {\n" +
                    "                this.elem = a, this.prop = c, this.easing = e || r.easing._default, this.options = b, this.start = this.now = this.cur(), this.end = d, this.unit = f || (r.cssNumber[c] ? \"\" : \"px\")\n" +
                    "            },\n" +
                    "            cur: function() {\n" +
                    "                var a = Xa.propHooks[this.prop];\n" +
                    "                return a && a.get ? a.get(this) : Xa.propHooks._default.get(this)\n" +
                    "            },\n" +
                    "            run: function(a) {\n" +
                    "                var b, c = Xa.propHooks[this.prop];\n" +
                    "                return this.options.duration ? this.pos = b = r.easing[this.easing](a, this.options.duration * a, 0, 1, this.options.duration) : this.pos = b = a, this.now = (this.end - this.start) * b + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), c && c.set ? c.set(this) : Xa.propHooks._default.set(this), this\n" +
                    "            }\n" +
                    "        }, Xa.prototype.init.prototype = Xa.prototype, Xa.propHooks = {\n" +
                    "            _default: {\n" +
                    "                get: function(a) {\n" +
                    "                    var b;\n" +
                    "                    return 1 !== a.elem.nodeType || null != a.elem[a.prop] && null == a.elem.style[a.prop] ? a.elem[a.prop] : (b = r.css(a.elem, a.prop, \"\"), b && \"auto\" !== b ? b : 0)\n" +
                    "                },\n" +
                    "                set: function(a) {\n" +
                    "                    r.fx.step[a.prop] ? r.fx.step[a.prop](a) : 1 !== a.elem.nodeType || null == a.elem.style[r.cssProps[a.prop]] && !r.cssHooks[a.prop] ? a.elem[a.prop] = a.now : r.style(a.elem, a.prop, a.now + a.unit)\n" +
                    "                }\n" +
                    "            }\n" +
                    "        }, Xa.propHooks.scrollTop = Xa.propHooks.scrollLeft = {\n" +
                    "            set: function(a) {\n" +
                    "                a.elem.nodeType && a.elem.parentNode && (a.elem[a.prop] = a.now)\n" +
                    "            }\n" +
                    "        }, r.easing = {\n" +
                    "            linear: function(a) {\n" +
                    "                return a\n" +
                    "            },\n" +
                    "            swing: function(a) {\n" +
                    "                return .5 - Math.cos(a * Math.PI) / 2\n" +
                    "            },\n" +
                    "            _default: \"swing\"\n" +
                    "        }, r.fx = Xa.prototype.init, r.fx.step = {};\n" +
                    "        var Ya, Za, $a = /^(?:toggle|show|hide)$/,\n" +
                    "            _a = /queueHooks$/;\n" +
                    "\n" +
                    "        function ab() {\n" +
                    "            Za && (a.requestAnimationFrame(ab), r.fx.tick())\n" +
                    "        }\n" +
                    "\n" +
                    "        function bb() {\n" +
                    "            return a.setTimeout(function() {\n" +
                    "                Ya = void 0\n" +
                    "            }), Ya = r.now()\n" +
                    "        }\n" +
                    "\n" +
                    "        function cb(a, b) {\n" +
                    "            var c, d = 0,\n" +
                    "                e = {\n" +
                    "                    height: a\n" +
                    "                };\n" +
                    "            for (b = b ? 1 : 0; 4 > d; d += 2 - b) c = aa[d], e[\"margin\" + c] = e[\"padding\" + c] = a;\n" +
                    "            return b && (e.opacity = e.width = a), e\n" +
                    "        }\n" +
                    "\n" +
                    "        function db(a, b, c) {\n" +
                    "            for (var d, e = (gb.tweeners[b] || []).concat(gb.tweeners[\"*\"]), f = 0, g = e.length; g > f; f++)\n" +
                    "                if (d = e[f].call(c, b, a)) return d\n" +
                    "        }\n" +
                    "\n" +
                    "        function eb(a, b, c) {\n" +
                    "            var d, e, f, g, h, i, j, k, l = \"width\" in b || \"height\" in b,\n" +
                    "                m = this,\n" +
                    "                n = {},\n" +
                    "                o = a.style,\n" +
                    "                p = a.nodeType && ba(a),\n" +
                    "                q = V.get(a, \"fxshow\");\n" +
                    "            c.queue || (g = r._queueHooks(a, \"fx\"), null == g.unqueued && (g.unqueued = 0, h = g.empty.fire, g.empty.fire = function() {\n" +
                    "                g.unqueued || h()\n" +
                    "            }), g.unqueued++, m.always(function() {\n" +
                    "                m.always(function() {\n" +
                    "                    g.unqueued--, r.queue(a, \"fx\").length || g.empty.fire()\n" +
                    "                })\n" +
                    "            }));\n" +
                    "            for (d in b)\n" +
                    "                if (e = b[d], $a.test(e)) {\n" +
                    "                    if (delete b[d], f = f || \"toggle\" === e, e === (p ? \"hide\" : \"show\")) {\n" +
                    "                        if (\"show\" !== e || !q || void 0 === q[d]) continue;\n" +
                    "                        p = !0\n" +
                    "                    }\n" +
                    "                    n[d] = q && q[d] || r.style(a, d)\n" +
                    "                }\n" +
                    "            if (i = !r.isEmptyObject(b), i || !r.isEmptyObject(n)) {\n" +
                    "                l && 1 === a.nodeType && (c.overflow = [o.overflow, o.overflowX, o.overflowY], j = q && q.display, null == j && (j = V.get(a, \"display\")), k = r.css(a, \"display\"), \"none\" === k && (j ? k = j : (ga([a], !0), j = a.style.display || j, k = r.css(a, \"display\"), ga([a]))), (\"inline\" === k || \"inline-block\" === k && null != j) && \"none\" === r.css(a, \"float\") && (i || (m.done(function() {\n" +
                    "                    o.display = j\n" +
                    "                }), null == j && (k = o.display, j = \"none\" === k ? \"\" : k)), o.display = \"inline-block\")), c.overflow && (o.overflow = \"hidden\", m.always(function() {\n" +
                    "                    o.overflow = c.overflow[0], o.overflowX = c.overflow[1], o.overflowY = c.overflow[2]\n" +
                    "                })), i = !1;\n" +
                    "                for (d in n) i || (q ? \"hidden\" in q && (p = q.hidden) : q = V.access(a, \"fxshow\", {\n" +
                    "                    display: j\n" +
                    "                }), f && (q.hidden = !p), p && ga([a], !0), m.done(function() {\n" +
                    "                    p || ga([a]), V.remove(a, \"fxshow\");\n" +
                    "                    for (d in n) r.style(a, d, n[d])\n" +
                    "                })), i = db(p ? q[d] : 0, d, m), d in q || (q[d] = i.start, p && (i.end = i.start, i.start = 0))\n" +
                    "            }\n" +
                    "        }\n" +
                    "\n" +
                    "        function fb(a, b) {\n" +
                    "            var c, d, e, f, g;\n" +
                    "            for (c in a)\n" +
                    "                if (d = r.camelCase(c), e = b[d], f = a[c], r.isArray(f) && (e = f[1], f = a[c] = f[0]), c !== d && (a[d] = f, delete a[c]), g = r.cssHooks[d], g && \"expand\" in g) {\n" +
                    "                    f = g.expand(f), delete a[d];\n" +
                    "                    for (c in f) c in a || (a[c] = f[c], b[c] = e)\n" +
                    "                } else b[d] = e\n" +
                    "        }\n" +
                    "\n" +
                    "        function gb(a, b, c) {\n" +
                    "            var d, e, f = 0,\n" +
                    "                g = gb.prefilters.length,\n" +
                    "                h = r.Deferred().always(function() {\n" +
                    "                    delete i.elem\n" +
                    "                }),\n" +
                    "                i = function() {\n" +
                    "                    if (e) return !1;\n" +
                    "                    for (var b = Ya || bb(), c = Math.max(0, j.startTime + j.duration - b), d = c / j.duration || 0, f = 1 - d, g = 0, i = j.tweens.length; i > g; g++) j.tweens[g].run(f);\n" +
                    "                    return h.notifyWith(a, [j, f, c]), 1 > f && i ? c : (h.resolveWith(a, [j]), !1)\n" +
                    "                },\n" +
                    "                j = h.promise({\n" +
                    "                    elem: a,\n" +
                    "                    props: r.extend({}, b),\n" +
                    "                    opts: r.extend(!0, {\n" +
                    "                        specialEasing: {},\n" +
                    "                        easing: r.easing._default\n" +
                    "                    }, c),\n" +
                    "                    originalProperties: b,\n" +
                    "                    originalOptions: c,\n" +
                    "                    startTime: Ya || bb(),\n" +
                    "                    duration: c.duration,\n" +
                    "                    tweens: [],\n" +
                    "                    createTween: function(b, c) {\n" +
                    "                        var d = r.Tween(a, j.opts, b, c, j.opts.specialEasing[b] || j.opts.easing);\n" +
                    "                        return j.tweens.push(d), d\n" +
                    "                    },\n" +
                    "                    stop: function(b) {\n" +
                    "                        var c = 0,\n" +
                    "                            d = b ? j.tweens.length : 0;\n" +
                    "                        if (e) return this;\n" +
                    "                        for (e = !0; d > c; c++) j.tweens[c].run(1);\n" +
                    "                        return b ? (h.notifyWith(a, [j, 1, 0]), h.resolveWith(a, [j, b])) : h.rejectWith(a, [j, b]), this\n" +
                    "                    }\n" +
                    "                }),\n" +
                    "                k = j.props;\n" +
                    "            for (fb(k, j.opts.specialEasing); g > f; f++)\n" +
                    "                if (d = gb.prefilters[f].call(j, a, k, j.opts)) return r.isFunction(d.stop) && (r._queueHooks(j.elem, j.opts.queue).stop = r.proxy(d.stop, d)), d;\n" +
                    "            return r.map(k, db, j), r.isFunction(j.opts.start) && j.opts.start.call(a, j), r.fx.timer(r.extend(i, {\n" +
                    "                elem: a,\n" +
                    "                anim: j,\n" +
                    "                queue: j.opts.queue\n" +
                    "            })), j.progress(j.opts.progress).done(j.opts.done, j.opts.complete).fail(j.opts.fail).always(j.opts.always)\n" +
                    "        }\n" +
                    "        r.Animation = r.extend(gb, {\n" +
                    "                tweeners: {\n" +
                    "                    \"*\": [function(a, b) {\n" +
                    "                        var c = this.createTween(a, b);\n" +
                    "                        return da(c.elem, a, _.exec(b), c), c\n" +
                    "                    }]\n" +
                    "                },\n" +
                    "                tweener: function(a, b) {\n" +
                    "                    r.isFunction(a) ? (b = a, a = [\"*\"]) : a = a.match(K);\n" +
                    "                    for (var c, d = 0, e = a.length; e > d; d++) c = a[d], gb.tweeners[c] = gb.tweeners[c] || [], gb.tweeners[c].unshift(b)\n" +
                    "                },\n" +
                    "                prefilters: [eb],\n" +
                    "                prefilter: function(a, b) {\n" +
                    "                    b ? gb.prefilters.unshift(a) : gb.prefilters.push(a)\n" +
                    "                }\n" +
                    "            }), r.speed = function(a, b, c) {\n" +
                    "                var e = a && \"object\" == typeof a ? r.extend({}, a) : {\n" +
                    "                    complete: c || !c && b || r.isFunction(a) && a,\n" +
                    "                    duration: a,\n" +
                    "                    easing: c && b || b && !r.isFunction(b) && b\n" +
                    "                };\n" +
                    "                return r.fx.off || d.hidden ? e.duration = 0 : e.duration = \"number\" == typeof e.duration ? e.duration : e.duration in r.fx.speeds ? r.fx.speeds[e.duration] : r.fx.speeds._default, null != e.queue && e.queue !== !0 || (e.queue = \"fx\"), e.old = e.complete, e.complete = function() {\n" +
                    "                    r.isFunction(e.old) && e.old.call(this), e.queue && r.dequeue(this, e.queue)\n" +
                    "                }, e\n" +
                    "            }, r.fn.extend({\n" +
                    "                fadeTo: function(a, b, c, d) {\n" +
                    "                    return this.filter(ba).css(\"opacity\", 0).show().end().animate({\n" +
                    "                        opacity: b\n" +
                    "                    }, a, c, d)\n" +
                    "                },\n" +
                    "                animate: function(a, b, c, d) {\n" +
                    "                    var e = r.isEmptyObject(a),\n" +
                    "                        f = r.speed(b, c, d),\n" +
                    "                        g = function() {\n" +
                    "                            var b = gb(this, r.extend({}, a), f);\n" +
                    "                            (e || V.get(this, \"finish\")) && b.stop(!0)\n" +
                    "                        };\n" +
                    "                    return g.finish = g, e || f.queue === !1 ? this.each(g) : this.queue(f.queue, g)\n" +
                    "                },\n" +
                    "                stop: function(a, b, c) {\n" +
                    "                    var d = function(a) {\n" +
                    "                        var b = a.stop;\n" +
                    "                        delete a.stop, b(c)\n" +
                    "                    };\n" +
                    "                    return \"string\" != typeof a && (c = b, b = a, a = void 0), b && a !== !1 && this.queue(a || \"fx\", []), this.each(function() {\n" +
                    "                        var b = !0,\n" +
                    "                            e = null != a && a + \"queueHooks\",\n" +
                    "                            f = r.timers,\n" +
                    "                            g = V.get(this);\n" +
                    "                        if (e) g[e] && g[e].stop && d(g[e]);\n" +
                    "                        else\n" +
                    "                            for (e in g) g[e] && g[e].stop && _a.test(e) && d(g[e]);\n" +
                    "                        for (e = f.length; e--;) f[e].elem !== this || null != a && f[e].queue !== a || (f[e].anim.stop(c), b = !1, f.splice(e, 1));\n" +
                    "                        !b && c || r.dequeue(this, a)\n" +
                    "                    })\n" +
                    "                },\n" +
                    "                finish: function(a) {\n" +
                    "                    return a !== !1 && (a = a || \"fx\"), this.each(function() {\n" +
                    "                        var b, c = V.get(this),\n" +
                    "                            d = c[a + \"queue\"],\n" +
                    "                            e = c[a + \"queueHooks\"],\n" +
                    "                            f = r.timers,\n" +
                    "                            g = d ? d.length : 0;\n" +
                    "                        for (c.finish = !0, r.queue(this, a, []), e && e.stop && e.stop.call(this, !0), b = f.length; b--;) f[b].elem === this && f[b].queue === a && (f[b].anim.stop(!0), f.splice(b, 1));\n" +
                    "                        for (b = 0; g > b; b++) d[b] && d[b].finish && d[b].finish.call(this);\n" +
                    "                        delete c.finish\n" +
                    "                    })\n" +
                    "                }\n" +
                    "            }), r.each([\"toggle\", \"show\", \"hide\"], function(a, b) {\n" +
                    "                var c = r.fn[b];\n" +
                    "                r.fn[b] = function(a, d, e) {\n" +
                    "                    return null == a || \"boolean\" == typeof a ? c.apply(this, arguments) : this.animate(cb(b, !0), a, d, e)\n" +
                    "                }\n" +
                    "            }), r.each({\n" +
                    "                slideDown: cb(\"show\"),\n" +
                    "                slideUp: cb(\"hide\"),\n" +
                    "                slideToggle: cb(\"toggle\"),\n" +
                    "                fadeIn: {\n" +
                    "                    opacity: \"show\"\n" +
                    "                },\n" +
                    "                fadeOut: {\n" +
                    "                    opacity: \"hide\"\n" +
                    "                },\n" +
                    "                fadeToggle: {\n" +
                    "                    opacity: \"toggle\"\n" +
                    "                }\n" +
                    "            }, function(a, b) {\n" +
                    "                r.fn[a] = function(a, c, d) {\n" +
                    "                    return this.animate(b, a, c, d)\n" +
                    "                }\n" +
                    "            }), r.timers = [], r.fx.tick = function() {\n" +
                    "                var a, b = 0,\n" +
                    "                    c = r.timers;\n" +
                    "                for (Ya = r.now(); b < c.length; b++) a = c[b], a() || c[b] !== a || c.splice(b--, 1);\n" +
                    "                c.length || r.fx.stop(), Ya = void 0\n" +
                    "            }, r.fx.timer = function(a) {\n" +
                    "                r.timers.push(a), a() ? r.fx.start() : r.timers.pop()\n" +
                    "            }, r.fx.interval = 13, r.fx.start = function() {\n" +
                    "                Za || (Za = a.requestAnimationFrame ? a.requestAnimationFrame(ab) : a.setInterval(r.fx.tick, r.fx.interval))\n" +
                    "            }, r.fx.stop = function() {\n" +
                    "                a.cancelAnimationFrame ? a.cancelAnimationFrame(Za) : a.clearInterval(Za), Za = null\n" +
                    "            }, r.fx.speeds = {\n" +
                    "                slow: 600,\n" +
                    "                fast: 200,\n" +
                    "                _default: 400\n" +
                    "            }, r.fn.delay = function(b, c) {\n" +
                    "                return b = r.fx ? r.fx.speeds[b] || b : b, c = c || \"fx\", this.queue(c, function(c, d) {\n" +
                    "                    var e = a.setTimeout(c, b);\n" +
                    "                    d.stop = function() {\n" +
                    "                        a.clearTimeout(e)\n" +
                    "                    }\n" +
                    "                })\n" +
                    "            },\n" +
                    "            function() {\n" +
                    "                var a = d.createElement(\"input\"),\n" +
                    "                    b = d.createElement(\"select\"),\n" +
                    "                    c = b.appendChild(d.createElement(\"option\"));\n" +
                    "                a.type = \"checkbox\", o.checkOn = \"\" !== a.value, o.optSelected = c.selected, a = d.createElement(\"input\"), a.value = \"t\", a.type = \"radio\", o.radioValue = \"t\" === a.value\n" +
                    "            }();\n" +
                    "        var hb, ib = r.expr.attrHandle;\n" +
                    "        r.fn.extend({\n" +
                    "            attr: function(a, b) {\n" +
                    "                return S(this, r.attr, a, b, arguments.length > 1)\n" +
                    "            },\n" +
                    "            removeAttr: function(a) {\n" +
                    "                return this.each(function() {\n" +
                    "                    r.removeAttr(this, a)\n" +
                    "                })\n" +
                    "            }\n" +
                    "        }), r.extend({\n" +
                    "            attr: function(a, b, c) {\n" +
                    "                var d, e, f = a.nodeType;\n" +
                    "                if (3 !== f && 8 !== f && 2 !== f) return \"undefined\" == typeof a.getAttribute ? r.prop(a, b, c) : (1 === f && r.isXMLDoc(a) || (e = r.attrHooks[b.toLowerCase()] || (r.expr.match.bool.test(b) ? hb : void 0)), void 0 !== c ? null === c ? void r.removeAttr(a, b) : e && \"set\" in e && void 0 !== (d = e.set(a, c, b)) ? d : (a.setAttribute(b, c + \"\"), c) : e && \"get\" in e && null !== (d = e.get(a, b)) ? d : (d = r.find.attr(a, b), null == d ? void 0 : d))\n" +
                    "            },\n" +
                    "            attrHooks: {\n" +
                    "                type: {\n" +
                    "                    set: function(a, b) {\n" +
                    "                        if (!o.radioValue && \"radio\" === b && r.nodeName(a, \"input\")) {\n" +
                    "                            var c = a.value;\n" +
                    "                            return a.setAttribute(\"type\", b), c && (a.value = c), b\n" +
                    "                        }\n" +
                    "                    }\n" +
                    "                }\n" +
                    "            },\n" +
                    "            removeAttr: function(a, b) {\n" +
                    "                var c, d = 0,\n" +
                    "                    e = b && b.match(K);\n" +
                    "                if (e && 1 === a.nodeType)\n" +
                    "                    while (c = e[d++]) a.removeAttribute(c);\n" +
                    "            }\n" +
                    "        }), hb = {\n" +
                    "            set: function(a, b, c) {\n" +
                    "                return b === !1 ? r.removeAttr(a, c) : a.setAttribute(c, c), c\n" +
                    "            }\n" +
                    "        }, r.each(r.expr.match.bool.source.match(/\\w+/g), function(a, b) {\n" +
                    "            var c = ib[b] || r.find.attr;\n" +
                    "            ib[b] = function(a, b, d) {\n" +
                    "                var e, f, g = b.toLowerCase();\n" +
                    "                return d || (f = ib[g], ib[g] = e, e = null != c(a, b, d) ? g : null, ib[g] = f), e\n" +
                    "            }\n" +
                    "        });\n" +
                    "        var jb = /^(?:input|select|textarea|button)$/i,\n" +
                    "            kb = /^(?:a|area)$/i;\n" +
                    "        r.fn.extend({\n" +
                    "            prop: function(a, b) {\n" +
                    "                return S(this, r.prop, a, b, arguments.length > 1)\n" +
                    "            },\n" +
                    "            removeProp: function(a) {\n" +
                    "                return this.each(function() {\n" +
                    "                    delete this[r.propFix[a] || a]\n" +
                    "                })\n" +
                    "            }\n" +
                    "        }), r.extend({\n" +
                    "            prop: function(a, b, c) {\n" +
                    "                var d, e, f = a.nodeType;\n" +
                    "                if (3 !== f && 8 !== f && 2 !== f) return 1 === f && r.isXMLDoc(a) || (b = r.propFix[b] || b, e = r.propHooks[b]), void 0 !== c ? e && \"set\" in e && void 0 !== (d = e.set(a, c, b)) ? d : a[b] = c : e && \"get\" in e && null !== (d = e.get(a, b)) ? d : a[b]\n" +
                    "            },\n" +
                    "            propHooks: {\n" +
                    "                tabIndex: {\n" +
                    "                    get: function(a) {\n" +
                    "                        var b = r.find.attr(a, \"tabindex\");\n" +
                    "                        return b ? parseInt(b, 10) : jb.test(a.nodeName) || kb.test(a.nodeName) && a.href ? 0 : -1\n" +
                    "                    }\n" +
                    "                }\n" +
                    "            },\n" +
                    "            propFix: {\n" +
                    "                \"for\": \"htmlFor\",\n" +
                    "                \"class\": \"className\"\n" +
                    "            }\n" +
                    "        }), o.optSelected || (r.propHooks.selected = {\n" +
                    "            get: function(a) {\n" +
                    "                var b = a.parentNode;\n" +
                    "                return b && b.parentNode && b.parentNode.selectedIndex, null\n" +
                    "            },\n" +
                    "            set: function(a) {\n" +
                    "                var b = a.parentNode;\n" +
                    "                b && (b.selectedIndex, b.parentNode && b.parentNode.selectedIndex)\n" +
                    "            }\n" +
                    "        }), r.each([\"tabIndex\", \"readOnly\", \"maxLength\", \"cellSpacing\", \"cellPadding\", \"rowSpan\", \"colSpan\", \"useMap\", \"frameBorder\", \"contentEditable\"], function() {\n" +
                    "            r.propFix[this.toLowerCase()] = this\n" +
                    "        });\n" +
                    "        var lb = /[\\t\\r\\n\\f]/g;\n" +
                    "\n" +
                    "        function mb(a) {\n" +
                    "            return a.getAttribute && a.getAttribute(\"class\") || \"\"\n" +
                    "        }\n" +
                    "        r.fn.extend({\n" +
                    "            addClass: function(a) {\n" +
                    "                var b, c, d, e, f, g, h, i = 0;\n" +
                    "                if (r.isFunction(a)) return this.each(function(b) {\n" +
                    "                    r(this).addClass(a.call(this, b, mb(this)))\n" +
                    "                });\n" +
                    "                if (\"string\" == typeof a && a) {\n" +
                    "                    b = a.match(K) || [];\n" +
                    "                    while (c = this[i++])\n" +
                    "                        if (e = mb(c), d = 1 === c.nodeType && (\" \" + e + \" \").replace(lb, \" \")) {\n" +
                    "                            g = 0;\n" +
                    "                            while (f = b[g++]) d.indexOf(\" \" + f + \" \") < 0 && (d += f + \" \");\n" +
                    "                            h = r.trim(d), e !== h && c.setAttribute(\"class\", h)\n" +
                    "                        }\n" +
                    "                }\n" +
                    "                return this\n" +
                    "            },\n" +
                    "            removeClass: function(a) {\n" +
                    "                var b, c, d, e, f, g, h, i = 0;\n" +
                    "                if (r.isFunction(a)) return this.each(function(b) {\n" +
                    "                    r(this).removeClass(a.call(this, b, mb(this)))\n" +
                    "                });\n" +
                    "                if (!arguments.length) return this.attr(\"class\", \"\");\n" +
                    "                if (\"string\" == typeof a && a) {\n" +
                    "                    b = a.match(K) || [];\n" +
                    "                    while (c = this[i++])\n" +
                    "                        if (e = mb(c), d = 1 === c.nodeType && (\" \" + e + \" \").replace(lb, \" \")) {\n" +
                    "                            g = 0;\n" +
                    "                            while (f = b[g++])\n" +
                    "                                while (d.indexOf(\" \" + f + \" \") > -1) d = d.replace(\" \" + f + \" \", \" \");\n" +
                    "                            h = r.trim(d), e !== h && c.setAttribute(\"class\", h)\n" +
                    "                        }\n" +
                    "                }\n" +
                    "                return this\n" +
                    "            },\n" +
                    "            toggleClass: function(a, b) {\n" +
                    "                var c = typeof a;\n" +
                    "                return \"boolean\" == typeof b && \"string\" === c ? b ? this.addClass(a) : this.removeClass(a) : r.isFunction(a) ? this.each(function(c) {\n" +
                    "                    r(this).toggleClass(a.call(this, c, mb(this), b), b)\n" +
                    "                }) : this.each(function() {\n" +
                    "                    var b, d, e, f;\n" +
                    "                    if (\"string\" === c) {\n" +
                    "                        d = 0, e = r(this), f = a.match(K) || [];\n" +
                    "                        while (b = f[d++]) e.hasClass(b) ? e.removeClass(b) : e.addClass(b)\n" +
                    "                    } else void 0 !== a && \"boolean\" !== c || (b = mb(this), b && V.set(this, \"__className__\", b), this.setAttribute && this.setAttribute(\"class\", b || a === !1 ? \"\" : V.get(this, \"__className__\") || \"\"))\n" +
                    "                })\n" +
                    "            },\n" +
                    "            hasClass: function(a) {\n" +
                    "                var b, c, d = 0;\n" +
                    "                b = \" \" + a + \" \";\n" +
                    "                while (c = this[d++])\n" +
                    "                    if (1 === c.nodeType && (\" \" + mb(c) + \" \").replace(lb, \" \").indexOf(b) > -1) return !0;\n" +
                    "                return !1\n" +
                    "            }\n" +
                    "        });\n" +
                    "        var nb = /\\r/g,\n" +
                    "            ob = /[\\x20\\t\\r\\n\\f]+/g;\n" +
                    "        r.fn.extend({\n" +
                    "            val: function(a) {\n" +
                    "                var b, c, d, e = this[0]; {\n" +
                    "                    if (arguments.length) return d = r.isFunction(a), this.each(function(c) {\n" +
                    "                        var e;\n" +
                    "                        1 === this.nodeType && (e = d ? a.call(this, c, r(this).val()) : a, null == e ? e = \"\" : \"number\" == typeof e ? e += \"\" : r.isArray(e) && (e = r.map(e, function(a) {\n" +
                    "                            return null == a ? \"\" : a + \"\"\n" +
                    "                        })), b = r.valHooks[this.type] || r.valHooks[this.nodeName.toLowerCase()], b && \"set\" in b && void 0 !== b.set(this, e, \"value\") || (this.value = e))\n" +
                    "                    });\n" +
                    "                    if (e) return b = r.valHooks[e.type] || r.valHooks[e.nodeName.toLowerCase()], b && \"get\" in b && void 0 !== (c = b.get(e, \"value\")) ? c : (c = e.value, \"string\" == typeof c ? c.replace(nb, \"\") : null == c ? \"\" : c)\n" +
                    "                }\n" +
                    "            }\n" +
                    "        }), r.extend({\n" +
                    "            valHooks: {\n" +
                    "                option: {\n" +
                    "                    get: function(a) {\n" +
                    "                        var b = r.find.attr(a, \"value\");\n" +
                    "                        return null != b ? b : r.trim(r.text(a)).replace(ob, \" \")\n" +
                    "                    }\n" +
                    "                },\n" +
                    "                select: {\n" +
                    "                    get: function(a) {\n" +
                    "                        for (var b, c, d = a.options, e = a.selectedIndex, f = \"select-one\" === a.type, g = f ? null : [], h = f ? e + 1 : d.length, i = 0 > e ? h : f ? e : 0; h > i; i++)\n" +
                    "                            if (c = d[i], (c.selected || i === e) && !c.disabled && (!c.parentNode.disabled || !r.nodeName(c.parentNode, \"optgroup\"))) {\n" +
                    "                                if (b = r(c).val(), f) return b;\n" +
                    "                                g.push(b)\n" +
                    "                            }\n" +
                    "                        return g\n" +
                    "                    },\n" +
                    "                    set: function(a, b) {\n" +
                    "                        var c, d, e = a.options,\n" +
                    "                            f = r.makeArray(b),\n" +
                    "                            g = e.length;\n" +
                    "                        while (g--) d = e[g], (d.selected = r.inArray(r.valHooks.option.get(d), f) > -1) && (c = !0);\n" +
                    "                        return c || (a.selectedIndex = -1), f\n" +
                    "                    }\n" +
                    "                }\n" +
                    "            }\n" +
                    "        }), r.each([\"radio\", \"checkbox\"], function() {\n" +
                    "            r.valHooks[this] = {\n" +
                    "                set: function(a, b) {\n" +
                    "                    return r.isArray(b) ? a.checked = r.inArray(r(a).val(), b) > -1 : void 0\n" +
                    "                }\n" +
                    "            }, o.checkOn || (r.valHooks[this].get = function(a) {\n" +
                    "                return null === a.getAttribute(\"value\") ? \"on\" : a.value\n" +
                    "            })\n" +
                    "        });\n" +
                    "        var pb = /^(?:focusinfocus|focusoutblur)$/;\n" +
                    "        r.extend(r.event, {\n" +
                    "            trigger: function(b, c, e, f) {\n" +
                    "                var g, h, i, j, k, m, n, o = [e || d],\n" +
                    "                    p = l.call(b, \"type\") ? b.type : b,\n" +
                    "                    q = l.call(b, \"namespace\") ? b.namespace.split(\".\") : [];\n" +
                    "                if (h = i = e = e || d, 3 !== e.nodeType && 8 !== e.nodeType && !pb.test(p + r.event.triggered) && (p.indexOf(\".\") > -1 && (q = p.split(\".\"), p = q.shift(), q.sort()), k = p.indexOf(\":\") < 0 && \"on\" + p, b = b[r.expando] ? b : new r.Event(p, \"object\" == typeof b && b), b.isTrigger = f ? 2 : 3, b.namespace = q.join(\".\"), b.rnamespace = b.namespace ? new RegExp(\"(^|\\\\.)\" + q.join(\"\\\\.(?:.*\\\\.|)\") + \"(\\\\.|$)\") : null, b.result = void 0, b.target || (b.target = e), c = null == c ? [b] : r.makeArray(c, [b]), n = r.event.special[p] || {}, f || !n.trigger || n.trigger.apply(e, c) !== !1)) {\n" +
                    "                    if (!f && !n.noBubble && !r.isWindow(e)) {\n" +
                    "                        for (j = n.delegateType || p, pb.test(j + p) || (h = h.parentNode); h; h = h.parentNode) o.push(h), i = h;\n" +
                    "                        i === (e.ownerDocument || d) && o.push(i.defaultView || i.parentWindow || a)\n" +
                    "                    }\n" +
                    "                    g = 0;\n" +
                    "                    while ((h = o[g++]) && !b.isPropagationStopped()) b.type = g > 1 ? j : n.bindType || p, m = (V.get(h, \"events\") || {})[b.type] && V.get(h, \"handle\"), m && m.apply(h, c), m = k && h[k], m && m.apply && T(h) && (b.result = m.apply(h, c), b.result === !1 && b.preventDefault());\n" +
                    "                    return b.type = p, f || b.isDefaultPrevented() || n._default && n._default.apply(o.pop(), c) !== !1 || !T(e) || k && r.isFunction(e[p]) && !r.isWindow(e) && (i = e[k], i && (e[k] = null), r.event.triggered = p, e[p](), r.event.triggered = void 0, i && (e[k] = i)), b.result\n" +
                    "                }\n" +
                    "            },\n" +
                    "            simulate: function(a, b, c) {\n" +
                    "                var d = r.extend(new r.Event, c, {\n" +
                    "                    type: a,\n" +
                    "                    isSimulated: !0\n" +
                    "                });\n" +
                    "                r.event.trigger(d, null, b)\n" +
                    "            }\n" +
                    "        }), r.fn.extend({\n" +
                    "            trigger: function(a, b) {\n" +
                    "                return this.each(function() {\n" +
                    "                    r.event.trigger(a, b, this)\n" +
                    "                })\n" +
                    "            },\n" +
                    "            triggerHandler: function(a, b) {\n" +
                    "                var c = this[0];\n" +
                    "                return c ? r.event.trigger(a, b, c, !0) : void 0\n" +
                    "            }\n" +
                    "        }), r.each(\"blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu\".split(\" \"), function(a, b) {\n" +
                    "            r.fn[b] = function(a, c) {\n" +
                    "                return arguments.length > 0 ? this.on(b, null, a, c) : this.trigger(b)\n" +
                    "            }\n" +
                    "        }), r.fn.extend({\n" +
                    "            hover: function(a, b) {\n" +
                    "                return this.mouseenter(a).mouseleave(b || a)\n" +
                    "            }\n" +
                    "        }), o.focusin = \"onfocusin\" in a, o.focusin || r.each({\n" +
                    "            focus: \"focusin\",\n" +
                    "            blur: \"focusout\"\n" +
                    "        }, function(a, b) {\n" +
                    "            var c = function(a) {\n" +
                    "                r.event.simulate(b, a.target, r.event.fix(a))\n" +
                    "            };\n" +
                    "            r.event.special[b] = {\n" +
                    "                setup: function() {\n" +
                    "                    var d = this.ownerDocument || this,\n" +
                    "                        e = V.access(d, b);\n" +
                    "                    e || d.addEventListener(a, c, !0), V.access(d, b, (e || 0) + 1)\n" +
                    "                },\n" +
                    "                teardown: function() {\n" +
                    "                    var d = this.ownerDocument || this,\n" +
                    "                        e = V.access(d, b) - 1;\n" +
                    "                    e ? V.access(d, b, e) : (d.removeEventListener(a, c, !0), V.remove(d, b))\n" +
                    "                }\n" +
                    "            }\n" +
                    "        });\n" +
                    "        var qb = a.location,\n" +
                    "            rb = r.now(),\n" +
                    "            sb = /\\?/;\n" +
                    "        r.parseXML = function(b) {\n" +
                    "            var c;\n" +
                    "            if (!b || \"string\" != typeof b) return null;\n" +
                    "            try {\n" +
                    "                c = (new a.DOMParser).parseFromString(b, \"text/xml\")\n" +
                    "            } catch (d) {\n" +
                    "                c = void 0\n" +
                    "            }\n" +
                    "            return c && !c.getElementsByTagName(\"parsererror\").length || r.error(\"Invalid XML: \" + b), c\n" +
                    "        };\n" +
                    "        var tb = /\\[\\]$/,\n" +
                    "            ub = /\\r?\\n/g,\n" +
                    "            vb = /^(?:submit|button|image|reset|file)$/i,\n" +
                    "            wb = /^(?:input|select|textarea|keygen)/i;\n" +
                    "\n" +
                    "        function xb(a, b, c, d) {\n" +
                    "            var e;\n" +
                    "            if (r.isArray(b)) r.each(b, function(b, e) {\n" +
                    "                c || tb.test(a) ? d(a, e) : xb(a + \"[\" + (\"object\" == typeof e && null != e ? b : \"\") + \"]\", e, c, d)\n" +
                    "            });\n" +
                    "            else if (c || \"object\" !== r.type(b)) d(a, b);\n" +
                    "            else\n" +
                    "                for (e in b) xb(a + \"[\" + e + \"]\", b[e], c, d)\n" +
                    "        }\n" +
                    "        r.param = function(a, b) {\n" +
                    "            var c, d = [],\n" +
                    "                e = function(a, b) {\n" +
                    "                    var c = r.isFunction(b) ? b() : b;\n" +
                    "                    d[d.length] = encodeURIComponent(a) + \"=\" + encodeURIComponent(null == c ? \"\" : c)\n" +
                    "                };\n" +
                    "            if (r.isArray(a) || a.jquery && !r.isPlainObject(a)) r.each(a, function() {\n" +
                    "                e(this.name, this.value)\n" +
                    "            });\n" +
                    "            else\n" +
                    "                for (c in a) xb(c, a[c], b, e);\n" +
                    "            return d.join(\"&\")\n" +
                    "        }, r.fn.extend({\n" +
                    "            serialize: function() {\n" +
                    "                return r.param(this.serializeArray())\n" +
                    "            },\n" +
                    "            serializeArray: function() {\n" +
                    "                return this.map(function() {\n" +
                    "                    var a = r.prop(this, \"elements\");\n" +
                    "                    return a ? r.makeArray(a) : this\n" +
                    "                }).filter(function() {\n" +
                    "                    var a = this.type;\n" +
                    "                    return this.name && !r(this).is(\":disabled\") && wb.test(this.nodeName) && !vb.test(a) && (this.checked || !ha.test(a))\n" +
                    "                }).map(function(a, b) {\n" +
                    "                    var c = r(this).val();\n" +
                    "                    return null == c ? null : r.isArray(c) ? r.map(c, function(a) {\n" +
                    "                        return {\n" +
                    "                            name: b.name,\n" +
                    "                            value: a.replace(ub, \"\\r\\n\")\n" +
                    "                        }\n" +
                    "                    }) : {\n" +
                    "                        name: b.name,\n" +
                    "                        value: c.replace(ub, \"\\r\\n\")\n" +
                    "                    }\n" +
                    "                }).get()\n" +
                    "            }\n" +
                    "        });\n" +
                    "        var yb = /%20/g,\n" +
                    "            zb = /#.*$/,\n" +
                    "            Ab = /([?&])_=[^&]*/,\n" +
                    "            Bb = /^(.*?):[ \\t]*([^\\r\\n]*)$/gm,\n" +
                    "            Cb = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,\n" +
                    "            Db = /^(?:GET|HEAD)$/,\n" +
                    "            Eb = /^\\/\\//,\n" +
                    "            Fb = {},\n" +
                    "            Gb = {},\n" +
                    "            Hb = \"*/\".concat(\"*\"),\n" +
                    "            Ib = d.createElement(\"a\");\n" +
                    "        Ib.href = qb.href;\n" +
                    "\n" +
                    "        function Jb(a) {\n" +
                    "            return function(b, c) {\n" +
                    "                \"string\" != typeof b && (c = b, b = \"*\");\n" +
                    "                var d, e = 0,\n" +
                    "                    f = b.toLowerCase().match(K) || [];\n" +
                    "                if (r.isFunction(c))\n" +
                    "                    while (d = f[e++]) \"+\" === d[0] ? (d = d.slice(1) || \"*\", (a[d] = a[d] || []).unshift(c)) : (a[d] = a[d] || []).push(c)\n" +
                    "            }\n" +
                    "        }\n" +
                    "\n" +
                    "        function Kb(a, b, c, d) {\n" +
                    "            var e = {},\n" +
                    "                f = a === Gb;\n" +
                    "\n" +
                    "            function g(h) {\n" +
                    "                var i;\n" +
                    "                return e[h] = !0, r.each(a[h] || [], function(a, h) {\n" +
                    "                    var j = h(b, c, d);\n" +
                    "                    return \"string\" != typeof j || f || e[j] ? f ? !(i = j) : void 0 : (b.dataTypes.unshift(j), g(j), !1)\n" +
                    "                }), i\n" +
                    "            }\n" +
                    "            return g(b.dataTypes[0]) || !e[\"*\"] && g(\"*\")\n" +
                    "        }\n" +
                    "\n" +
                    "        function Lb(a, b) {\n" +
                    "            var c, d, e = r.ajaxSettings.flatOptions || {};\n" +
                    "            for (c in b) void 0 !== b[c] && ((e[c] ? a : d || (d = {}))[c] = b[c]);\n" +
                    "            return d && r.extend(!0, a, d), a\n" +
                    "        }\n" +
                    "\n" +
                    "        function Mb(a, b, c) {\n" +
                    "            var d, e, f, g, h = a.contents,\n" +
                    "                i = a.dataTypes;\n" +
                    "            while (\"*\" === i[0]) i.shift(), void 0 === d && (d = a.mimeType || b.getResponseHeader(\"Content-Type\"));\n" +
                    "            if (d)\n" +
                    "                for (e in h)\n" +
                    "                    if (h[e] && h[e].test(d)) {\n" +
                    "                        i.unshift(e);\n" +
                    "                        break\n" +
                    "                    }\n" +
                    "            if (i[0] in c) f = i[0];\n" +
                    "            else {\n" +
                    "                for (e in c) {\n" +
                    "                    if (!i[0] || a.converters[e + \" \" + i[0]]) {\n" +
                    "                        f = e;\n" +
                    "                        break\n" +
                    "                    }\n" +
                    "                    g || (g = e)\n" +
                    "                }\n" +
                    "                f = f || g\n" +
                    "            }\n" +
                    "            return f ? (f !== i[0] && i.unshift(f), c[f]) : void 0\n" +
                    "        }\n" +
                    "\n" +
                    "        function Nb(a, b, c, d) {\n" +
                    "            var e, f, g, h, i, j = {},\n" +
                    "                k = a.dataTypes.slice();\n" +
                    "            if (k[1])\n" +
                    "                for (g in a.converters) j[g.toLowerCase()] = a.converters[g];\n" +
                    "            f = k.shift();\n" +
                    "            while (f)\n" +
                    "                if (a.responseFields[f] && (c[a.responseFields[f]] = b), !i && d && a.dataFilter && (b = a.dataFilter(b, a.dataType)), i = f, f = k.shift())\n" +
                    "                    if (\"*\" === f) f = i;\n" +
                    "                    else if (\"*\" !== i && i !== f) {\n" +
                    "                if (g = j[i + \" \" + f] || j[\"* \" + f], !g)\n" +
                    "                    for (e in j)\n" +
                    "                        if (h = e.split(\" \"), h[1] === f && (g = j[i + \" \" + h[0]] || j[\"* \" + h[0]])) {\n" +
                    "                            g === !0 ? g = j[e] : j[e] !== !0 && (f = h[0], k.unshift(h[1]));\n" +
                    "                            break\n" +
                    "                        }\n" +
                    "                if (g !== !0)\n" +
                    "                    if (g && a[\"throws\"]) b = g(b);\n" +
                    "                    else try {\n" +
                    "                        b = g(b)\n" +
                    "                    } catch (l) {\n" +
                    "                        return {\n" +
                    "                            state: \"parsererror\",\n" +
                    "                            error: g ? l : \"No conversion from \" + i + \" to \" + f\n" +
                    "                        }\n" +
                    "                    }\n" +
                    "            }\n" +
                    "            return {\n" +
                    "                state: \"success\",\n" +
                    "                data: b\n" +
                    "            }\n" +
                    "        }\n" +
                    "        r.extend({\n" +
                    "            active: 0,\n" +
                    "            lastModified: {},\n" +
                    "            etag: {},\n" +
                    "            ajaxSettings: {\n" +
                    "                url: qb.href,\n" +
                    "                type: \"GET\",\n" +
                    "                isLocal: Cb.test(qb.protocol),\n" +
                    "                global: !0,\n" +
                    "                processData: !0,\n" +
                    "                async: !0,\n" +
                    "                contentType: \"application/x-www-form-urlencoded; charset=UTF-8\",\n" +
                    "                accepts: {\n" +
                    "                    \"*\": Hb,\n" +
                    "                    text: \"text/plain\",\n" +
                    "                    html: \"text/html\",\n" +
                    "                    xml: \"application/xml, text/xml\",\n" +
                    "                    json: \"application/json, text/javascript\"\n" +
                    "                },\n" +
                    "                contents: {\n" +
                    "                    xml: /\\bxml\\b/,\n" +
                    "                    html: /\\bhtml/,\n" +
                    "                    json: /\\bjson\\b/\n" +
                    "                },\n" +
                    "                responseFields: {\n" +
                    "                    xml: \"responseXML\",\n" +
                    "                    text: \"responseText\",\n" +
                    "                    json: \"responseJSON\"\n" +
                    "                },\n" +
                    "                converters: {\n" +
                    "                    \"* text\": String,\n" +
                    "                    \"text html\": !0,\n" +
                    "                    \"text json\": JSON.parse,\n" +
                    "                    \"text xml\": r.parseXML\n" +
                    "                },\n" +
                    "                flatOptions: {\n" +
                    "                    url: !0,\n" +
                    "                    context: !0\n" +
                    "                }\n" +
                    "            },\n" +
                    "            ajaxSetup: function(a, b) {\n" +
                    "                return b ? Lb(Lb(a, r.ajaxSettings), b) : Lb(r.ajaxSettings, a)\n" +
                    "            },\n" +
                    "            ajaxPrefilter: Jb(Fb),\n" +
                    "            ajaxTransport: Jb(Gb),\n" +
                    "            ajax: function(b, c) {\n" +
                    "                \"object\" == typeof b && (c = b, b = void 0), c = c || {};\n" +
                    "                var e, f, g, h, i, j, k, l, m, n, o = r.ajaxSetup({}, c),\n" +
                    "                    p = o.context || o,\n" +
                    "                    q = o.context && (p.nodeType || p.jquery) ? r(p) : r.event,\n" +
                    "                    s = r.Deferred(),\n" +
                    "                    t = r.Callbacks(\"once memory\"),\n" +
                    "                    u = o.statusCode || {},\n" +
                    "                    v = {},\n" +
                    "                    w = {},\n" +
                    "                    x = \"canceled\",\n" +
                    "                    y = {\n" +
                    "                        readyState: 0,\n" +
                    "                        getResponseHeader: function(a) {\n" +
                    "                            var b;\n" +
                    "                            if (k) {\n" +
                    "                                if (!h) {\n" +
                    "                                    h = {};\n" +
                    "                                    while (b = Bb.exec(g)) h[b[1].toLowerCase()] = b[2]\n" +
                    "                                }\n" +
                    "                                b = h[a.toLowerCase()]\n" +
                    "                            }\n" +
                    "                            return null == b ? null : b\n" +
                    "                        },\n" +
                    "                        getAllResponseHeaders: function() {\n" +
                    "                            return k ? g : null\n" +
                    "                        },\n" +
                    "                        setRequestHeader: function(a, b) {\n" +
                    "                            return null == k && (a = w[a.toLowerCase()] = w[a.toLowerCase()] || a, v[a] = b), this\n" +
                    "                        },\n" +
                    "                        overrideMimeType: function(a) {\n" +
                    "                            return null == k && (o.mimeType = a), this\n" +
                    "                        },\n" +
                    "                        statusCode: function(a) {\n" +
                    "                            var b;\n" +
                    "                            if (a)\n" +
                    "                                if (k) y.always(a[y.status]);\n" +
                    "                                else\n" +
                    "                                    for (b in a) u[b] = [u[b], a[b]];\n" +
                    "                            return this\n" +
                    "                        },\n" +
                    "                        abort: function(a) {\n" +
                    "                            var b = a || x;\n" +
                    "                            return e && e.abort(b), A(0, b), this\n" +
                    "                        }\n" +
                    "                    };\n" +
                    "                if (s.promise(y), o.url = ((b || o.url || qb.href) + \"\").replace(Eb, qb.protocol + \"//\"), o.type = c.method || c.type || o.method || o.type, o.dataTypes = (o.dataType || \"*\").toLowerCase().match(K) || [\"\"], null == o.crossDomain) {\n" +
                    "                    j = d.createElement(\"a\");\n" +
                    "                    try {\n" +
                    "                        j.href = o.url, j.href = j.href, o.crossDomain = Ib.protocol + \"//\" + Ib.host != j.protocol + \"//\" + j.host\n" +
                    "                    } catch (z) {\n" +
                    "                        o.crossDomain = !0\n" +
                    "                    }\n" +
                    "                }\n" +
                    "                if (o.data && o.processData && \"string\" != typeof o.data && (o.data = r.param(o.data, o.traditional)), Kb(Fb, o, c, y), k) return y;\n" +
                    "                l = r.event && o.global, l && 0 === r.active++ && r.event.trigger(\"ajaxStart\"), o.type = o.type.toUpperCase(), o.hasContent = !Db.test(o.type), f = o.url.replace(zb, \"\"), o.hasContent ? o.data && o.processData && 0 === (o.contentType || \"\").indexOf(\"application/x-www-form-urlencoded\") && (o.data = o.data.replace(yb, \"+\")) : (n = o.url.slice(f.length), o.data && (f += (sb.test(f) ? \"&\" : \"?\") + o.data, delete o.data), o.cache === !1 && (f = f.replace(Ab, \"\"), n = (sb.test(f) ? \"&\" : \"?\") + \"_=\" + rb++ + n), o.url = f + n), o.ifModified && (r.lastModified[f] && y.setRequestHeader(\"If-Modified-Since\", r.lastModified[f]), r.etag[f] && y.setRequestHeader(\"If-None-Match\", r.etag[f])), (o.data && o.hasContent && o.contentType !== !1 || c.contentType) && y.setRequestHeader(\"Content-Type\", o.contentType), y.setRequestHeader(\"Accept\", o.dataTypes[0] && o.accepts[o.dataTypes[0]] ? o.accepts[o.dataTypes[0]] + (\"*\" !== o.dataTypes[0] ? \", \" + Hb + \"; q=0.01\" : \"\") : o.accepts[\"*\"]);\n" +
                    "                for (m in o.headers) y.setRequestHeader(m, o.headers[m]);\n" +
                    "                if (o.beforeSend && (o.beforeSend.call(p, y, o) === !1 || k)) return y.abort();\n" +
                    "                if (x = \"abort\", t.add(o.complete), y.done(o.success), y.fail(o.error), e = Kb(Gb, o, c, y)) {\n" +
                    "                    if (y.readyState = 1, l && q.trigger(\"ajaxSend\", [y, o]), k) return y;\n" +
                    "                    o.async && o.timeout > 0 && (i = a.setTimeout(function() {\n" +
                    "                        y.abort(\"timeout\")\n" +
                    "                    }, o.timeout));\n" +
                    "                    try {\n" +
                    "                        k = !1, e.send(v, A)\n" +
                    "                    } catch (z) {\n" +
                    "                        if (k) throw z;\n" +
                    "                        A(-1, z)\n" +
                    "                    }\n" +
                    "                } else A(-1, \"No Transport\");\n" +
                    "\n" +
                    "                function A(b, c, d, h) {\n" +
                    "                    var j, m, n, v, w, x = c;\n" +
                    "                    k || (k = !0, i && a.clearTimeout(i), e = void 0, g = h || \"\", y.readyState = b > 0 ? 4 : 0, j = b >= 200 && 300 > b || 304 === b, d && (v = Mb(o, y, d)), v = Nb(o, v, y, j), j ? (o.ifModified && (w = y.getResponseHeader(\"Last-Modified\"), w && (r.lastModified[f] = w), w = y.getResponseHeader(\"etag\"), w && (r.etag[f] = w)), 204 === b || \"HEAD\" === o.type ? x = \"nocontent\" : 304 === b ? x = \"notmodified\" : (x = v.state, m = v.data, n = v.error, j = !n)) : (n = x, !b && x || (x = \"error\", 0 > b && (b = 0))), y.status = b, y.statusText = (c || x) + \"\", j ? s.resolveWith(p, [m, x, y]) : s.rejectWith(p, [y, x, n]), y.statusCode(u), u = void 0, l && q.trigger(j ? \"ajaxSuccess\" : \"ajaxError\", [y, o, j ? m : n]), t.fireWith(p, [y, x]), l && (q.trigger(\"ajaxComplete\", [y, o]), --r.active || r.event.trigger(\"ajaxStop\")))\n" +
                    "                }\n" +
                    "                return y\n" +
                    "            },\n" +
                    "            getJSON: function(a, b, c) {\n" +
                    "                return r.get(a, b, c, \"json\")\n" +
                    "            },\n" +
                    "            getScript: function(a, b) {\n" +
                    "                return r.get(a, void 0, b, \"script\")\n" +
                    "            }\n" +
                    "        }), r.each([\"get\", \"post\"], function(a, b) {\n" +
                    "            r[b] = function(a, c, d, e) {\n" +
                    "                return r.isFunction(c) && (e = e || d, d = c, c = void 0), r.ajax(r.extend({\n" +
                    "                    url: a,\n" +
                    "                    type: b,\n" +
                    "                    dataType: e,\n" +
                    "                    data: c,\n" +
                    "                    success: d\n" +
                    "                }, r.isPlainObject(a) && a))\n" +
                    "            }\n" +
                    "        }), r._evalUrl = function(a) {\n" +
                    "            return r.ajax({\n" +
                    "                url: a,\n" +
                    "                type: \"GET\",\n" +
                    "                dataType: \"script\",\n" +
                    "                cache: !0,\n" +
                    "                async: !1,\n" +
                    "                global: !1,\n" +
                    "                \"throws\": !0\n" +
                    "            })\n" +
                    "        }, r.fn.extend({\n" +
                    "            wrapAll: function(a) {\n" +
                    "                var b;\n" +
                    "                return this[0] && (r.isFunction(a) && (a = a.call(this[0])), b = r(a, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && b.insertBefore(this[0]), b.map(function() {\n" +
                    "                    var a = this;\n" +
                    "                    while (a.firstElementChild) a = a.firstElementChild;\n" +
                    "                    return a\n" +
                    "                }).append(this)), this\n" +
                    "            },\n" +
                    "            wrapInner: function(a) {\n" +
                    "                return r.isFunction(a) ? this.each(function(b) {\n" +
                    "                    r(this).wrapInner(a.call(this, b))\n" +
                    "                }) : this.each(function() {\n" +
                    "                    var b = r(this),\n" +
                    "                        c = b.contents();\n" +
                    "                    c.length ? c.wrapAll(a) : b.append(a)\n" +
                    "                })\n" +
                    "            },\n" +
                    "            wrap: function(a) {\n" +
                    "                var b = r.isFunction(a);\n" +
                    "                return this.each(function(c) {\n" +
                    "                    r(this).wrapAll(b ? a.call(this, c) : a)\n" +
                    "                })\n" +
                    "            },\n" +
                    "            unwrap: function(a) {\n" +
                    "                return this.parent(a).not(\"body\").each(function() {\n" +
                    "                    r(this).replaceWith(this.childNodes)\n" +
                    "                }), this\n" +
                    "            }\n" +
                    "        }), r.expr.pseudos.hidden = function(a) {\n" +
                    "            return !r.expr.pseudos.visible(a)\n" +
                    "        }, r.expr.pseudos.visible = function(a) {\n" +
                    "            return !!(a.offsetWidth || a.offsetHeight || a.getClientRects().length)\n" +
                    "        }, r.ajaxSettings.xhr = function() {\n" +
                    "            try {\n" +
                    "                return new a.XMLHttpRequest\n" +
                    "            } catch (b) {}\n" +
                    "        };\n" +
                    "        var Ob = {\n" +
                    "                0: 200,\n" +
                    "                1223: 204\n" +
                    "            },";
            String msg4="            Pb = r.ajaxSettings.xhr();\n" +
                    "        o.cors = !!Pb && \"withCredentials\" in Pb, o.ajax = Pb = !!Pb, r.ajaxTransport(function(b) {\n" +
                    "            var c, d;\n" +
                    "            return o.cors || Pb && !b.crossDomain ? {\n" +
                    "                send: function(e, f) {\n" +
                    "                    var g, h = b.xhr();\n" +
                    "                    if (h.open(b.type, b.url, b.async, b.username, b.password), b.xhrFields)\n" +
                    "                        for (g in b.xhrFields) h[g] = b.xhrFields[g];\n" +
                    "                    b.mimeType && h.overrideMimeType && h.overrideMimeType(b.mimeType), b.crossDomain || e[\"X-Requested-With\"] || (e[\"X-Requested-With\"] = \"XMLHttpRequest\");\n" +
                    "                    for (g in e) h.setRequestHeader(g, e[g]);\n" +
                    "                    c = function(a) {\n" +
                    "                        return function() {\n" +
                    "                            c && (c = d = h.onload = h.onerror = h.onabort = h.onreadystatechange = null, \"abort\" === a ? h.abort() : \"error\" === a ? \"number\" != typeof h.status ? f(0, \"error\") : f(h.status, h.statusText) : f(Ob[h.status] || h.status, h.statusText, \"text\" !== (h.responseType || \"text\") || \"string\" != typeof h.responseText ? {\n" +
                    "                                binary: h.response\n" +
                    "                            } : {\n" +
                    "                                text: h.responseText\n" +
                    "                            }, h.getAllResponseHeaders()))\n" +
                    "                        }\n" +
                    "                    }, h.onload = c(), d = h.onerror = c(\"error\"), void 0 !== h.onabort ? h.onabort = d : h.onreadystatechange = function() {\n" +
                    "                        4 === h.readyState && a.setTimeout(function() {\n" +
                    "                            c && d()\n" +
                    "                        })\n" +
                    "                    }, c = c(\"abort\");\n" +
                    "                    try {\n" +
                    "                        h.send(b.hasContent && b.data || null)\n" +
                    "                    } catch (i) {\n" +
                    "                        if (c) throw i\n" +
                    "                    }\n" +
                    "                },\n" +
                    "                abort: function() {\n" +
                    "                    c && c()\n" +
                    "                }\n" +
                    "            } : void 0\n" +
                    "        }), r.ajaxPrefilter(function(a) {\n" +
                    "            a.crossDomain && (a.contents.script = !1)\n" +
                    "        }), r.ajaxSetup({\n" +
                    "            accepts: {\n" +
                    "                script: \"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript\"\n" +
                    "            },\n" +
                    "            contents: {\n" +
                    "                script: /\\b(?:java|ecma)script\\b/\n" +
                    "            },\n" +
                    "            converters: {\n" +
                    "                \"text script\": function(a) {\n" +
                    "                    return r.globalEval(a), a\n" +
                    "                }\n" +
                    "            }\n" +
                    "        }), r.ajaxPrefilter(\"script\", function(a) {\n" +
                    "            void 0 === a.cache && (a.cache = !1), a.crossDomain && (a.type = \"GET\")\n" +
                    "        }), r.ajaxTransport(\"script\", function(a) {\n" +
                    "            if (a.crossDomain) {\n" +
                    "                var b, c;\n" +
                    "                return {\n" +
                    "                    send: function(e, f) {\n" +
                    "                        b = r(\"<script>\").prop({\n" +
                    "                            charset: a.scriptCharset,\n" +
                    "                            src: a.url\n" +
                    "                        }).on(\"load error\", c = function(a) {\n" +
                    "                            b.remove(), c = null, a && f(\"error\" === a.type ? 404 : 200, a.type)\n" +
                    "                        }), d.head.appendChild(b[0])\n" +
                    "                    },\n" +
                    "                    abort: function() {\n" +
                    "                        c && c()\n" +
                    "                    }\n" +
                    "                }\n" +
                    "            }\n" +
                    "        });\n" +
                    "        var Qb = [],\n" +
                    "            Rb = /(=)\\?(?=&|$)|\\?\\?/;\n" +
                    "        r.ajaxSetup({\n" +
                    "            jsonp: \"callback\",\n" +
                    "            jsonpCallback: function() {\n" +
                    "                var a = Qb.pop() || r.expando + \"_\" + rb++;\n" +
                    "                return this[a] = !0, a\n" +
                    "            }\n" +
                    "        }), r.ajaxPrefilter(\"json jsonp\", function(b, c, d) {\n" +
                    "            var e, f, g, h = b.jsonp !== !1 && (Rb.test(b.url) ? \"url\" : \"string\" == typeof b.data && 0 === (b.contentType || \"\").indexOf(\"application/x-www-form-urlencoded\") && Rb.test(b.data) && \"data\");\n" +
                    "            return h || \"jsonp\" === b.dataTypes[0] ? (e = b.jsonpCallback = r.isFunction(b.jsonpCallback) ? b.jsonpCallback() : b.jsonpCallback, h ? b[h] = b[h].replace(Rb, \"$1\" + e) : b.jsonp !== !1 && (b.url += (sb.test(b.url) ? \"&\" : \"?\") + b.jsonp + \"=\" + e), b.converters[\"script json\"] = function() {\n" +
                    "                return g || r.error(e + \" was not called\"), g[0]\n" +
                    "            }, b.dataTypes[0] = \"json\", f = a[e], a[e] = function() {\n" +
                    "                g = arguments\n" +
                    "            }, d.always(function() {\n" +
                    "                void 0 === f ? r(a).removeProp(e) : a[e] = f, b[e] && (b.jsonpCallback = c.jsonpCallback, Qb.push(e)), g && r.isFunction(f) && f(g[0]), g = f = void 0\n" +
                    "            }), \"script\") : void 0\n" +
                    "        }), o.createHTMLDocument = function() {\n" +
                    "            var a = d.implementation.createHTMLDocument(\"\").body;\n" +
                    "            return a.innerHTML = \"<form></form><form></form>\", 2 === a.childNodes.length\n" +
                    "        }(), r.parseHTML = function(a, b, c) {\n" +
                    "            if (\"string\" != typeof a) return [];\n" +
                    "            \"boolean\" == typeof b && (c = b, b = !1);\n" +
                    "            var e, f, g;\n" +
                    "            return b || (o.createHTMLDocument ? (b = d.implementation.createHTMLDocument(\"\"), e = b.createElement(\"base\"), e.href = d.location.href, b.head.appendChild(e)) : b = d), f = B.exec(a), g = !c && [], f ? [b.createElement(f[1])] : (f = oa([a], b, g), g && g.length && r(g).remove(), r.merge([], f.childNodes))\n" +
                    "        }, r.fn.load = function(a, b, c) {\n" +
                    "            var d, e, f, g = this,\n" +
                    "                h = a.indexOf(\" \");\n" +
                    "            return h > -1 && (d = r.trim(a.slice(h)), a = a.slice(0, h)), r.isFunction(b) ? (c = b, b = void 0) : b && \"object\" == typeof b && (e = \"POST\"), g.length > 0 && r.ajax({\n" +
                    "                url: a,\n" +
                    "                type: e || \"GET\",\n" +
                    "                dataType: \"html\",\n" +
                    "                data: b\n" +
                    "            }).done(function(a) {\n" +
                    "                f = arguments, g.html(d ? r(\"<div>\").append(r.parseHTML(a)).find(d) : a)\n" +
                    "            }).always(c && function(a, b) {\n" +
                    "                g.each(function() {\n" +
                    "                    c.apply(this, f || [a.responseText, b, a])\n" +
                    "                })\n" +
                    "            }), this\n" +
                    "        }, r.each([\"ajaxStart\", \"ajaxStop\", \"ajaxComplete\", \"ajaxError\", \"ajaxSuccess\", \"ajaxSend\"], function(a, b) {\n" +
                    "            r.fn[b] = function(a) {\n" +
                    "                return this.on(b, a)\n" +
                    "            }\n" +
                    "        }), r.expr.pseudos.animated = function(a) {\n" +
                    "            return r.grep(r.timers, function(b) {\n" +
                    "                return a === b.elem\n" +
                    "            }).length\n" +
                    "        };\n" +
                    "\n" +
                    "        function Sb(a) {\n" +
                    "            return r.isWindow(a) ? a : 9 === a.nodeType && a.defaultView\n" +
                    "        }\n" +
                    "        r.offset = {\n" +
                    "            setOffset: function(a, b, c) {\n" +
                    "                var d, e, f, g, h, i, j, k = r.css(a, \"position\"),\n" +
                    "                    l = r(a),\n" +
                    "                    m = {};\n" +
                    "                \"static\" === k && (a.style.position = \"relative\"), h = l.offset(), f = r.css(a, \"top\"), i = r.css(a, \"left\"), j = (\"absolute\" === k || \"fixed\" === k) && (f + i).indexOf(\"auto\") > -1, j ? (d = l.position(), g = d.top, e = d.left) : (g = parseFloat(f) || 0, e = parseFloat(i) || 0), r.isFunction(b) && (b = b.call(a, c, r.extend({}, h))), null != b.top && (m.top = b.top - h.top + g), null != b.left && (m.left = b.left - h.left + e), \"using\" in b ? b.using.call(a, m) : l.css(m)\n" +
                    "            }\n" +
                    "        }, r.fn.extend({\n" +
                    "            offset: function(a) {\n" +
                    "                if (arguments.length) return void 0 === a ? this : this.each(function(b) {\n" +
                    "                    r.offset.setOffset(this, a, b)\n" +
                    "                });\n" +
                    "                var b, c, d, e, f = this[0];\n" +
                    "                if (f) return f.getClientRects().length ? (d = f.getBoundingClientRect(), d.width || d.height ? (e = f.ownerDocument, c = Sb(e), b = e.documentElement, {\n" +
                    "                    top: d.top + c.pageYOffset - b.clientTop,\n" +
                    "                    left: d.left + c.pageXOffset - b.clientLeft\n" +
                    "                }) : d) : {\n" +
                    "                    top: 0,\n" +
                    "                    left: 0\n" +
                    "                }\n" +
                    "            },\n" +
                    "            position: function() {\n" +
                    "                if (this[0]) {\n" +
                    "                    var a, b, c = this[0],\n" +
                    "                        d = {\n" +
                    "                            top: 0,\n" +
                    "                            left: 0\n" +
                    "                        };\n" +
                    "                    return \"fixed\" === r.css(c, \"position\") ? b = c.getBoundingClientRect() : (a = this.offsetParent(), b = this.offset(), r.nodeName(a[0], \"html\") || (d = a.offset()), d = {\n" +
                    "                        top: d.top + r.css(a[0], \"borderTopWidth\", !0),\n" +
                    "                        left: d.left + r.css(a[0], \"borderLeftWidth\", !0)\n" +
                    "                    }), {\n" +
                    "                        top: b.top - d.top - r.css(c, \"marginTop\", !0),\n" +
                    "                        left: b.left - d.left - r.css(c, \"marginLeft\", !0)\n" +
                    "                    }\n" +
                    "                }\n" +
                    "            },\n" +
                    "            offsetParent: function() {\n" +
                    "                return this.map(function() {\n" +
                    "                    var a = this.offsetParent;\n" +
                    "                    while (a && \"static\" === r.css(a, \"position\")) a = a.offsetParent;\n" +
                    "                    return a || pa\n" +
                    "                })\n" +
                    "            }\n" +
                    "        }), r.each({\n" +
                    "            scrollLeft: \"pageXOffset\",\n" +
                    "            scrollTop: \"pageYOffset\"\n" +
                    "        }, function(a, b) {\n" +
                    "            var c = \"pageYOffset\" === b;\n" +
                    "            r.fn[a] = function(d) {\n" +
                    "                return S(this, function(a, d, e) {\n" +
                    "                    var f = Sb(a);\n" +
                    "                    return void 0 === e ? f ? f[b] : a[d] : void(f ? f.scrollTo(c ? f.pageXOffset : e, c ? e : f.pageYOffset) : a[d] = e)\n" +
                    "                }, a, d, arguments.length)\n" +
                    "            }\n" +
                    "        }), r.each([\"top\", \"left\"], function(a, b) {\n" +
                    "            r.cssHooks[b] = Na(o.pixelPosition, function(a, c) {\n" +
                    "                return c ? (c = Ma(a, b), Ka.test(c) ? r(a).position()[b] + \"px\" : c) : void 0\n" +
                    "            })\n" +
                    "        }), r.each({\n" +
                    "            Height: \"height\",\n" +
                    "            Width: \"width\"\n" +
                    "        }, function(a, b) {\n" +
                    "            r.each({\n" +
                    "                padding: \"inner\" + a,\n" +
                    "                content: b,\n" +
                    "                \"\": \"outer\" + a\n" +
                    "            }, function(c, d) {\n" +
                    "                r.fn[d] = function(e, f) {\n" +
                    "                    var g = arguments.length && (c || \"boolean\" != typeof e),\n" +
                    "                        h = c || (e === !0 || f === !0 ? \"margin\" : \"border\");\n" +
                    "                    return S(this, function(b, c, e) {\n" +
                    "                        var f;\n" +
                    "                        return r.isWindow(b) ? 0 === d.indexOf(\"outer\") ? b[\"inner\" + a] : b.document.documentElement[\"client\" + a] : 9 === b.nodeType ? (f = b.documentElement, Math.max(b.body[\"scroll\" + a], f[\"scroll\" + a], b.body[\"offset\" + a], f[\"offset\" + a], f[\"client\" + a])) : void 0 === e ? r.css(b, c, h) : r.style(b, c, e, h)\n" +
                    "                    }, b, g ? e : void 0, g)\n" +
                    "                }\n" +
                    "            })\n" +
                    "        }), r.fn.extend({\n" +
                    "            bind: function(a, b, c) {\n" +
                    "                return this.on(a, null, b, c)\n" +
                    "            },\n" +
                    "            unbind: function(a, b) {\n" +
                    "                return this.off(a, null, b)\n" +
                    "            },\n" +
                    "            delegate: function(a, b, c, d) {\n" +
                    "                return this.on(b, a, c, d)\n" +
                    "            },\n" +
                    "            undelegate: function(a, b, c) {\n" +
                    "                return 1 === arguments.length ? this.off(a, \"**\") : this.off(b, a || \"**\", c)\n" +
                    "            }\n" +
                    "        }), r.parseJSON = JSON.parse, \"function\" == typeof define && define.amd && define(\"jquery\", [], function() {\n" +
                    "            return r\n" +
                    "        });\n" +
                    "        var Tb = a.jQuery,\n" +
                    "            Ub = a.$;\n" +
                    "        return r.noConflict = function(b) {\n" +
                    "            return a.$ === r && (a.$ = Ub), b && a.jQuery === r && (a.jQuery = Tb), r\n" +
                    "        }, b || (a.jQuery = a.$ = r), r\n" +
                    "    });\n" +
                    "    </script>\n" +
                    "    <!-- Jquery -->\n" +
                    "    <style type=\"text/css\"></style>\n" +
                    "    <script type=\"text/javascript\">\n" +
                    "    (function(f) {\n" +
                    "        if (typeof exports === \"object\" && typeof module !== \"undefined\") {\n" +
                    "            module.exports = f()\n" +
                    "        } else if (typeof define === \"function\" && define.amd) {\n" +
                    "            define([], f)\n" +
                    "        } else {\n" +
                    "            var g;\n" +
                    "            if (typeof window !== \"undefined\") {\n" +
                    "                g = window\n" +
                    "            } else if (typeof global !== \"undefined\") {\n" +
                    "                g = global\n" +
                    "            } else if (typeof self !== \"undefined\") {\n" +
                    "                g = self\n" +
                    "            } else {\n" +
                    "                g = this\n" +
                    "            }\n" +
                    "            g.Clipboard = f()\n" +
                    "        }\n" +
                    "    })(function() {\n" +
                    "        var define, module, exports;\n" +
                    "        return (function e(t, n, r) {\n" +
                    "            function s(o, u) {\n" +
                    "                if (!n[o]) {\n" +
                    "                    if (!t[o]) {\n" +
                    "                        var a = typeof require == \"function\" && require;\n" +
                    "                        if (!u && a) return a(o, !0);\n" +
                    "                        if (i) return i(o, !0);\n" +
                    "                        var f = new Error(\"Cannot find module '\" + o + \"'\");\n" +
                    "                        throw f.code = \"MODULE_NOT_FOUND\", f\n" +
                    "                    }\n" +
                    "                    var l = n[o] = {\n" +
                    "                        exports: {}\n" +
                    "                    };\n" +
                    "                    t[o][0].call(l.exports, function(e) {\n" +
                    "                        var n = t[o][1][e];\n" +
                    "                        return s(n ? n : e)\n" +
                    "                    }, l, l.exports, e, t, n, r)\n" +
                    "                }\n" +
                    "                return n[o].exports\n" +
                    "            }\n" +
                    "            var i = typeof require == \"function\" && require;\n" +
                    "            for (var o = 0; o < r.length; o++) s(r[o]);\n" +
                    "            return s\n" +
                    "        })({\n" +
                    "            1: [function(require, module, exports) {\n" +
                    "                var matches = require('matches-selector')\n" +
                    "\n" +
                    "                module.exports = function(element, selector, checkYoSelf) {\n" +
                    "                    var parent = checkYoSelf ? element : element.parentNode\n" +
                    "\n" +
                    "                    while (parent && parent !== document) {\n" +
                    "                        if (matches(parent, selector)) return parent;\n" +
                    "                        parent = parent.parentNode\n" +
                    "                    }\n" +
                    "                }\n" +
                    "\n" +
                    "            }, {\n" +
                    "                \"matches-selector\": 5\n" +
                    "            }],\n" +
                    "            2: [function(require, module, exports) {\n" +
                    "                var closest = require('closest');\n" +
                    "\n" +
                    "                /**\n" +
                    "                 * Delegates event to a selector.\n" +
                    "                 *\n" +
                    "                 * @param {Element} element\n" +
                    "                 * @param {String} selector\n" +
                    "                 * @param {String} type\n" +
                    "                 * @param {Function} callback\n" +
                    "                 * @param {Boolean} useCapture\n" +
                    "                 * @return {Object}\n" +
                    "                 */\n" +
                    "                function delegate(element, selector, type, callback, useCapture) {\n" +
                    "                    var listenerFn = listener.apply(this, arguments);\n" +
                    "\n" +
                    "                    element.addEventListener(type, listenerFn, useCapture);\n" +
                    "\n" +
                    "                    return {\n" +
                    "                        destroy: function() {\n" +
                    "                            element.removeEventListener(type, listenerFn, useCapture);\n" +
                    "                        }\n" +
                    "                    }\n" +
                    "                }\n" +
                    "\n" +
                    "                /**\n" +
                    "                 * Finds closest match and invokes callback.\n" +
                    "                 *\n" +
                    "                 * @param {Element} element\n" +
                    "                 * @param {String} selector\n" +
                    "                 * @param {String} type\n" +
                    "                 * @param {Function} callback\n" +
                    "                 * @return {Function}\n" +
                    "                 */\n" +
                    "                function listener(element, selector, type, callback) {\n" +
                    "                    return function(e) {\n" +
                    "                        e.delegateTarget = closest(e.target, selector, true);\n" +
                    "\n" +
                    "                        if (e.delegateTarget) {\n" +
                    "                            callback.call(element, e);\n" +
                    "                        }\n" +
                    "                    }\n" +
                    "                }\n" +
                    "\n" +
                    "                module.exports = delegate;\n" +
                    "\n" +
                    "            }, {\n" +
                    "                \"closest\": 1\n" +
                    "            }],\n" +
                    "            3: [function(require, module, exports) {\n" +
                    "                /**\n" +
                    "                 * Check if argument is a HTML element.\n" +
                    "                 *\n" +
                    "                 * @param {Object} value\n" +
                    "                 * @return {Boolean}\n" +
                    "                 */\n" +
                    "                exports.node = function(value) {\n" +
                    "                    return value !== undefined && value instanceof HTMLElement && value.nodeType === 1;\n" +
                    "                };\n" +
                    "\n" +
                    "                /**\n" +
                    "                 * Check if argument is a list of HTML elements.\n" +
                    "                 *\n" +
                    "                 * @param {Object} value\n" +
                    "                 * @return {Boolean}\n" +
                    "                 */\n" +
                    "                exports.nodeList = function(value) {\n" +
                    "                    var type = Object.prototype.toString.call(value);\n" +
                    "\n" +
                    "                    return value !== undefined && (type === '[object NodeList]' || type === '[object HTMLCollection]') && ('length' in value) && (value.length === 0 || exports.node(value[0]));\n" +
                    "                };\n" +
                    "\n" +
                    "                /**\n" +
                    "                 * Check if argument is a string.\n" +
                    "                 *\n" +
                    "                 * @param {Object} value\n" +
                    "                 * @return {Boolean}\n" +
                    "                 */\n" +
                    "                exports.string = function(value) {\n" +
                    "                    return typeof value === 'string' || value instanceof String;\n" +
                    "                };\n" +
                    "\n" +
                    "                /**\n" +
                    "                 * Check if argument is a function.\n" +
                    "                 *\n" +
                    "                 * @param {Object} value\n" +
                    "                 * @return {Boolean}\n" +
                    "                 */\n" +
                    "                exports.fn = function(value) {\n" +
                    "                    var type = Object.prototype.toString.call(value);\n" +
                    "\n" +
                    "                    return type === '[object Function]';\n" +
                    "                };\n" +
                    "\n" +
                    "            }, {}],\n" +
                    "            4: [function(require, module, exports) {\n" +
                    "                var is = require('./is');\n" +
                    "                var delegate = require('delegate');\n" +
                    "\n" +
                    "                /**\n" +
                    "                 * Validates all params and calls the right\n" +
                    "                 * listener function based on its target type.\n" +
                    "                 *\n" +
                    "                 * @param {String|HTMLElement|HTMLCollection|NodeList} target\n" +
                    "                 * @param {String} type\n" +
                    "                 * @param {Function} callback\n" +
                    "                 * @return {Object}\n" +
                    "                 */\n" +
                    "                function listen(target, type, callback) {\n" +
                    "                    if (!target && !type && !callback) {\n" +
                    "                        throw new Error('Missing required arguments');\n" +
                    "                    }\n" +
                    "\n" +
                    "                    if (!is.string(type)) {\n" +
                    "                        throw new TypeError('Second argument must be a String');\n" +
                    "                    }\n" +
                    "\n" +
                    "                    if (!is.fn(callback)) {\n" +
                    "                        throw new TypeError('Third argument must be a Function');\n" +
                    "                    }\n" +
                    "\n" +
                    "                    if (is.node(target)) {\n" +
                    "                        return listenNode(target, type, callback);\n" +
                    "                    } else if (is.nodeList(target)) {\n" +
                    "                        return listenNodeList(target, type, callback);\n" +
                    "                    } else if (is.string(target)) {\n" +
                    "                        return listenSelector(target, type, callback);\n" +
                    "                    } else {\n" +
                    "                        throw new TypeError('First argument must be a String, HTMLElement, HTMLCollection, or NodeList');\n" +
                    "                    }\n" +
                    "                }\n" +
                    "\n" +
                    "                /**\n" +
                    "                 * Adds an event listener to a HTML element\n" +
                    "                 * and returns a remove listener function.\n" +
                    "                 *\n" +
                    "                 * @param {HTMLElement} node\n" +
                    "                 * @param {String} type\n" +
                    "                 * @param {Function} callback\n" +
                    "                 * @return {Object}\n" +
                    "                 */\n" +
                    "                function listenNode(node, type, callback) {\n" +
                    "                    node.addEventListener(type, callback);\n" +
                    "\n" +
                    "                    return {\n" +
                    "                        destroy: function() {\n" +
                    "                            node.removeEventListener(type, callback);\n" +
                    "                        }\n" +
                    "                    }\n" +
                    "                }\n" +
                    "\n" +
                    "                /**\n" +
                    "                 * Add an event listener to a list of HTML elements\n" +
                    "                 * and returns a remove listener function.\n" +
                    "                 *\n" +
                    "                 * @param {NodeList|HTMLCollection} nodeList\n" +
                    "                 * @param {String} type\n" +
                    "                 * @param {Function} callback\n" +
                    "                 * @return {Object}\n" +
                    "                 */\n" +
                    "                function listenNodeList(nodeList, type, callback) {\n" +
                    "                    Array.prototype.forEach.call(nodeList, function(node) {\n" +
                    "                        node.addEventListener(type, callback);\n" +
                    "                    });\n" +
                    "\n" +
                    "                    return {\n" +
                    "                        destroy: function() {\n" +
                    "                            Array.prototype.forEach.call(nodeList, function(node) {\n" +
                    "                                node.removeEventListener(type, callback);\n" +
                    "                            });\n" +
                    "                        }\n" +
                    "                    }\n" +
                    "                }\n" +
                    "\n" +
                    "                /**\n" +
                    "                 * Add an event listener to a selector\n" +
                    "                 * and returns a remove listener function.\n" +
                    "                 *\n" +
                    "                 * @param {String} selector\n" +
                    "                 * @param {String} type\n" +
                    "                 * @param {Function} callback\n" +
                    "                 * @return {Object}\n" +
                    "                 */\n" +
                    "                function listenSelector(selector, type, callback) {\n" +
                    "                    return delegate(document.body, selector, type, callback);\n" +
                    "                }\n" +
                    "\n" +
                    "                module.exports = listen;\n" +
                    "\n" +
                    "            }, {\n" +
                    "                \"./is\": 3,\n" +
                    "                \"delegate\": 2\n" +
                    "            }],\n" +
                    "            5: [function(require, module, exports) {\n" +
                    "\n" +
                    "                /**\n" +
                    "                 * Element prototype.\n" +
                    "                 */\n" +
                    "\n" +
                    "                var proto = Element.prototype;\n" +
                    "\n" +
                    "                /**\n" +
                    "                 * Vendor function.\n" +
                    "                 */\n" +
                    "\n" +
                    "                var vendor = proto.matchesSelector || proto.webkitMatchesSelector || proto.mozMatchesSelector || proto.msMatchesSelector || proto.oMatchesSelector;\n" +
                    "\n" +
                    "                /**\n" +
                    "                 * Expose `match()`.\n" +
                    "                 */\n" +
                    "\n" +
                    "                module.exports = match;\n" +
                    "\n" +
                    "                /**\n" +
                    "                 * Match `el` to `selector`.\n" +
                    "                 *\n" +
                    "                 * @param {Element} el\n" +
                    "                 * @param {String} selector\n" +
                    "                 * @return {Boolean}\n" +
                    "                 * @api public\n" +
                    "                 */\n" +
                    "\n" +
                    "                function match(el, selector) {\n" +
                    "                    if (vendor) return vendor.call(el, selector);\n" +
                    "                    var nodes = el.parentNode.querySelectorAll(selector);\n" +
                    "                    for (var i = 0; i < nodes.length; ++i) {\n" +
                    "                        if (nodes[i] == el) return true;\n" +
                    "                    }\n" +
                    "                    return false;\n" +
                    "                }\n" +
                    "            }, {}],\n" +
                    "            6: [function(require, module, exports) {\n" +
                    "                function select(element) {\n" +
                    "                    var selectedText;\n" +
                    "\n" +
                    "                    if (element.nodeName === 'INPUT' || element.nodeName === 'TEXTAREA') {\n" +
                    "                        element.focus();\n" +
                    "                        element.setSelectionRange(0, element.value.length);\n" +
                    "\n" +
                    "                        selectedText = element.value;\n" +
                    "                    } else {\n" +
                    "                        if (element.hasAttribute('contenteditable')) {\n" +
                    "                            element.focus();\n" +
                    "                        }\n" +
                    "\n" +
                    "                        var selection = window.getSelection();\n" +
                    "                        var range = document.createRange();\n" +
                    "\n" +
                    "                        range.selectNodeContents(element);\n" +
                    "                        selection.removeAllRanges();\n" +
                    "                        selection.addRange(range);\n" +
                    "\n" +
                    "                        selectedText = selection.toString();\n" +
                    "                    }\n" +
                    "\n" +
                    "                    return selectedText;\n" +
                    "                }\n" +
                    "\n" +
                    "                module.exports = select;\n" +
                    "\n" +
                    "            }, {}],\n" +
                    "            7: [function(require, module, exports) {\n" +
                    "                function E() {\n" +
                    "                    // Keep this empty so it's easier to inherit from\n" +
                    "                    // (via https://github.com/lipsmack from https://github.com/scottcorgan/tiny-emitter/issues/3)\n" +
                    "                }\n" +
                    "\n" +
                    "                E.prototype = {\n" +
                    "                    on: function(name, callback, ctx) {\n" +
                    "                        var e = this.e || (this.e = {});\n" +
                    "\n" +
                    "                        (e[name] || (e[name] = [])).push({\n" +
                    "                            fn: callback,\n" +
                    "                            ctx: ctx\n" +
                    "                        });\n" +
                    "\n" +
                    "                        return this;\n" +
                    "                    },\n" +
                    "\n" +
                    "                    once: function(name, callback, ctx) {\n" +
                    "                        var self = this;\n" +
                    "\n" +
                    "                        function listener() {\n" +
                    "                            self.off(name, listener);\n" +
                    "                            callback.apply(ctx, arguments);\n" +
                    "                        };\n" +
                    "\n" +
                    "                        listener._ = callback\n" +
                    "                        return this.on(name, listener, ctx);\n" +
                    "                    },\n" +
                    "\n" +
                    "                    emit: function(name) {\n" +
                    "                        var data = [].slice.call(arguments, 1);\n" +
                    "                        var evtArr = ((this.e || (this.e = {}))[name] || []).slice();\n" +
                    "                        var i = 0;\n" +
                    "                        var len = evtArr.length;\n" +
                    "\n" +
                    "                        for (i; i < len; i++) {\n" +
                    "                            evtArr[i].fn.apply(evtArr[i].ctx, data);\n" +
                    "                        }\n" +
                    "\n" +
                    "                        return this;\n" +
                    "                    },\n" +
                    "\n" +
                    "                    off: function(name, callback) {\n" +
                    "                        var e = this.e || (this.e = {});\n" +
                    "                        var evts = e[name];\n" +
                    "                        var liveEvents = [];\n" +
                    "\n" +
                    "                        if (evts && callback) {\n" +
                    "                            for (var i = 0, len = evts.length; i < len; i++) {\n" +
                    "                                if (evts[i].fn !== callback && evts[i].fn._ !== callback)\n" +
                    "                                    liveEvents.push(evts[i]);\n" +
                    "                            }\n" +
                    "                        }\n" +
                    "\n" +
                    "                        // Remove event from queue to prevent memory leak\n" +
                    "                        // Suggested by https://github.com/lazd\n" +
                    "                        // Ref: https://github.com/scottcorgan/tiny-emitter/commit/c6ebfaa9bc973b33d110a84a307742b7cf94c953#commitcomment-5024910\n" +
                    "\n" +
                    "                        (liveEvents.length) ? e[name] = liveEvents: delete e[name];\n" +
                    "\n" +
                    "                        return this;\n" +
                    "                    }\n" +
                    "                };\n" +
                    "\n" +
                    "                module.exports = E;\n" +
                    "\n" +
                    "            }, {}],\n" +
                    "            8: [function(require, module, exports) {\n" +
                    "                (function(global, factory) {\n" +
                    "                    if (typeof define === \"function\" && define.amd) {\n" +
                    "                        define(['module', 'select'], factory);\n" +
                    "                    } else if (typeof exports !== \"undefined\") {\n" +
                    "                        factory(module, require('select'));\n" +
                    "                    } else {\n" +
                    "                        var mod = {\n" +
                    "                            exports: {}\n" +
                    "                        };\n" +
                    "                        factory(mod, global.select);\n" +
                    "                        global.clipboardAction = mod.exports;\n" +
                    "                    }\n" +
                    "                })(this, function(module, _select) {\n" +
                    "                    'use strict';\n" +
                    "\n" +
                    "                    var _select2 = _interopRequireDefault(_select);\n" +
                    "\n" +
                    "                    function _interopRequireDefault(obj) {\n" +
                    "                        return obj && obj.__esModule ? obj : {\n" +
                    "                            default: obj\n" +
                    "                        };\n" +
                    "                    }\n" +
                    "\n" +
                    "                    var _typeof = typeof Symbol === \"function\" && typeof Symbol.iterator === \"symbol\" ? function(obj) {\n" +
                    "                        return typeof obj;\n" +
                    "                    } : function(obj) {\n" +
                    "                        return obj && typeof Symbol === \"function\" && obj.constructor === Symbol ? \"symbol\" : typeof obj;\n" +
                    "                    };\n" +
                    "\n" +
                    "                    function _classCallCheck(instance, Constructor) {\n" +
                    "                        if (!(instance instanceof Constructor)) {\n" +
                    "                            throw new TypeError(\"Cannot call a class as a function\");\n" +
                    "                        }\n" +
                    "                    }\n" +
                    "\n" +
                    "                    var _createClass = function() {\n" +
                    "                        function defineProperties(target, props) {\n" +
                    "                            for (var i = 0; i < props.length; i++) {\n" +
                    "                                var descriptor = props[i];\n" +
                    "                                descriptor.enumerable = descriptor.enumerable || false;\n" +
                    "                                descriptor.configurable = true;\n" +
                    "                                if (\"value\" in descriptor) descriptor.writable = true;\n" +
                    "                                Object.defineProperty(target, descriptor.key, descriptor);\n" +
                    "                            }\n" +
                    "                        }\n" +
                    "\n" +
                    "                        return function(Constructor, protoProps, staticProps) {\n" +
                    "                            if (protoProps) defineProperties(Constructor.prototype, protoProps);\n" +
                    "                            if (staticProps) defineProperties(Constructor, staticProps);\n" +
                    "                            return Constructor;\n" +
                    "                        };\n" +
                    "                    }();\n" +
                    "\n" +
                    "                    var ClipboardAction = function() {\n" +
                    "                        /**\n" +
                    "                         * @param {Object} options\n" +
                    "                         */\n" +
                    "\n" +
                    "                        function ClipboardAction(options) {\n" +
                    "                            _classCallCheck(this, ClipboardAction);\n" +
                    "\n" +
                    "                            this.resolveOptions(options);\n" +
                    "                            this.initSelection();\n" +
                    "                        }\n" +
                    "\n" +
                    "                        /**\n" +
                    "                         * Defines base properties passed from constructor.\n" +
                    "                         * @param {Object} options\n" +
                    "                         */\n" +
                    "\n" +
                    "\n" +
                    "                        ClipboardAction.prototype.resolveOptions = function resolveOptions() {\n" +
                    "                            var options = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];\n" +
                    "\n" +
                    "                            this.action = options.action;\n" +
                    "                            this.emitter = options.emitter;\n" +
                    "                            this.target = options.target;\n" +
                    "                            this.text = options.text;\n" +
                    "                            this.trigger = options.trigger;\n" +
                    "\n" +
                    "                            this.selectedText = '';\n" +
                    "                        };\n" +
                    "\n" +
                    "                        ClipboardAction.prototype.initSelection = function initSelection() {\n" +
                    "                            if (this.text) {\n" +
                    "                                this.selectFake();\n" +
                    "                            } else if (this.target) {\n" +
                    "                                this.selectTarget();\n" +
                    "                            }\n" +
                    "                        };\n" +
                    "\n" +
                    "                        ClipboardAction.prototype.selectFake = function selectFake() {\n" +
                    "                            var _this = this;\n" +
                    "\n" +
                    "                            var isRTL = document.documentElement.getAttribute('dir') == 'rtl';\n" +
                    "\n" +
                    "                            this.removeFake();\n" +
                    "\n" +
                    "                            this.fakeHandlerCallback = function() {\n" +
                    "                                return _this.removeFake();\n" +
                    "                            };\n" +
                    "                            this.fakeHandler = document.body.addEventListener('click', this.fakeHandlerCallback) || true;\n" +
                    "\n" +
                    "                            this.fakeElem = document.createElement('textarea');\n" +
                    "                            // Prevent zooming on iOS\n" +
                    "                            this.fakeElem.style.fontSize = '12pt';\n" +
                    "                            // Reset box model\n" +
                    "                            this.fakeElem.style.border = '0';\n" +
                    "                            this.fakeElem.style.padding = '0';\n" +
                    "                            this.fakeElem.style.margin = '0';\n" +
                    "                            // Move element out of screen horizontally\n" +
                    "                            this.fakeElem.style.position = 'absolute';\n" +
                    "                            this.fakeElem.style[isRTL ? 'right' : 'left'] = '-9999px';\n" +
                    "                            // Move element to the same position vertically\n" +
                    "                            this.fakeElem.style.top = (window.pageYOffset || document.documentElement.scrollTop) + 'px';\n" +
                    "                            this.fakeElem.setAttribute('readonly', '');\n" +
                    "                            this.fakeElem.value = this.text;\n" +
                    "\n" +
                    "                            document.body.appendChild(this.fakeElem);\n" +
                    "\n" +
                    "                            this.selectedText = (0, _select2.default)(this.fakeElem);\n" +
                    "                            this.copyText();\n" +
                    "                        };\n" +
                    "\n" +
                    "                        ClipboardAction.prototype.removeFake = function removeFake() {\n" +
                    "                            if (this.fakeHandler) {\n" +
                    "                                document.body.removeEventListener('click', this.fakeHandlerCallback);\n" +
                    "                                this.fakeHandler = null;\n" +
                    "                                this.fakeHandlerCallback = null;\n" +
                    "                            }\n" +
                    "\n" +
                    "                            if (this.fakeElem) {\n" +
                    "                                document.body.removeChild(this.fakeElem);\n" +
                    "                                this.fakeElem = null;\n" +
                    "                            }\n" +
                    "                        };\n" +
                    "\n" +
                    "                        ClipboardAction.prototype.selectTarget = function selectTarget() {\n" +
                    "                            this.selectedText = (0, _select2.default)(this.target);\n" +
                    "                            this.copyText();\n" +
                    "                        };\n" +
                    "\n" +
                    "                        ClipboardAction.prototype.copyText = function copyText() {\n" +
                    "                            var succeeded = undefined;\n" +
                    "\n" +
                    "                            try {\n" +
                    "                                succeeded = document.execCommand(this.action);\n" +
                    "                            } catch (err) {\n" +
                    "                                succeeded = false;\n" +
                    "                            }\n" +
                    "\n" +
                    "                            this.handleResult(succeeded);\n" +
                    "                        };\n" +
                    "\n" +
                    "                        ClipboardAction.prototype.handleResult = function handleResult(succeeded) {\n" +
                    "                            if (succeeded) {\n" +
                    "                                this.emitter.emit('success', {\n" +
                    "                                    action: this.action,\n" +
                    "                                    text: this.selectedText,\n" +
                    "                                    trigger: this.trigger,\n" +
                    "                                    clearSelection: this.clearSelection.bind(this)\n" +
                    "                                });\n" +
                    "                            } else {\n" +
                    "                                this.emitter.emit('error', {\n" +
                    "                                    action: this.action,\n" +
                    "                                    trigger: this.trigger,\n" +
                    "                                    clearSelection: this.clearSelection.bind(this)\n" +
                    "                                });\n" +
                    "                            }\n" +
                    "                        };\n" +
                    "\n" +
                    "                        ClipboardAction.prototype.clearSelection = function clearSelection() {\n" +
                    "                            if (this.target) {\n" +
                    "                                this.target.blur();\n" +
                    "                            }\n" +
                    "\n" +
                    "                            window.getSelection().removeAllRanges();\n" +
                    "                        };\n" +
                    "\n" +
                    "                        ClipboardAction.prototype.destroy = function destroy() {\n" +
                    "                            this.removeFake();\n" +
                    "                        };\n" +
                    "\n" +
                    "                        _createClass(ClipboardAction, [{\n" +
                    "                            key: 'action',\n" +
                    "                            set: function set() {\n" +
                    "                                var action = arguments.length <= 0 || arguments[0] === undefined ? 'copy' : arguments[0];\n" +
                    "\n" +
                    "                                this._action = action;\n" +
                    "\n" +
                    "                                if (this._action !== 'copy' && this._action !== 'cut') {\n" +
                    "                                    throw new Error('Invalid \"action\" value, use either \"copy\" or \"cut\"');\n" +
                    "                                }\n" +
                    "                            },\n" +
                    "                            get: function get() {\n" +
                    "                                return this._action;\n" +
                    "                            }\n" +
                    "                        }, {\n" +
                    "                            key: 'target',\n" +
                    "                            set: function set(target) {\n" +
                    "                                if (target !== undefined) {\n" +
                    "                                    if (target && (typeof target === 'undefined' ? 'undefined' : _typeof(target)) === 'object' && target.nodeType === 1) {\n" +
                    "                                        if (this.action === 'copy' && target.hasAttribute('disabled')) {\n" +
                    "                                            throw new Error('Invalid \"target\" attribute. Please use \"readonly\" instead of \"disabled\" attribute');\n" +
                    "                                        }\n" +
                    "\n" +
                    "                                        if (this.action === 'cut' && (target.hasAttribute('readonly') || target.hasAttribute('disabled'))) {\n" +
                    "                                            throw new Error('Invalid \"target\" attribute. You can\\'t cut text from elements with \"readonly\" or \"disabled\" attributes');\n" +
                    "                                        }\n" +
                    "\n" +
                    "                                        this._target = target;\n" +
                    "                                    } else {\n" +
                    "                                        throw new Error('Invalid \"target\" value, use a valid Element');\n" +
                    "                                    }\n" +
                    "                                }\n" +
                    "                            },\n" +
                    "                            get: function get() {\n" +
                    "                                return this._target;\n" +
                    "                            }\n" +
                    "                        }]);\n" +
                    "\n" +
                    "                        return ClipboardAction;\n" +
                    "                    }();\n" +
                    "\n" +
                    "                    module.exports = ClipboardAction;\n" +
                    "                });\n" +
                    "\n" +
                    "            }, {\n" +
                    "                \"select\": 6\n" +
                    "            }],\n" +
                    "            9: [function(require, module, exports) {\n" +
                    "                (function(global, factory) {\n" +
                    "                    if (typeof define === \"function\" && define.amd) {\n" +
                    "                        define(['module', './clipboard-action', 'tiny-emitter', 'good-listener'], factory);\n" +
                    "                    } else if (typeof exports !== \"undefined\") {\n" +
                    "                        factory(module, require('./clipboard-action'), require('tiny-emitter'), require('good-listener'));\n" +
                    "                    } else {\n" +
                    "                        var mod = {\n" +
                    "                            exports: {}\n" +
                    "                        };\n" +
                    "                        factory(mod, global.clipboardAction, global.tinyEmitter, global.goodListener);\n" +
                    "                        global.clipboard = mod.exports;\n" +
                    "                    }\n" +
                    "                })(this, function(module, _clipboardAction, _tinyEmitter, _goodListener) {\n" +
                    "                    'use strict';\n" +
                    "\n" +
                    "                    var _clipboardAction2 = _interopRequireDefault(_clipboardAction);\n" +
                    "\n" +
                    "                    var _tinyEmitter2 = _interopRequireDefault(_tinyEmitter);\n" +
                    "\n" +
                    "                    var _goodListener2 = _interopRequireDefault(_goodListener);\n" +
                    "\n" +
                    "                    function _interopRequireDefault(obj) {\n" +
                    "                        return obj && obj.__esModule ? obj : {\n" +
                    "                            default: obj\n" +
                    "                        };\n" +
                    "                    }\n" +
                    "\n" +
                    "                    function _classCallCheck(instance, Constructor) {\n" +
                    "                        if (!(instance instanceof Constructor)) {\n" +
                    "                            throw new TypeError(\"Cannot call a class as a function\");\n" +
                    "                        }\n" +
                    "                    }\n" +
                    "\n" +
                    "                    function _possibleConstructorReturn(self, call) {\n" +
                    "                        if (!self) {\n" +
                    "                            throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\");\n" +
                    "                        }\n" +
                    "\n" +
                    "                        return call && (typeof call === \"object\" || typeof call === \"function\") ? call : self;\n" +
                    "                    }\n" +
                    "\n" +
                    "                    function _inherits(subClass, superClass) {\n" +
                    "                        if (typeof superClass !== \"function\" && superClass !== null) {\n" +
                    "                            throw new TypeError(\"Super expression must either be null or a function, not \" + typeof superClass);\n" +
                    "                        }\n" +
                    "\n" +
                    "                        subClass.prototype = Object.create(superClass && superClass.prototype, {\n" +
                    "                            constructor: {\n" +
                    "                                value: subClass,\n" +
                    "                                enumerable: false,\n" +
                    "                                writable: true,\n" +
                    "                                configurable: true\n" +
                    "                            }\n" +
                    "                        });\n" +
                    "                        if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;\n" +
                    "                    }\n" +
                    "\n" +
                    "                    var Clipboard = function(_Emitter) {\n" +
                    "                        _inherits(Clipboard, _Emitter);\n" +
                    "\n" +
                    "                        /**\n" +
                    "                         * @param {String|HTMLElement|HTMLCollection|NodeList} trigger\n" +
                    "                         * @param {Object} options\n" +
                    "                         */\n" +
                    "\n" +
                    "                        function Clipboard(trigger, options) {\n" +
                    "                            _classCallCheck(this, Clipboard);\n" +
                    "\n" +
                    "                            var _this = _possibleConstructorReturn(this, _Emitter.call(this));\n" +
                    "\n" +
                    "                            _this.resolveOptions(options);\n" +
                    "                            _this.listenClick(trigger);\n" +
                    "                            return _this;\n" +
                    "                        }\n" +
                    "\n" +
                    "                        /**\n" +
                    "                         * Defines if attributes would be resolved using internal setter functions\n" +
                    "                         * or custom functions that were passed in the constructor.\n" +
                    "                         * @param {Object} options\n" +
                    "                         */\n" +
                    "\n" +
                    "\n" +
                    "                        Clipboard.prototype.resolveOptions = function resolveOptions() {\n" +
                    "                            var options = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];\n" +
                    "\n" +
                    "                            this.action = typeof options.action === 'function' ? options.action : this.defaultAction;\n" +
                    "                            this.target = typeof options.target === 'function' ? options.target : this.defaultTarget;\n" +
                    "                            this.text = typeof options.text === 'function' ? options.text : this.defaultText;\n" +
                    "                        };\n" +
                    "\n" +
                    "                        Clipboard.prototype.listenClick = function listenClick(trigger) {\n" +
                    "                            var _this2 = this;\n" +
                    "\n" +
                    "                            this.listener = (0, _goodListener2.default)(trigger, 'click', function(e) {\n" +
                    "                                return _this2.onClick(e);\n" +
                    "                            });\n" +
                    "                        };\n" +
                    "\n" +
                    "                        Clipboard.prototype.onClick = function onClick(e) {\n" +
                    "                            var trigger = e.delegateTarget || e.currentTarget;\n" +
                    "\n" +
                    "                            if (this.clipboardAction) {\n" +
                    "                                this.clipboardAction = null;\n" +
                    "                            }\n" +
                    "\n" +
                    "                            this.clipboardAction = new _clipboardAction2.default({\n" +
                    "                                action: this.action(trigger),\n" +
                    "                                target: this.target(trigger),\n" +
                    "                                text: this.text(trigger),\n" +
                    "                                trigger: trigger,\n" +
                    "                                emitter: this\n" +
                    "                            });\n" +
                    "                        };\n" +
                    "\n" +
                    "                        Clipboard.prototype.defaultAction = function defaultAction(trigger) {\n" +
                    "                            return getAttributeValue('action', trigger);\n" +
                    "                        };\n" +
                    "\n" +
                    "                        Clipboard.prototype.defaultTarget = function defaultTarget(trigger) {\n" +
                    "                            var selector = getAttributeValue('target', trigger);\n" +
                    "\n" +
                    "                            if (selector) {\n" +
                    "                                return document.querySelector(selector);\n" +
                    "                            }\n" +
                    "                        };\n" +
                    "\n" +
                    "                        Clipboard.prototype.defaultText = function defaultText(trigger) {\n" +
                    "                            return getAttributeValue('text', trigger);\n" +
                    "                        };\n" +
                    "\n" +
                    "                        Clipboard.prototype.destroy = function destroy() {\n" +
                    "                            this.listener.destroy();\n" +
                    "\n" +
                    "                            if (this.clipboardAction) {\n" +
                    "                                this.clipboardAction.destroy();\n" +
                    "                                this.clipboardAction = null;\n" +
                    "                            }\n" +
                    "                        };\n" +
                    "\n" +
                    "                        return Clipboard;\n" +
                    "                    }(_tinyEmitter2.default);\n" +
                    "\n" +
                    "                    /**\n" +
                    "                     * Helper function to retrieve attribute value.\n" +
                    "                     * @param {String} suffix\n" +
                    "                     * @param {Element} element\n" +
                    "                     */\n" +
                    "                    function getAttributeValue(suffix, element) {\n" +
                    "                        var attribute = 'data-clipboard-' + suffix;\n" +
                    "\n" +
                    "                        if (!element.hasAttribute(attribute)) {\n" +
                    "                            return;\n" +
                    "                        }\n" +
                    "\n" +
                    "                        return element.getAttribute(attribute);\n" +
                    "                    }";
            String msg5="                    module.exports = Clipboard;\n" +
                    "                });\n" +
                    "\n" +
                    "            }, {\n" +
                    "                \"./clipboard-action\": 8,\n" +
                    "                \"good-listener\": 4,\n" +
                    "                \"tiny-emitter\": 7\n" +
                    "            }]\n" +
                    "        }, {}, [9])(9)\n" +
                    "    });\n" +
                    "    /*!\n" +
                    "     * clipboard.js v1.5.12\n" +
                    "     * https://zenorocha.github.io/clipboard.js\n" +
                    "     *\n" +
                    "     * Licensed MIT © Zeno Rocha\n" +
                    "     */\n" +
                    "    ! function(t) {\n" +
                    "        if (\"object\" == typeof exports && \"undefined\" != typeof module) module.exports = t();\n" +
                    "        else if (\"function\" == typeof define && define.amd) define([], t);\n" +
                    "        else {\n" +
                    "            var e;\n" +
                    "            e = \"undefined\" != typeof window ? window : \"undefined\" != typeof global ? global : \"undefined\" != typeof self ? self : this, e.Clipboard = t()\n" +
                    "        }\n" +
                    "    }(function() {\n" +
                    "        var t, e, n;\n" +
                    "        return function t(e, n, o) {\n" +
                    "            function i(a, c) {\n" +
                    "                if (!n[a]) {\n" +
                    "                    if (!e[a]) {\n" +
                    "                        var s = \"function\" == typeof require && require;\n" +
                    "                        if (!c && s) return s(a, !0);\n" +
                    "                        if (r) return r(a, !0);\n" +
                    "                        var l = new Error(\"Cannot find module '\" + a + \"'\");\n" +
                    "                        throw l.code = \"MODULE_NOT_FOUND\", l\n" +
                    "                    }\n" +
                    "                    var u = n[a] = {\n" +
                    "                        exports: {}\n" +
                    "                    };\n" +
                    "                    e[a][0].call(u.exports, function(t) {\n" +
                    "                        var n = e[a][1][t];\n" +
                    "                        return i(n ? n : t)\n" +
                    "                    }, u, u.exports, t, e, n, o)\n" +
                    "                }\n" +
                    "                return n[a].exports\n" +
                    "            }\n" +
                    "            for (var r = \"function\" == typeof require && require, a = 0; a < o.length; a++) i(o[a]);\n" +
                    "            return i\n" +
                    "        }({\n" +
                    "            1: [function(t, e, n) {\n" +
                    "                var o = t(\"matches-selector\");\n" +
                    "                e.exports = function(t, e, n) {\n" +
                    "                    for (var i = n ? t : t.parentNode; i && i !== document;) {\n" +
                    "                        if (o(i, e)) return i;\n" +
                    "                        i = i.parentNode\n" +
                    "                    }\n" +
                    "                }\n" +
                    "            }, {\n" +
                    "                \"matches-selector\": 5\n" +
                    "            }],\n" +
                    "            2: [function(t, e, n) {\n" +
                    "                function o(t, e, n, o, r) {\n" +
                    "                    var a = i.apply(this, arguments);\n" +
                    "                    return t.addEventListener(n, a, r), {\n" +
                    "                        destroy: function() {\n" +
                    "                            t.removeEventListener(n, a, r)\n" +
                    "                        }\n" +
                    "                    }\n" +
                    "                }\n" +
                    "\n" +
                    "                function i(t, e, n, o) {\n" +
                    "                    return function(n) {\n" +
                    "                        n.delegateTarget = r(n.target, e, !0), n.delegateTarget && o.call(t, n)\n" +
                    "                    }\n" +
                    "                }\n" +
                    "                var r = t(\"closest\");\n" +
                    "                e.exports = o\n" +
                    "            }, {\n" +
                    "                closest: 1\n" +
                    "            }],\n" +
                    "            3: [function(t, e, n) {\n" +
                    "                n.node = function(t) {\n" +
                    "                    return void 0 !== t && t instanceof HTMLElement && 1 === t.nodeType\n" +
                    "                }, n.nodeList = function(t) {\n" +
                    "                    var e = Object.prototype.toString.call(t);\n" +
                    "                    return void 0 !== t && (\"[object NodeList]\" === e || \"[object HTMLCollection]\" === e) && \"length\" in t && (0 === t.length || n.node(t[0]))\n" +
                    "                }, n.string = function(t) {\n" +
                    "                    return \"string\" == typeof t || t instanceof String\n" +
                    "                }, n.fn = function(t) {\n" +
                    "                    var e = Object.prototype.toString.call(t);\n" +
                    "                    return \"[object Function]\" === e\n" +
                    "                }\n" +
                    "            }, {}],\n" +
                    "            4: [function(t, e, n) {\n" +
                    "                function o(t, e, n) {\n" +
                    "                    if (!t && !e && !n) throw new Error(\"Missing required arguments\");\n" +
                    "                    if (!c.string(e)) throw new TypeError(\"Second argument must be a String\");\n" +
                    "                    if (!c.fn(n)) throw new TypeError(\"Third argument must be a Function\");\n" +
                    "                    if (c.node(t)) return i(t, e, n);\n" +
                    "                    if (c.nodeList(t)) return r(t, e, n);\n" +
                    "                    if (c.string(t)) return a(t, e, n);\n" +
                    "                    throw new TypeError(\"First argument must be a String, HTMLElement, HTMLCollection, or NodeList\")\n" +
                    "                }\n" +
                    "\n" +
                    "                function i(t, e, n) {\n" +
                    "                    return t.addEventListener(e, n), {\n" +
                    "                        destroy: function() {\n" +
                    "                            t.removeEventListener(e, n)\n" +
                    "                        }\n" +
                    "                    }\n" +
                    "                }\n" +
                    "\n" +
                    "                function r(t, e, n) {\n" +
                    "                    return Array.prototype.forEach.call(t, function(t) {\n" +
                    "                        t.addEventListener(e, n)\n" +
                    "                    }), {\n" +
                    "                        destroy: function() {\n" +
                    "                            Array.prototype.forEach.call(t, function(t) {\n" +
                    "                                t.removeEventListener(e, n)\n" +
                    "                            })\n" +
                    "                        }\n" +
                    "                    }\n" +
                    "                }\n" +
                    "\n" +
                    "                function a(t, e, n) {\n" +
                    "                    return s(document.body, t, e, n)\n" +
                    "                }\n" +
                    "                var c = t(\"./is\"),\n" +
                    "                    s = t(\"delegate\");\n" +
                    "                e.exports = o\n" +
                    "            }, {\n" +
                    "                \"./is\": 3,\n" +
                    "                delegate: 2\n" +
                    "            }],\n" +
                    "            5: [function(t, e, n) {\n" +
                    "                function o(t, e) {\n" +
                    "                    if (r) return r.call(t, e);\n" +
                    "                    for (var n = t.parentNode.querySelectorAll(e), o = 0; o < n.length; ++o)\n" +
                    "                        if (n[o] == t) return !0;\n" +
                    "                    return !1\n" +
                    "                }\n" +
                    "                var i = Element.prototype,\n" +
                    "                    r = i.matchesSelector || i.webkitMatchesSelector || i.mozMatchesSelector || i.msMatchesSelector || i.oMatchesSelector;\n" +
                    "                e.exports = o\n" +
                    "            }, {}],\n" +
                    "            6: [function(t, e, n) {\n" +
                    "                function o(t) {\n" +
                    "                    var e;\n" +
                    "                    if (\"INPUT\" === t.nodeName || \"TEXTAREA\" === t.nodeName) t.focus(), t.setSelectionRange(0, t.value.length), e = t.value;\n" +
                    "                    else {\n" +
                    "                        t.hasAttribute(\"contenteditable\") && t.focus();\n" +
                    "                        var n = window.getSelection(),\n" +
                    "                            o = document.createRange();\n" +
                    "                        o.selectNodeContents(t), n.removeAllRanges(), n.addRange(o), e = n.toString()\n" +
                    "                    }\n" +
                    "                    return e\n" +
                    "                }\n" +
                    "                e.exports = o\n" +
                    "            }, {}],\n" +
                    "            7: [function(t, e, n) {\n" +
                    "                function o() {}\n" +
                    "                o.prototype = {\n" +
                    "                    on: function(t, e, n) {\n" +
                    "                        var o = this.e || (this.e = {});\n" +
                    "                        return (o[t] || (o[t] = [])).push({\n" +
                    "                            fn: e,\n" +
                    "                            ctx: n\n" +
                    "                        }), this\n" +
                    "                    },\n" +
                    "                    once: function(t, e, n) {\n" +
                    "                        function o() {\n" +
                    "                            i.off(t, o), e.apply(n, arguments)\n" +
                    "                        }\n" +
                    "                        var i = this;\n" +
                    "                        return o._ = e, this.on(t, o, n)\n" +
                    "                    },\n" +
                    "                    emit: function(t) {\n" +
                    "                        var e = [].slice.call(arguments, 1),\n" +
                    "                            n = ((this.e || (this.e = {}))[t] || []).slice(),\n" +
                    "                            o = 0,\n" +
                    "                            i = n.length;\n" +
                    "                        for (o; i > o; o++) n[o].fn.apply(n[o].ctx, e);\n" +
                    "                        return this\n" +
                    "                    },\n" +
                    "                    off: function(t, e) {\n" +
                    "                        var n = this.e || (this.e = {}),\n" +
                    "                            o = n[t],\n" +
                    "                            i = [];\n" +
                    "                        if (o && e)\n" +
                    "                            for (var r = 0, a = o.length; a > r; r++) o[r].fn !== e && o[r].fn._ !== e && i.push(o[r]);\n" +
                    "                        return i.length ? n[t] = i : delete n[t], this\n" +
                    "                    }\n" +
                    "                }, e.exports = o\n" +
                    "            }, {}],\n" +
                    "            8: [function(e, n, o) {\n" +
                    "                ! function(i, r) {\n" +
                    "                    if (\"function\" == typeof t && t.amd) t([\"module\", \"select\"], r);\n" +
                    "                    else if (\"undefined\" != typeof o) r(n, e(\"select\"));\n" +
                    "                    else {\n" +
                    "                        var a = {\n" +
                    "                            exports: {}\n" +
                    "                        };\n" +
                    "                        r(a, i.select), i.clipboardAction = a.exports\n" +
                    "                    }\n" +
                    "                }(this, function(t, e) {\n" +
                    "                    \"use strict\";\n" +
                    "\n" +
                    "                    function n(t) {\n" +
                    "                        return t && t.__esModule ? t : {\n" +
                    "                            \"default\": t\n" +
                    "                        }\n" +
                    "                    }\n" +
                    "\n" +
                    "                    function o(t, e) {\n" +
                    "                        if (!(t instanceof e)) throw new TypeError(\"Cannot call a class as a function\")\n" +
                    "                    }\n" +
                    "                    var i = n(e),\n" +
                    "                        r = \"function\" == typeof Symbol && \"symbol\" == typeof Symbol.iterator ? function(t) {\n" +
                    "                            return typeof t\n" +
                    "                        } : function(t) {\n" +
                    "                            return t && \"function\" == typeof Symbol && t.constructor === Symbol ? \"symbol\" : typeof t\n" +
                    "                        },\n" +
                    "                        a = function() {\n" +
                    "                            function t(t, e) {\n" +
                    "                                for (var n = 0; n < e.length; n++) {\n" +
                    "                                    var o = e[n];\n" +
                    "                                    o.enumerable = o.enumerable || !1, o.configurable = !0, \"value\" in o && (o.writable = !0), Object.defineProperty(t, o.key, o)\n" +
                    "                                }\n" +
                    "                            }\n" +
                    "                            return function(e, n, o) {\n" +
                    "                                return n && t(e.prototype, n), o && t(e, o), e\n" +
                    "                            }\n" +
                    "                        }(),\n" +
                    "                        c = function() {\n" +
                    "                            function t(e) {\n" +
                    "                                o(this, t), this.resolveOptions(e), this.initSelection()\n" +
                    "                            }\n" +
                    "                            return t.prototype.resolveOptions = function t() {\n" +
                    "                                var e = arguments.length <= 0 || void 0 === arguments[0] ? {} : arguments[0];\n" +
                    "                                this.action = e.action, this.emitter = e.emitter, this.target = e.target, this.text = e.text, this.trigger = e.trigger, this.selectedText = \"\"\n" +
                    "                            }, t.prototype.initSelection = function t() {\n" +
                    "                                this.text ? this.selectFake() : this.target && this.selectTarget()\n" +
                    "                            }, t.prototype.selectFake = function t() {\n" +
                    "                                var e = this,\n" +
                    "                                    n = \"rtl\" == document.documentElement.getAttribute(\"dir\");\n" +
                    "                                this.removeFake(), this.fakeHandlerCallback = function() {\n" +
                    "                                    return e.removeFake()\n" +
                    "                                }, this.fakeHandler = document.body.addEventListener(\"click\", this.fakeHandlerCallback) || !0, this.fakeElem = document.createElement(\"textarea\"), this.fakeElem.style.fontSize = \"12pt\", this.fakeElem.style.border = \"0\", this.fakeElem.style.padding = \"0\", this.fakeElem.style.margin = \"0\", this.fakeElem.style.position = \"absolute\", this.fakeElem.style[n ? \"right\" : \"left\"] = \"-9999px\", this.fakeElem.style.top = (window.pageYOffset || document.documentElement.scrollTop) + \"px\", this.fakeElem.setAttribute(\"readonly\", \"\"), this.fakeElem.value = this.text, document.body.appendChild(this.fakeElem), this.selectedText = (0, i.default)(this.fakeElem), this.copyText()\n" +
                    "                            }, t.prototype.removeFake = function t() {\n" +
                    "                                this.fakeHandler && (document.body.removeEventListener(\"click\", this.fakeHandlerCallback), this.fakeHandler = null, this.fakeHandlerCallback = null), this.fakeElem && (document.body.removeChild(this.fakeElem), this.fakeElem = null)\n" +
                    "                            }, t.prototype.selectTarget = function t() {\n" +
                    "                                this.selectedText = (0, i.default)(this.target), this.copyText()\n" +
                    "                            }, t.prototype.copyText = function t() {\n" +
                    "                                var e = void 0;\n" +
                    "                                try {\n" +
                    "                                    e = document.execCommand(this.action)\n" +
                    "                                } catch (n) {\n" +
                    "                                    e = !1\n" +
                    "                                }\n" +
                    "                                this.handleResult(e)\n" +
                    "                            }, t.prototype.handleResult = function t(e) {\n" +
                    "                                e ? this.emitter.emit(\"success\", {\n" +
                    "                                    action: this.action,\n" +
                    "                                    text: this.selectedText,\n" +
                    "                                    trigger: this.trigger,\n" +
                    "                                    clearSelection: this.clearSelection.bind(this)\n" +
                    "                                }) : this.emitter.emit(\"error\", {\n" +
                    "                                    action: this.action,\n" +
                    "                                    trigger: this.trigger,\n" +
                    "                                    clearSelection: this.clearSelection.bind(this)\n" +
                    "                                })\n" +
                    "                            }, t.prototype.clearSelection = function t() {\n" +
                    "                                this.target && this.target.blur(), window.getSelection().removeAllRanges()\n" +
                    "                            }, t.prototype.destroy = function t() {\n" +
                    "                                this.removeFake()\n" +
                    "                            }, a(t, [{\n" +
                    "                                key: \"action\",\n" +
                    "                                set: function t() {\n" +
                    "                                    var e = arguments.length <= 0 || void 0 === arguments[0] ? \"copy\" : arguments[0];\n" +
                    "                                    if (this._action = e, \"copy\" !== this._action && \"cut\" !== this._action) throw new Error('Invalid \"action\" value, use either \"copy\" or \"cut\"')\n" +
                    "                                },\n" +
                    "                                get: function t() {\n" +
                    "                                    return this._action\n" +
                    "                                }\n" +
                    "                            }, {\n" +
                    "                                key: \"target\",\n" +
                    "                                set: function t(e) {\n" +
                    "                                    if (void 0 !== e) {\n" +
                    "                                        if (!e || \"object\" !== (\"undefined\" == typeof e ? \"undefined\" : r(e)) || 1 !== e.nodeType) throw new Error('Invalid \"target\" value, use a valid Element');\n" +
                    "                                        if (\"copy\" === this.action && e.hasAttribute(\"disabled\")) throw new Error('Invalid \"target\" attribute. Please use \"readonly\" instead of \"disabled\" attribute');\n" +
                    "                                        if (\"cut\" === this.action && (e.hasAttribute(\"readonly\") || e.hasAttribute(\"disabled\"))) throw new Error('Invalid \"target\" attribute. You can\\'t cut text from elements with \"readonly\" or \"disabled\" attributes');\n" +
                    "                                        this._target = e\n" +
                    "                                    }\n" +
                    "                                },\n" +
                    "                                get: function t() {\n" +
                    "                                    return this._target\n" +
                    "                                }\n" +
                    "                            }]), t\n" +
                    "                        }();\n" +
                    "                    t.exports = c\n" +
                    "                })\n" +
                    "            }, {\n" +
                    "                select: 6\n" +
                    "            }],\n" +
                    "            9: [function(e, n, o) {\n" +
                    "                ! function(i, r) {\n" +
                    "                    if (\"function\" == typeof t && t.amd) t([\"module\", \"./clipboard-action\", \"tiny-emitter\", \"good-listener\"], r);\n" +
                    "                    else if (\"undefined\" != typeof o) r(n, e(\"./clipboard-action\"), e(\"tiny-emitter\"), e(\"good-listener\"));\n" +
                    "                    else {\n" +
                    "                        var a = {\n" +
                    "                            exports: {}\n" +
                    "                        };\n" +
                    "                        r(a, i.clipboardAction, i.tinyEmitter, i.goodListener), i.clipboard = a.exports\n" +
                    "                    }\n" +
                    "                }(this, function(t, e, n, o) {\n" +
                    "                    \"use strict\";\n" +
                    "\n" +
                    "                    function i(t) {\n" +
                    "                        return t && t.__esModule ? t : {\n" +
                    "                            \"default\": t\n" +
                    "                        }\n" +
                    "                    }\n" +
                    "\n" +
                    "                    function r(t, e) {\n" +
                    "                        if (!(t instanceof e)) throw new TypeError(\"Cannot call a class as a function\")\n" +
                    "                    }\n" +
                    "\n" +
                    "                    function a(t, e) {\n" +
                    "                        if (!t) throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\");\n" +
                    "                        return !e || \"object\" != typeof e && \"function\" != typeof e ? t : e\n" +
                    "                    }\n" +
                    "\n" +
                    "                    function c(t, e) {\n" +
                    "                        if (\"function\" != typeof e && null !== e) throw new TypeError(\"Super expression must either be null or a function, not \" + typeof e);\n" +
                    "                        t.prototype = Object.create(e && e.prototype, {\n" +
                    "                            constructor: {\n" +
                    "                                value: t,\n" +
                    "                                enumerable: !1,\n" +
                    "                                writable: !0,\n" +
                    "                                configurable: !0\n" +
                    "                            }\n" +
                    "                        }), e && (Object.setPrototypeOf ? Object.setPrototypeOf(t, e) : t.__proto__ = e)\n" +
                    "                    }\n" +
                    "\n" +
                    "                    function s(t, e) {\n" +
                    "                        var n = \"data-clipboard-\" + t;\n" +
                    "                        if (e.hasAttribute(n)) return e.getAttribute(n)\n" +
                    "                    }\n" +
                    "                    var l = i(e),\n" +
                    "                        u = i(n),\n" +
                    "                        f = i(o),\n" +
                    "                        d = function(t) {\n" +
                    "                            function e(n, o) {\n" +
                    "                                r(this, e);\n" +
                    "                                var i = a(this, t.call(this));\n" +
                    "                                return i.resolveOptions(o), i.listenClick(n), i\n" +
                    "                            }\n" +
                    "                            return c(e, t), e.prototype.resolveOptions = function t() {\n" +
                    "                                var e = arguments.length <= 0 || void 0 === arguments[0] ? {} : arguments[0];\n" +
                    "                                this.action = \"function\" == typeof e.action ? e.action : this.defaultAction, this.target = \"function\" == typeof e.target ? e.target : this.defaultTarget, this.text = \"function\" == typeof e.text ? e.text : this.defaultText\n" +
                    "                            }, e.prototype.listenClick = function t(e) {\n" +
                    "                                var n = this;\n" +
                    "                                this.listener = (0, f.default)(e, \"click\", function(t) {\n" +
                    "                                    return n.onClick(t)\n" +
                    "                                })\n" +
                    "                            }, e.prototype.onClick = function t(e) {\n" +
                    "                                var n = e.delegateTarget || e.currentTarget;\n" +
                    "                                this.clipboardAction && (this.clipboardAction = null), this.clipboardAction = new l.default({\n" +
                    "                                    action: this.action(n),\n" +
                    "                                    target: this.target(n),\n" +
                    "                                    text: this.text(n),\n" +
                    "                                    trigger: n,\n" +
                    "                                    emitter: this\n" +
                    "                                })\n" +
                    "                            }, e.prototype.defaultAction = function t(e) {\n" +
                    "                                return s(\"action\", e)\n" +
                    "                            }, e.prototype.defaultTarget = function t(e) {\n" +
                    "                                var n = s(\"target\", e);\n" +
                    "                                return n ? document.querySelector(n) : void 0\n" +
                    "                            }, e.prototype.defaultText = function t(e) {\n" +
                    "                                return s(\"text\", e)\n" +
                    "                            }, e.prototype.destroy = function t() {\n" +
                    "                                this.listener.destroy(), this.clipboardAction && (this.clipboardAction.destroy(), this.clipboardAction = null)\n" +
                    "                            }, e\n" +
                    "                        }(u.default);\n" +
                    "                    t.exports = d\n" +
                    "                })\n" +
                    "            }, {\n" +
                    "                \"./clipboard-action\": 8,\n" +
                    "                \"good-listener\": 4,\n" +
                    "                \"tiny-emitter\": 7\n" +
                    "            }]\n" +
                    "        }, {}, [9])(9)\n" +
                    "    });\n" +
                    "    </script>\n" +
                    "    <!-- クリップボード -->\n" +
                    "    <TITLE>都道府県ライブラリ</TITLE>\n" +
                    "    <script>\n" +
                    "    function funcSubmit() {\n" +
                    "        if (document.formMain.pref.selectedIndex == 0 && document.formMain.city.selectedIndex == 0) {\n" +
                    "            window.alert('都道府県と市町村を選択してください');\n" +
                    "            return false;\n" +
                    "        } else if (document.formMain.pref.selectedIndex == 0) {\n" +
                    "            window.alert('都道府県を選択してください');\n" +
                    "            return false;\n" +
                    "        } else if (document.formMain.city.selectedIndex == 0) {\n" +
                    "            window.alert('市町村を選択してください');\n" +
                    "            return false;\n" +
                    "        } else {\n" +
                    "            return true;\n" +
                    "        }\n" +
                    "    }\n" +
                    "\n" +
                    "    function funcMain(b) {\n" +
                    "\n" +
                    "        if (document.formMain.pref.selectedIndex == 0) {\n" +
                    "            document.formMain.city.length = 1;\n" +
                    "            document.formMain.city.selectedIndex = 0;\n" +
                    "        } else {\n" +
                    "            if (b) {\n" +
                    "                document.formMain.city.length = 1;\n" +
                    "                document.formMain.city.selectedIndex = 0;\n" +
                    "            }\n" +
                    "            var city = cities[document.formMain.pref.selectedIndex - 1];\n" +
                    "            document.formMain.city.length = city.length + 1;\n" +
                    "\n" +
                    "            for (var i = 0; i < city.length; i++) {\n" +
                    "                document.formMain.city.options[i + 1].value = city[i]; //Valueの設定\n" +
                    "                document.formMain.city.options[i + 1].text = city[i];\n" +
                    "            }\n" +
                    "        }\n" +
                    "    }\n" +
                    "\n" +
                    "    function dispType() {\n" +
                    "\n" +
                    "\n" +
                    "        var f = document.formMain.pref;\n" +
                    "        var cityname = document.formMain.city;\n" +
                    "        for (var i = 0; i < f.options.length; i++) {\n" +
                    "            if (f.options[i].selected) {\n" +
                    "                alert('選択した血液型：' + f.options[i].value);\n" +
                    "            }\n" +
                    "        }\n" +
                    "\n" +
                    "        for (var i = 0; i < cityname.options.length; i++) {\n" +
                    "            if (cityname.options[i].selected) {\n" +
                    "                alert('選択した血液型：' + cityname.options[i].value);\n" +
                    "            }\n" +
                    "        }\n" +
                    "    }\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "    var prefs = new Array('ほっかいどう', '東京都', '大阪府', '愛知県', '千葉県');\n" +
                    "    var cities = new Array();\n" +
                    "    //北海道\n" +
                    "    cities[0] = new Array('札幌市', '函館市', '小樽市', '旭川市', '室蘭市', '釧路市', '帯広市', '北見市', '夕張市', '岩見沢市', '網走市', '留萌市', '苫小牧市', '稚内市', '美唄市', '芦別市', '江別市', '赤平市', '紋別市', '士別市', '名寄市', '三笠市', '根室市', '千歳市', '滝川市', '砂川市', '歌志内市', '深川市', '富良野市', '登別市', '恵庭市', '伊達市', '北広島市', '石狩市', '北斗市', '当別町', '新篠津村', '松前町', '福島町', '知内町', '木古内町', '七飯町', '鹿部町', '森町', '八雲町', '長万部町', '江差町', '上ノ国町', '厚沢部町', '乙部町', '奥尻町', '今金町', 'せたな町', '島牧村', '寿都町', '黒松内町', '蘭越町', 'ニセコ村', '真狩村', '留寿都村', '喜茂別町', '京極町', '倶知安町',\n" +
                    "        '共和町', '岩内町', '泊村', '神恵内村', '積丹町', '古平町', '仁木町', '余市町', '赤井川村', '南幌町', '奈井江町', '上砂川町', '由仁町', '長沼町', '栗山町', '月形町', '浦臼町', '新十津川町', '妹背牛町', '秩父別町', '雨竜町', '北竜町', '沼田町', '鷹栖町', '東神楽町', '当麻町', '比布町', '愛別町', '上川町', '東川町', '美瑛町', '上富良野町', '中富良野町',\n" +
                    "        '南富良野町', '占冠村', '和寒町', '剣淵町', '下川町', '美深町', '音威子府村', '中川町', '幌加内町', '増毛町', '小平町', '苫前町', '羽幌町', '初山別村', '遠別町', '天塩町', '猿払村', '浜頓別町', '中頓別町', '枝幸町', '豊富町', '礼文町', '利尻町', '利尻富士町', '幌延町', '美幌町', '津別町', '斜里町', '清里町', '小清水町', '訓子府町', '置戸町',\n" +
                    "        '佐呂間町', '遠軽町', '湧別町', '滝上町', '興部町', '西興部村', '雄武町', '大空町', '豊浦町', '壮瞥町', '白老町', '厚真町', '洞爺湖町', '安平町', 'むかわ町', '日高町', '平取町', '新冠町', '浦河町', '様似町', 'えりも町', '新ひだか町', '音更町', '士幌町', '上士幌町', '鹿追町', '新得町', '清水町', '芽室町', '中札内村', '更別村', '大樹町', '広尾町', '幕別町',\n" +
                    "        '池田町', '豊頃町', '本別町', '足寄町', '陸別町', '浦幌町', '釧路町', '厚岸町', '浜中町', '標茶町', '弟子屈町', '鶴居村', '白糠町', '別海町', '中標津町', '標津町', '羅臼町');\n" +
                    "    //青森\n" +
                    "    cities[1] = new Array('青森市', '弘前市', '八戸市', '黒石市', '五所川原市', '十和田市', '三沢市', 'むつ市', 'つがる市', '平川市', '平内町', '今別町', '蓬田町', '外ヶ浜町', '鰺ヶ沢町', '深浦町', '西目屋村', '藤崎町', '大鰐町', '田舎館村', '板柳町', '鶴多町', '中泊町', '野辺地町', '七戸町', '六戸町', '横浜町', '東北町', '六ヶ所村', 'おいらせ村', '大間町', '東通村',\n" +
                    "        '風間浦村', '佐井村', '三戸町', '五戸町', '田子町', '南部町', '階上町', '新郷村');\n" +
                    "    //岩手県\n" +
                    "    cities[2] = new Array('盛岡市', '宮古市', '大船渡市', '花巻市', '北上市', '久慈市', '遠野市', '一関市', '陸前高田市', '釜石市', '二戸市', '八幡平市', '奥州市', '滝沢市', '雫石町', '葛巻町', '岩手町', '紫波町', '矢巾町', '西和賀町', '金ヶ崎町', '平泉町', '住田町', '大槌町', '山田町', '岩泉町', '田野畑村', '普代村', '軽米町', '野田村', '九戸村', '洋野町', '一戸町');\n" +
                    "    //宮城県\n" +
                    "    cities[3] = new Array('仙台市', '石巻市', '塩竈市', '気仙沼市', '白石市', '名取市', '角田市', '多賀城市', '岩沼市', '登米市', '栗原市', '東松島市', '大崎市', '蔵王町', '七ヶ宿町', '大河原町', '村田町', '柴田町', '川崎町', '丸森町', '亘理町', '山元町', '松島町', '七ヶ浜市', '利府町', '大和町', '大郷町', '富谷町', '大衡村', '色麻町', '加美町', '涌谷町', '美里町', '女川町', '南三陸町');\n" +
                    "    //秋田県\n" +
                    "    cities[4] = new Array('秋田市', '能代市', '横手町', '大館市', '男鹿市', '湯沢市', '鹿角市', '由利本荘市', '潟上市', '大仙市', '北秋田市', 'にかほ市', '仙北市', '小坂町', '上小阿仁村', '藤里町', '三種町', '八峰町', '五城目町', '八郎潟町', '井川町', '大潟村', '美郷町', '羽後町', '東成瀬村');\n" +
                    "    //山形県\n" +
                    "    cities[5] = new Array('山形市', '米沢市', '鶴岡市', '酒田市', '新庄市', '寒河江市', '上山市', '村山市', '長井市', '天童市', '東根市', '尾花沢市', '南陽市', '山辺町', '中山町', '河北町', '西川町', '朝日町', '大江町', '大石田町', '金山町', '最上町', '舟形町', '真室川町', '大蔵町', '鮭川村', '戸沢村', '高畠町', '川西町', '小国町', '白鷹町', '飯豊町', '三川町', '庄内町', '遊佐町');\n" +
                    "    //福島県 \n" +
                    "    cities[6] = new Array('福島市', '会津若松市', '郡山市', 'いわき市', '白河市', '須賀川市', '喜多方市', '相馬市', '二本松市', '田村市', '南相馬市', '伊達市', '本宮市', '桑折町', '国見町', '川俣町', '大玉村', '鏡石町', '天栄村', '下郷町', '檜枝岐村', '只見町', '南会津町', '北塩原村', '西会津町', '磐梯町', '猪苗代町', '湯川村', '柳津町', '三島町', '金山町', '昭和村', '会津美里町', '泉崎村', '中島町', '矢吹町', '棚倉町', '矢祭町', '塙町', '鮫川村', '石川町', '玉川村', '平田村', '浅川町', '古戸町', '三春町', '小野町', '広野町', '楢葉町', '富岡町', '川内村', '大熊町', '双葉町', '浪江町', '葛尾村', '新地町', '飯舘村');\n" +
                    "    //茨城県\n" +
                    "    cities[7] = new Array('水戸市', '日立市', '土浦市', '古河市', '石岡市', '結城市', '龍ヶ崎市', '下妻市', '常総市', '常陸太田市', '高萩市', '北茨城市', '笠間市', '取手市', '牛久市', 'つくば市', 'ひたちなか市', '鹿嶋市', '潮来市', '守谷市', '常陸大宮市', '那珂市', '筑西市', '坂東市', '稲敷市', 'かすみがうら市', '桜川市', '神栖市', '行方市', '鉾田市', 'つくばみらい市', '小美玉市', '茨城町', '大洗町', '城里町', '東海村', '大子町', '美浦村', '阿見町', '河内町', '八千代町', '五霞町', '境町', '利根町');\n" +
                    "    //栃木\n" +
                    "    cities[8] = new Array('宇都宮市', '足利市', '栃木市', '佐野市', '鹿沼市', '日光市', '小山市', '真岡市', '大田原市', '矢板市', '那須塩原市', 'さくら市', '那須烏山市', '下野市', '上三川町', '益子町', '茂木町', '市貝町', '芳賀町', '壬生町', '野木町', '塩谷町', '高根沢町', '那須町', '那珂川町');\n" +
                    "    //群馬\n" +
                    "    cities[9] = new Array('前橋市', '高崎市', '桐生市', '伊勢崎市', '太田市', '沼田市', '館林市', '渋川市', '藤岡市', '安中市', '富岡市', 'みどり市', '榛東村', '吉岡町', '上野村', '神流町', '下仁田町', '南牧村', '甘楽町', '中之条町', '長野原町', '嬬恋村', '草津町', '高山村', '東吾妻町', '片品村', '川場村', '昭和村', 'みなかみ町', '玉村町', '板倉町', '明和町', '千代田町', '大泉町', '邑楽町');\n" +
                    "    //埼玉県\n" +
                    "    cities[10] = new Array('さいたま市', '川越市', '熊谷市', '川口市', '行田市', '秩父市', '所沢市', '飯能市', '加須市', '本庄市', '東松山市', '春日部市', '狭山市', '羽生市', '鴻巣市', '深谷市', '上尾市', '草加市', '越谷市', '蕨市', '戸田市', '入間市', '朝霞市', '志木市', '和光市', '新座市', '桶川市', '久喜市', '北本市', '八潮市', '富士見市', '三郷市', '蓮田市', '坂戸市', '幸手市', '鶴ヶ島市', '日高市', '吉川市', 'ふじみ野市', '白岡市', '伊奈町', '三芳町', '毛呂山町', '越生町', '滑川町', '嵐山町', '小川町', '川島町', '吉見町', '鳩山町', 'ときがわ町', '横瀬町', '皆野町', '長瀞町', '小鹿野町', '東秩父村', '美里町', '神川町', '上里町', '寄居町', '宮代町', '杉戸町', '松伏町');\n" +
                    "    //千葉県\n" +
                    "    cities[11] = new Array('千葉市', '銚子市', '市川市', '船橋市', '館山市', '木更津市', '松戸市', '野田市', '茂原市', '成田市', '佐倉市', '東金市', '旭市', '習志野市', '柏市', '勝浦市', '市原市', '流山市', '八千代市', '我孫子市', '鴨川市', '鎌ヶ谷市', '君津市', '富津市', '浦安市', '四街道市', '袖ケ浦市', '八街市', '印西市', '白井市', '富里市', '南房総市', '匝瑳市',\n" +
                    "        '香取市', '山武市', 'いすみ市', '大網白里町', '酒々井町', '栄町', '神崎町', '多古町', '東庄町', '九十九里町', '芝山町', '横芝光町', '一宮町', '睦沢町', '長生村', '白子町', '長柄町', '長南町', '大多喜町', '御宿町', '鋸南町');\n" +
                    "    //東京都\n" +
                    "    cities[12] = new Array('千代田区', '中央区', '港区', '新宿区', '文京区', '台東区', '墨田区', '江東区', '品川区', '目黒区', '大田区', '世田谷区', '渋谷区', '中野区', '杉並区', '豊島区', '北区', '荒川区', '板橋区', '練馬区', '足立区', '葛飾区', '江戸川区', '八王子市', '立川市', '武蔵野市', '三鷹市', '青梅市', '府中市', '昭島市', '調布市', '町田市', '小金井市', '小平市', '日野市', '東村山市', '国分寺市', '国立市', '福生市', '狛江市', '東大和市', '清瀬市', '東久留米市', '武蔵村山市', '多摩市', '稲城市', '羽村市', 'あきる野市', '西東京市', '瑞穂町', '日の出町', '檜原村', '奥多摩町', '大島町', '利島村', '新島村', '神津島村', '八丈町', '三宅村', '御蔵島村', '青ヶ島村', '小笠原村');\n" +
                    "    //神奈川県\n" +
                    "    cities[13] = new Array('横浜市', '川崎市', '相模原市', '横須賀市', '平塚市', '鎌倉市', '藤沢市', '小田原市', '茅ヶ崎市', '逗子市', '三浦市', '秦野市', '厚木市', '大和市', '伊勢原市', '海老名市', '座間市', '南足柄市', '綾瀬市', '葉山町', '寒川町', '大磯町', '二宮町', '中井町', '大井町', '松田町', '山北町', '開成町', '箱根町', '真鶴町', '湯河原町', '愛川町', '清川村');\n" +
                    "    //新潟県\n" +
                    "    cities[14] = new Array('新潟市', '長岡市', '三条市', '柏崎市', '新発田市', '小千谷市', '加茂市', '十日町市', '見附市', '村上市', '燕市', '糸魚川市', '妙高市', '五泉市', '上越市', '阿賀野市', '佐渡市', '魚沼市', '南魚沼市', '胎内市', '聖籠町', '弥彦村', '田上町', '阿賀町', '出雲崎町', '湯沢町', '津南町', '刈羽村', '関川村', '粟島浦村');\n" +
                    "    //富山県\n" +
                    "    cities[15] = new Array('富山市', '高岡市', '魚津市', '氷見市', '滑川市', '黒部市', '砺波市', '小矢部市', '南砺市', '射水市', '舟橋村', '上市町', '立山町', '入善町', '朝日町');\n" +
                    "    //石川県\n" +
                    "    cities[16] = new Array('金沢市', '七尾市', '小松市', '輪島市', '珠洲市', '加賀市', '羽咋市', 'かほく市', '白山市', '能美市', '野々市市', '川北町', '津幡町', '内灘町', '志賀町', '宝達志水町', '中能登町', '穴水町', '能登町');\n" +
                    "    //福井県\n" +
                    "    cities[17] = new Array('福井市', '敦賀市', '小浜市', '大野市', '勝山市', '鯖江市', 'あわら市', '越前市', '坂井市', '永平寺町', '池田町', '南越前町', '美浜町', '高浜町', 'おおい町', '若狭町');\n" +
                    "    //山梨県\n" +
                    "    cities[18] = new Array('甲府市', '富士吉田市', '都留市', '山梨市', '大月市', '韮崎市', '南アルプス市', '北杜市', '甲斐市', '笛吹市', '上野原市', '甲州市', '中央市', '市川三郷町', '早川町', '身延町', '南部町', '富士川町', '昭和町', '道志村', '西桂町', '忍野村', '山中湖村', '鳴沢村', '富士河口湖町', '小菅村', '丹波山村');\n" +
                    "    //長野市\n" +
                    "    cities[19] = new Array('長野市', '松本市', '上田市', '岡谷市', '飯田市', '諏訪市', '須坂市', '小諸市', '伊那市', '駒ヶ根市', '中野市', '大町市', '飯山市', '茅野市', '塩尻市', '佐久市', '千曲市', '東御市', '安曇野市', '小海町', '川上村', '南牧村', '南相木村', '北相木村', '佐久穂町', '軽井沢町', '御代田町', '立科町', '青木村', '長和町', '下諏訪町', '富士見町', '原村', '辰野町', '箕輪町', '飯島町', '南箕輪村', '中川村', '宮田村', '松川町', '高森町', '阿南町', '阿智村', '平谷村', '根羽村', '下條村', '売木村', '天龍村', '泰阜村', '喬木村', '豊丘村', '大鹿村', '上松町', '南木曽町', '木祖村', '王滝村', '大桑村', '木曽町', '麻績村', '生坂村', '山形村', '朝日村', '筑北村', '池田町', '松川村', '白馬村', '小谷村', '坂城町', '小布施町', '高山村', '山ノ内町', '木島平村', '野沢温泉村', '信濃町', '小川村', '飯綱町', '栄村');\n" +
                    "    //岐阜県\n" +
                    "    cities[20] = new Array('岐阜市', '大垣市', '高山市', '多治見市', '関市', '中津川市', '美濃市', '瑞浪市', '羽島市', '恵那市', '美濃加茂市', '土岐市', '各務原市', '可児市', '山県市', '瑞穂市', '飛騨市', '本巣市', '群上市', '下呂市', '海津市', '岐南町', '笠松町', '養老町', '垂井町', '関ヶ原町', '神戸町', '輪之内町', '安八町', '揖斐川町', '大野町', '池田町', '北方町', '坂祝町', '富川町', '川辺町', '七宗町', '八百津町', '白河町', '東白川村', '御嵩町', '白川村');\n" +
                    "    //静岡県\n" +
                    "    cities[21] = new Array('静岡市', '浜松市', '沼津市', '熱海市', '三島市', '富士宮市', '伊東市', '島田市', '富士市', '磐田市', '焼津市', '掛川市', '藤枝市', '御殿場市', '袋井市', '下田市', '裾野市', '湖西市', '伊豆市', '御前崎市', '菊川市', '伊豆の国市', '牧之原市', '東伊豆町', '河津町', '南伊豆町', '松崎町', '西伊豆町', '函南町', '清水町', '長泉町', '小山町', '吉田町', '川根本町', '森町');\n" +
                    "    //愛知県\n" +
                    "    cities[22] = new Array('名古屋市', '豊橋市', '岡崎市', '一宮市', '瀬戸市', '半田市', '春日井市', '豊川市', '津島市', '碧南市', '刈谷市', '豊田市', '安城市', '西尾市', '蒲郡市', '犬山市', '常滑市', '江南市', '小牧市', '稲沢市', '新城市', '東海市', '大府市', '知多市', '知立市', '尾張旭市', '高浜市', '岩倉市', '豊明市', '日進市', '田原市', '愛西市', '清須市', '北名古屋市',\n" +
                    "        '弥富市', 'みよし市', 'あま市', '長久手市', '東郷町', '富山町', '大口町', '扶桑町', '大治町', '蟹江町', '飛島村', '阿久比町', '東浦町', '南知多町', '美浜町', '武豊町', '幸田町', '設楽町', '東栄町', '豊根村');\n" +
                    "    //三重県\n" +
                    "    cities[23] = new Array('津市', '四日市市', '伊勢市', '松阪市', '桑名市', '鈴鹿市', '名張市', '尾鷲市', '亀山市', '鳥羽市', '熊野市', 'いなべ市', '志摩市', '伊賀市', '木曽岬町', '東員町', '菰野町', '朝日町', '川越町', '多気町', '明和町', '大台町', '玉城町', '度会町', '大紀町', '南伊勢町', '紀北町', '御浜町', '紀宝町');\n" +
                    "    //滋賀県\n" +
                    "    cities[24] = new Array('大津市', '彦根市', '長浜市', '近江八幡市', '草津市', '守山市', '栗東市', '甲賀市', '野洲市', '湖南市', '高島市', '東近江市', '米原市', '日野町', '竜王町', '愛荘町', '豊郷町', '甲良町', '多賀町');\n" +
                    "    //京都府\n" +
                    "    cities[25] = new Array('京都市', '福知山市', '舞鶴市', '綾部市', '宇治市', '宮津市', '亀岡市', '城陽市', '向日市', '長岡京市', '八幡市', '京田辺市', '京丹後市', '南丹市', '木津川市', '大山崎町', '久御山町', '井手町', '宇治田原町', '笠置町', '和束町', '精華町', '南山城村', '京丹波町', '伊根町', '与謝野町');\n" +
                    "    //大阪府\n" +
                    "    cities[26] = new Array('大阪市', '堺市', '岸和田市', '豊中市', '池田市', '吹田市', '泉大津市', '高槻市', '貝塚市', '守口市', '枚方市', '茨木市', '八尾市', '泉佐野市', '富田林市', '寝屋川市', '河内長野市', '松原市', '大東市', '和泉市', '箕面市', '柏原市', '羽曳野市', '門真市', '摂津市', '高石市', '藤井寺市', '東大阪市', '泉南市', '四條畷市', '交野市', '大阪狭山市', '阪南市', '島本町', '豊能町', '能勢町', '忠岡町', '熊取町', '田尻町', '岬町', '太子町', '河南町', '千早赤阪村');\n" +
                    "    //兵庫県 \n" +
                    "    cities[27] = new Array('神戸市', '姫路市', '尼崎市', '明石市', '西宮市', '洲本市', '芦屋市', '伊丹市', '相生市', '豊岡市', '加古川市', '赤穂市', '西脇市', '宝塚市', '三木市', '高砂市', '川西市', '小野市', '三田市', '加西市', '篠山市', '養父市', '丹波市', '南あわじ市', '朝来市', '淡路市', '宍粟市', '加東市', 'たつの市', '猪名川町', '多可町', '稲美町', '播磨町', '市川町', '福崎町', '神河町', '太子町', '上郡町', '佐用町', '香美町', '新温泉町');\n" +
                    "    //奈良県\n" +
                    "    cities[28] = new Array('奈良市', '大和高田市', '大和郡山市', '天理市', '橿原市', '桜井市', '五條市', '御所市', '生駒市', '香芝市', '葛城市', '宇陀市', '山添村', '平群町', '三郷町', '斑鳩町', '安堵町', '川西町', '三宅町', '田原本町', '曽爾村', '御杖村', '高取町', '明日香村', '上牧町', '王寺町', '広陵町', '河合町', '吉野町', '大淀町', '下市町', '黒滝村', '天川村', '野迫川村', '十津川村', '下北山村', '上北山村', '川上村', '東吉野村');\n" +
                    "    //和歌山県\n" +
                    "    cities[29] = new Array('和歌山市', '海南市', '橋本市', '有田市', '御坊市', '田辺市', '新宮市', '紀の川市', '岩出市', '紀美野町', 'かつらぎ町', '九度山町', '高野町', '湯浅町', '広川町', '有田川町', '美浜町', '日高町', '由良町', '印南町', 'みなべ町', '日高川町', '白浜町', '上富田町', 'すさみ町', '那智勝浦町', '太地町', '古座川町', '北山村', '串本町');\n" +
                    "    //鳥取県\n" +
                    "    cities[30] = new Array('鳥取市', '米子市', '倉吉市', '境港市', '岩美町', '智頭町', '八頭町', '三朝町', '湯梨浜町', '琴浦町', '北栄町', '日吉津村', '大山町', '南部町', '伯耆町', '日南町', '日野町', '江府町');\n" +
                    "    //島根県\n" +
                    "    cities[31] = new Array('松江市', '浜田市', '出雲市', '益田市', '大田市', '安来市', '江津市', '雲南市', '奥出雲町', '飯南町', '川本町', '美郷町', '邑南町', '津和野町', '吉賀町', '海士町', '西ノ島町', '知夫村', '隠岐の島町');\n" +
                    "    //岡山県\n" +
                    "    cities[32] = new Array('岡山市', '倉敷町', '津山市', '玉野市', '笠岡市', '井原市', '総社市', '高梁市', '新見市', '備前市', '瀬戸内市', '赤磐市', '真庭市', '美作市', '浅口市', '和気町', '早島町', '里庄町', '矢掛町', '新庄村', '鏡野町', '勝央町', '奈義町', '西粟倉村', '久米南町', '美咲町', '吉備中央町');\n" +
                    "    //広島県\n" +
                    "    cities[33] = new Array('広島市', '呉市', '竹原市', '三原市', '尾道市', '福山市', '府中市', '三次市', '庄原市', '大竹市', '東広島市', '廿日市市', '安芸高田市', '江田島市', '府中町', '海田町', '熊野町', '坂町', '安芸太田町', '北広島町', '大崎上島町', '世羅町', '神石高原町');\n" +
                    "    //山口県\n" +
                    "    cities[34] = new Array('下関市', '宇部市', '山口市', '萩市', '防府市', '下松市', '岩国市', '光市', '長門市', '柳井市', '美祢市', '周南市', '山陽小野田市', '周防大島町', '和木町', '上関町', '田布施町', '平生町', '阿武町');\n" +
                    "    //徳島県\n" +
                    "    cities[35] = new Array('徳島市', '鳴門市', '小松島市', '阿南市', '吉野川市', '阿波市', '美馬市', '三好市', '勝浦町', '上勝町', '佐那河内村', '石井町', '神山町', '那賀町', '牟岐町', '美波町', '海陽町', '松茂町', '北島町', '藍住町', '板野町', '上板町', 'つるぎ町', '東みよし町');\n" +
                    "    //香川県\n" +
                    "    cities[36] = new Array('高松市', '丸亀市', '坂出市', '善通寺市', '観音寺市', 'さぬき市', '東かがわ市', '三豊市', '土庄町', '小豆島町', '三木町', '直島町', '宇多津町', '綾川町', '琴平町', '多度津町', 'まんのう町');\n" +
                    "    //愛媛県\n" +
                    "    cities[37] = new Array('松山市', '今治市', '宇和島市', '八幡浜市', '新居浜市', '西条市', '大洲市', '伊予市', '四国中央市', '西予市', '東温市', '上島町', '久万高原町', '松前町', '砥部町', '内子町', '伊方町', '松野町', '鬼北町', '愛南町');\n" +
                    "    //高知県\n" +
                    "    cities[38] = new Array('高知市', '室戸市', '安芸市', '南国市', '土佐市', '須崎市', '宿毛市', '土佐清水市', '四万十市', '香南市', '香美市', '東洋町', '奈半利町', '田野町', '安田町', '北川村', '馬路村', '芸西村', '元山町', '大豊町', '土佐町', '大川村', 'いの町', '仁淀川町', '中土佐町', '佐川町', '越智町', '梼原町', '日高村', '津野町', '四万十町', '大月町', '三原村', '黒潮町');\n" +
                    "    //福岡県\n" +
                    "    cities[39] = new Array('北九州市', '福岡市', '大牟田市', '久留米市', '直方市', '飯塚市', '田川市', '柳川市', '八女市', '筑後市', '大川市', '行橋市', '豊前市', '中間市', '小郡市', '筑紫野市', '春日市', '大野城市', '宗像市', '太宰府市', '古賀市', '福津市', 'うきは市', '宮若市', '嘉麻市', '朝倉市', 'みやま市', '糸島市', '那珂川町', '宇美町', '篠栗町', '志免町', '須恵町', '新宮町',\n" +
                    "        '久山町', '粕屋町', '芦屋町', '水巻町', '岡垣町', '遠賀町', '小竹町', '鞍手町', '桂川町', '筑前町', '東峰村', '大刀洗町', '大木町', '広川町', '香春町', '添田町', '糸田町', '川崎町', '大任町', '赤村', '福智町', '苅田町', 'みやこ町', '吉富町', '上毛町', '築上町');\n" +
                    "    //佐賀県\n" +
                    "    cities[40] = new Array('佐賀市', '唐津市', '鳥栖市', '多久市', '伊万里市', '武雄市', '鹿嶋市', '小城市', '嬉野市', '神埼市', '吉野ヶ里町', '基山町', '上峰町', 'みやき町', '玄海町', '有田町', '大町町', '江北町', '白石町', '太良町');\n" +
                    "    //長崎県\n" +
                    "    cities[41] = new Array('長崎市', '佐世保市', '島原市', '諫早市', '大村市', '平戸市', '松浦市', '津島市', '壱岐市', '五島市', '西海市', '雲仙市', '南島原市', '長与町', '時津町', '東彼杵町', '川棚町', '波佐見町', '小値賀町', '佐々町', '新上五島町');\n" +
                    "    //熊本県\n" +
                    "    cities[42] = new Array('熊本市', '八代市', '人吉市', '荒尾市', '水俣市', '玉名市', '山鹿市', '菊池市', '宇土市', '上天草市', '宇城市', '阿蘇市', '天草市', '合志市', '美里町', '玉東町', '南関町', '長洲町', '和水町', '大津町', '菊陽町', '南小国町', '小国町', '産山村', '高森町', '西原村', '南阿蘇村', '御船町', '嘉島町', '益城町', '甲佐町', '山都町', '氷川町', '芦北町', '津奈木町', '錦町', '多良木町', '湯前町', '水上村', '相良村', '五木村', '山江村', '球磨村', 'あさぎり町', '苓北町');\n" +
                    "    //大分県\n" +
                    "    cities[43] = new Array('大分市', '別府市', '中津市', '日田市', '佐伯市', '臼杵市', '津久見市', '竹田市', '豊後高田市', '杵築市', '宇佐市', '豊後大野市', '由布市', '国東市', '姫島村', '日出町', '九重町', '玖珠町');\n" +
                    "    //宮崎市   \n" +
                    "    cities[44] = new Array('宮崎市', '都城市', '延岡市', '日南市', '小林市', '日向市', '串間市', '西都市', 'えびの市', '三股町', '高原町', '国富町', '綾町', '高鍋町', '新富町', '西米良村', '木城町', '川南町', '都農町', '門川町', '諸塚村', '椎葉村', '美郷町', '高千穂町', '日之影町', '五ヶ瀬町');\n" +
                    "    //鹿児島市\n" +
                    "    cities[45] = new Array('鹿児島市', '鹿屋市', '枕崎市', '阿久根市', '出水市', '指宿市', '西之表市', '垂水市', '薩摩川内市', '日置市', '曽於市', '霧島市', 'いちき串木野市', '南さつま市', '志布志市', '奄美市', '南九州市', '伊佐市', '姶良市', '三島村', '十島村', 'さつま町', '長島町', '湧水町', '大崎町', '東串良町', '錦江町', '南大隅町', '屋久島町', '大和村', '宇検村', '瀬戸内町', '龍郷町', '喜界町', '徳之島町', '天城町', '伊仙町', '和泊町', '知名町', '与論町');\n" +
                    "    //沖縄県\n" +
                    "    cities[46] = new Array('那覇市', '宜野湾市', '石垣市', '浦添市', '名護市', '糸満市', '沖縄市', '豊見城市', 'うるま市', '宮古島市', '南城市', '国頭村市', '大宜味村市', '東村', '今帰仁村', '本部町', '恩納村', '宜野座村', '金武町', '伊江村', '読谷村', '嘉手納町', '北谷町', '北中城村', '中城村', '西原町', '与那原町', '南風原町', '渡嘉敷村', '座間味村', '粟国村', '渡名喜村', '南大東村', '北大東村', '伊平屋村', '伊是名村', '久米島町', '八重瀬町', '多良間村', '竹富町', '与那国町');\n" +
                    "    </script>\n" +
                    "    <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js'></script>\n" +
                    "    <script type='text/javascript' src='http://geoapi.heartrails.com/api/geoapi.js'></script>\n" +
                    "    <script type='text/javascript'>\n" +
                    "    </script>\n" +
                    "</head>\n" +
                    "都道府県市町村を選択してください\n" +
                    "<br>\n" +
                    "<!--県を取得javascriptでdocument.formMain.pref で値取得 -->\n" +
                    "\n" +
                    "<body onLoad='funcMain(false)'>\n" +
                    "    <form name=formMain method=POST action=result.asp onSubmit='return funcSubmit()'>\n" +
                    "        <select NAME='pref' onChange='funcMain(true)'>\n" +
                    "            <OPTION SELECTED>(都道府県を選択してください)</OPTION>\n" +
                    "            <OPTION VALUE='北海道'>北海道</OPTION>\n" +
                    "            <OPTION VALUE='青森県'>青森県</OPTION>\n" +
                    "            <OPTION VALUE='岩手県'>岩手県</OPTION>\n" +
                    "            <OPTION VALUE='宮城県'>宮城県</OPTION>\n" +
                    "            <OPTION VALUE='秋田県'>秋田県</OPTION>\n" +
                    "            <OPTION VALUE='山形県'>山形県</OPTION>\n" +
                    "            <OPTION VALUE='福島県'>福島県</OPTION>\n" +
                    "            <OPTION VALUE='茨城県'>茨城県</OPTION>\n" +
                    "            <OPTION VALUE='栃木県'>栃木県</OPTION>\n" +
                    "            <OPTION VALUE='群馬県'>群馬県</OPTION>\n" +
                    "            <OPTION VALUE='埼玉県'>埼玉県</OPTION>\n" +
                    "            <OPTION VALUE='千葉県'>千葉県</OPTION>\n" +
                    "            <OPTION VALUE='東京都'>東京都</OPTION>\n" +
                    "            <OPTION VALUE='神奈川県'>神奈川県</OPTION>\n" +
                    "            <OPTION VALUE='新潟県'>新潟県</OPTION>\n" +
                    "            <OPTION VALUE='富山県'>富山県</OPTION>\n" +
                    "            <OPTION VALUE='福井県'>福井県</OPTION>\n" +
                    "            <OPTION VALUE='石川県'>石川県</OPTION>\n" +
                    "            <OPTION VALUE='山梨県'>山梨県</OPTION>\n" +
                    "            <OPTION VALUE='長野県'>長野県</OPTION>\n" +
                    "            <OPTION VALUE='岐阜県'>岐阜県</OPTION>\n" +
                    "            <OPTION VALUE='静岡県'>静岡県</OPTION>\n" +
                    "            <OPTION VALUE='愛知県'>愛知県</OPTION>\n" +
                    "            <OPTION VALUE='三重県'>三重県</OPTION>\n" +
                    "            <OPTION VALUE='滋賀県'>滋賀県</OPTION>\n" +
                    "            <OPTION VALUE='京都府'>京都府</OPTION>\n" +
                    "            <OPTION VALUE='大阪府'>大阪府</OPTION>\n" +
                    "            <OPTION VALUE='兵庫県'>兵庫県</OPTION>\n" +
                    "            <OPTION VALUE='奈良県'>奈良県</OPTION>\n" +
                    "            <OPTION VALUE='和歌山県'>和歌山県</OPTION>\n" +
                    "            <OPTION VALUE='鳥取県'>鳥取県</OPTION>\n" +
                    "            <OPTION VALUE='島根県'>島根県</OPTION>\n" +
                    "            <OPTION VALUE='岡山県'>岡山県</OPTION>\n" +
                    "            <OPTION VALUE='広島県'>広島県</OPTION>\n" +
                    "            <OPTION VALUE='山口県'>山口県</OPTION>\n" +
                    "            <OPTION VALUE='徳島県'>徳島県</OPTION>\n" +
                    "            <OPTION VALUE='香川県'>香川県</OPTION>\n" +
                    "            <OPTION VALUE='愛媛県'>愛媛県</OPTION>\n" +
                    "            <OPTION VALUE='高知県'>高知県</OPTION>\n" +
                    "            <OPTION VALUE='福岡県'>福岡県</OPTION>\n" +
                    "            <OPTION VALUE='佐賀県'>佐賀県</OPTION>\n" +
                    "            <OPTION VALUE='長崎県'>長崎県</OPTION>\n" +
                    "            <OPTION VALUE='熊本県'>熊本県</OPTION>\n" +
                    "            <OPTION VALUE='大分県'>大分県</OPTION>\n" +
                    "            <OPTION VALUE='宮崎県'>宮崎県</OPTION>\n" +
                    "            <OPTION VALUE='鹿児島県'>鹿児島県</OPTION>\n" +
                    "            <OPTION VALUE='沖縄県'>沖縄県</OPTION>\n" +
                    "        </select>\n" +
                    "        <br>\n" +
                    "        <!--県を取得javascriptでdocument.formMain.city で値取得 -->\n" +
                    "        <select name='city'>\n" +
                    "            <option value='notselect' selected>(市町村を選択してください)</option>\n" +
                    "        </select>\n" +
                    "    </form>\n" +
                    "    <!--救援物資情報を取得javascriptでdocument.ask.goods で値取得 -->\n" +
                    "    <form name=ask>\n" +
                    "        <select NAME='goods'>\n" +
                    "            <OPTION SELECTED>必要なものを選んでください</OPTION>\n" +
                    "            <OPTION VALUE='飲料'>飲料</OPTION>\n" +
                    "            <OPTION VALUE='食料'>食料</OPTION>\n" +
                    "            <OPTION VALUE='防寒具'>防寒具</OPTION>\n" +
                    "            <OPTION VALUE='その他'>その他</OPTION>\n" +
                    "        </select>\n" +
                    "    </form>\n" +
                    "    <br>\n" +
                    "    <br>\n" +
                    "    <noscript>\n" +
                    "        <p class='warning-message' <a href='/web-app/support/javascript/'>;JavaScriptを有効にする</a>と、サンプルを実行できます</p>\n" +
                    "    </noscript>\n" +
                    "    <!--救援物資でその他を選んだら利用しよう-->\n" +
                    "    </form>その他を選んだら書いてね\n" +
                    "    <br>\n" +
                    "    <div class='example-box'>\n" +
                    "        <form name=priority>\n" +
                    "            <select NAME='number'>\n" +
                    "                <OPTION SELECTED>優先度</OPTION>\n" +
                    "                <OPTION VALUE='1'>1</OPTION>\n" +
                    "                <OPTION VALUE='2'>2</OPTION>\n" +
                    "                <OPTION VALUE='3'>3</OPTION>\n" +
                    "                <OPTION VALUE='4'>4</OPTION>\n" +
                    "                <OPTION VALUE='5'>5</OPTION>\n" +
                    "            </select>\n" +
                    "        </form>\n" +
                    "        <textarea style='display: block' rows=3 cols=100></textarea><a href='jtavascript:;' download='sample.txt'>ダウンロード</a>\n" +
                    "    </div>\n" +
                    "    <script type='text/javascript'>\n" +
                    "    (function() {\n" +
                    "\n" +
                    "        var anchor = document.links[document.links.length - 1]\n" +
                    "        if ('download' in anchor) {\n" +
                    "            anchor.download = 'sample.txt';\n" +
                    "\n" +
                    "            anchor.onclick = function() {\n" +
                    "\n" +
                    "                //県の情報\n" +
                    "                var prefname = document.formMain.pref;\n" +
                    "                //市町村の情報\n" +
                    "                var cityname = document.formMain.city;\n" +
                    "                //救援物資の情報\n" +
                    "                var askgoods = document.ask.goods;\n" +
                    "                //優先度\n" +
                    "                var prioritynumber = document.priority.number;\n" +
                    "\n" +
                    "                //上記の情報が何を選んだかを格納する変数\n" +
                    "                var prefnamecomit;\n" +
                    "                var citynamecomit\n" +
                    "                var askgoodscomit;\n" +
                    "                var prioritynumbercomit;\n" +
                    "\n" +
                    "                //何県を選んだかを調べる\n" +
                    "                for (var i = 0; i < prefname.options.length; i++) {\n" +
                    "                    if (prefname.options[i].selected) {\n" +
                    "                        prefnamecomit = prefname.options[i].value;\n" +
                    "                    }\n" +
                    "                }\n" +
                    "\n" +
                    "                //何市を選んだかを調べる\n" +
                    "                for (var i = 0; i < cityname.options.length; i++) {\n" +
                    "                    if (cityname.options[i].selected) {\n" +
                    "                        //alert('選択した血液型：' + cityname.options[i].value);\n" +
                    "                        citynamecomit = cityname.options[i].value;\n" +
                    "                    }\n" +
                    "                }\n" +
                    "\n" +
                    "                //求められる物資を調べる\n" +
                    "                for (var i = 0; i < askgoods.options.length; i++) {\n" +
                    "                    if (askgoods.options[i].selected) {\n" +
                    "                        //alert('選択した血液型：' + cityname.options[i].value);\n" +
                    "                        askgoodscomit = askgoods.options[i].value;\n" +
                    "                    }\n" +
                    "                }\n" +
                    "\n" +
                    "                //求められる物資を調べる\n" +
                    "                for (var i = 0; i < prioritynumber.options.length; i++) {\n" +
                    "                    if (prioritynumber.options[i].selected) {\n" +
                    "                        //alert('選択した血液型：' + cityname.options[i].value);\n" +
                    "                        prioritynumbercomit = prioritynumber.options[i].value;\n" +
                    "                    }\n" +
                    "                }\n" +
                    "\n" +
                    "\n" +
                    "                //都道府県市町村が選ばれている時にダウンロードできるようにする処理\n" +
                    "                if (prefnamecomit == '(都道府県を選択してください)') {\n" +
                    "                    alert('都道府県選んで');\n" +
                    "                } \n" +
                    "                else if (citynamecomit == 'notselect') {\n" +
                    "                    alert('市町村を選んで');\n" +
                    "                }\n" +
                    "\n" +
                    "\n" +
                    "                 else {\n" +
                    "                    if (askgoodscomit == 'その他') {\n" +
                    "                        if (prioritynumbercomit == '優先度') {\n" +
                    "                            alert('優先度を選んでください');\n" +
                    "                        } else {\n" +
                    "                            var text = '<ここから>--------------------------\\n'\n" +
                    "                            text += '県名:'+prefnamecomit + '\\n';\n" +
                    "                            text += '市町村名:'+citynamecomit + '\\n';\n" +
                    "                            text += '救援物資:'+askgoodscomit;\n" +
                    "                            text += '\\n'+'優先度:' + prioritynumbercomit\n" +
                    "                            text += '\\n' + '備考:'+this.previousSibling.value\n" +
                    "                            text += '\\n-------------------------------<ここまで>'\n" +
                    "                            this.href = 'data:,' + encodeURIComponent(text);\n" +
                    "                        }\n" +
                    "\n" +
                    "                    } else {\n" +
                    "                        var text = '<ここから>--------------------------\\n'\n" +
                    "                        text += '県名:'+prefnamecomit + '\\n';\n" +
                    "                            text += '市町村名:'+citynamecomit + '\\n';\n" +
                    "                            text += '救援物資:'+askgoodscomit;\n" +
                    "                        text += '\\n-------------------------------<ここまで>'\n" +
                    "                        this.href = 'data:,' + encodeURIComponent(text);\n" +
                    "                    }\n" +
                    "\n" +
                    "                }\n" +
                    "\n" +
                    "\n" +
                    "            }\n" +
                    "        } else {\n" +
                    "            var message = document.createElement('span');\n" +
                    "            message.className = 'warning-message';\n" +
                    "            message.appendChild(document.createTextNode('お使いのブラウザでは、download属性がサポートされていません。'));\n" +
                    "\n" +
                    "            anchor.parentNode.appendChild(message);\n" +
                    "        }\n" +
                    "    })();\n" +
                    "    </script>\n" +
                    "    <br>\n" +
                    "    <br> ダウンロードしたファイルを選択してください\n" +
                    "    <br>\n" +
                    "    <!--ファイルを読み込んでクリップボードを起動させる処理-->\n" +
                    "    <!-- ファイルの読み込みエリアの指定 -->\n" +
                    "    <form name='test'>\n" +
                    "        <input type='file' id='selfile'>\n" +
                    "        <br>\n" +
                    "        <textarea id='fe_text' name='txt' rows='10' cols='50' readonly></textarea>\n" +
                    "    </form>\n" +
                    "    <!-- ファイルの読み込みコピーボタン -->\n" +
                    "    <button class='btn' data-clipboard-target='#fe_text' onClick=\"kakunin()\">\n" +
                    "        クリップボードにコピー\n" +
                    "    </button>\n" +
                    "    <button>\n" +
                    "        長押ししてください\n" +
                    "    </button>\n" +
                    "    <script>\n" +
                    "    var obj1 = document.getElementById('selfile');\n" +
                    "\n" +
                    "    //ダイアログでファイルが選択された時\n" +
                    "    obj1.addEventListener('change', function(evt) {\n" +
                    "\n" +
                    "        var file = evt.target.files;\n" +
                    "\n" +
                    "        //FileReaderの作成\n" +
                    "        var reader = new FileReader();\n" +
                    "        //テキスト形式で読み込む\n" +
                    "        reader.readAsText(file[0]);\n" +
                    "\n" +
                    "        //読込終了後の処理\n" +
                    "        reader.onload = function(ev) {\n" +
                    "            //テキストエリアに表示する\n" +
                    "            document.test.txt.value = reader.result;\n" +
                    "        }\n" +
                    "        var clipboard = new Clipboard('.btn');\n" +
                    "    }, false);\n" +
                    "\n" +
                    "    $(function() {\n" +
                    "        var clipboard = new Clipboard('.btn');\n" +
                    "    });\n" +
                    "\n" +
                    "    function kakunin() {\n" +
                    "        var clipboard = new Clipboard('.btn');\n" +
                    "    }\n" +
                    "    </script>";



            return newFixedLengthResponse(msg +msg2 +msg3 +msg4 +msg5 +"</body></html>\n");
        }
    }
}